import tomllib
from dataclasses import dataclass
from pathlib import Path

import colorcet as cc
import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
from matplotlib.axes import Axes
from matplotlib.colors import ListedColormap
from matplotlib.figure import Figure
from skimage.feature import peak_local_max

from wimr_postproc import spectra
from wimr_postproc.cube import CartCube
from wimr_postproc.params import SpectrumRoiParams
from wimr_postproc.spectra.plotting import KxKyPlotInfo
from wimr_postproc.spectra.datastructs import SpectrumFKxKy, SpectrumFPhi, SpectrumKxKy
from wimr_postproc.util.conversions import cart2pol
from wimr_postproc.util.types import is_F64Arr2D

CWD = Path(__file__).parent
SAMPLE_DIR = CWD / ".." / "samples"


def plot_kxky(
    spec_kxky: SpectrumKxKy,
    title: str,
) -> tuple[Figure, Axes, KxKyPlotInfo]:
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111)
    plot_info = spectra.plot_kxky_spec_contour(ax, spec_kxky, title)
    return fig, ax, plot_info


@dataclass
class KxKyStats:
    peak_kx: float
    peak_ky: float
    peak_azi: float
    peak_k: float
    peak_L: float
    peak_ratio: float


def get_kxky_stats(spec_kxky: SpectrumKxKy) -> KxKyStats:
    kxky_data = spec_kxky.data
    # print stats
    peak_kxi, peak_kyi = np.unravel_index(np.nanargmax(kxky_data), kxky_data.shape)
    peak_kx, peak_ky = spec_kxky.k[peak_kxi], spec_kxky.k[peak_kyi]
    peak_phi, _ = cart2pol(peak_kx, peak_ky)
    peak_azi = np.mod(90 - peak_phi * 180 / np.pi, 360)
    peak_k = np.sqrt(peak_kx**2 + peak_ky**2)
    peak_L = 2 * np.pi / peak_k
    peak = kxky_data[peak_kxi, peak_kyi]
    two_sigma = np.nanmean(kxky_data) + 2 * np.nanstd(kxky_data)
    peak_ratio = peak / two_sigma
    return KxKyStats(peak_kx, peak_ky, peak_azi, peak_k, peak_L, peak_ratio)


def print_kxky_stats(stats: KxKyStats, f_type: str = "f-integrated"):
    print(
        f"{f_type} peak kx: {stats.peak_kx:.3f}, peak ky: {stats.peak_ky:.3f}\n"
        f"{f_type} peak azi: {stats.peak_azi:.1f}\n"
        f"{f_type} peak k: {stats.peak_k:.3f}, peak L{stats.peak_L:.3f}\n"
        f"{f_type} peak ratio: {stats.peak_ratio:.2f}"
    )


def plot_fphi(spec_fphi: SpectrumFPhi, title: str):
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111)
    spectra.plot_fphi_spectrum(ax, spec_fphi, title)
    png_path = CWD / "fig" / f"{title}_fphi.png"
    fig.savefig(png_path)


def plot_fkxky_scatter(spec_fkxky: SpectrumFKxKy, title: str):
    # create colormap
    fire_r_data = cc.m_fire_r(np.arange(cc.m_fire_r.N))
    fire_r_data[:, -1] = np.linspace(0, 1, cc.m_fire_r.N)
    fire_r_alpha_cmap = ListedColormap(fire_r_data, "fire_r_alpha")
    # get coords
    kx_grid, ky_grid, f_grid = np.meshgrid(
        spec_fkxky.k, spec_fkxky.k, spec_fkxky.f, indexing="ij"
    )
    kx_flat = kx_grid.flatten()
    ky_flat = ky_grid.flatten()
    f_flat = f_grid.flatten()
    spec_flat = spec_fkxky.data.flatten()

    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(projection="3d")
    ax.scatter(kx_flat, ky_flat, f_flat, c=spec_flat, cmap=fire_r_alpha_cmap)
    ax.set_title(title)


def plot_all_f_kxky(spec_fkxky: SpectrumFKxKy, region_name: str):
    fig = plt.figure(figsize=(8, 8))
    spec_max = np.nanmax(spec_fkxky.data)
    for freq, kxky_data in zip(spec_fkxky.f, spec_fkxky.data):
        fig.clf()
        assert is_F64Arr2D(kxky_data)
        spec_kxky = SpectrumKxKy(kxky_data, spec_fkxky.k)
        ax = fig.add_subplot(111)
        title = f"{region_name} f = {freq:.3f}\n"
        spectra.plot_kxky_spec_contour(
            ax,
            spec_kxky,
            title=title,
            vmax=spec_max,
            L_min=1/spec_fkxky.k.max(),
            cmap=cc.m_fire,
        )
        png_path = CWD / "fig" / f"{region_name}_f{freq:.3f}.png"
        fig.savefig(png_path)
        print(".", end="")
    print()
    plt.clf()
    spec_kxky = spectra.fkxky_to_kxky(spec_fkxky)
    ax = fig.add_subplot(111)
    spectra.plot_kxky_spec_contour(
        ax,
        spec_kxky,
        f"{region_name} f_int",
        vmax=np.nanmax(spec_kxky.data),
        L_min=1/spec_fkxky.k.max(),
        cmap=cc.m_fire,
    )
    png_path = CWD / "fig" / f"{region_name}_f_int.png"
    fig.savefig(png_path)
    print(png_path)
    # plt.pause(0.1)


def fkxky_peaks(spec_fkxky: SpectrumFKxKy) -> npt.NDArray[np.int_]:
    spec_data = spec_fkxky.data.copy()
    spec_data[np.isnan(spec_data)] = 0.0001
    peaks = peak_local_max(spec_data, min_distance=3, threshold_rel=0.75)
    for peak in peaks:
        print(peak, spec_data[*peak] / spec_data.max())
    return peaks


if __name__ == "__main__":
    with open(CWD / "../example_spectra_params.toml", "rb") as fobj:
        subimages = [
            SpectrumRoiParams.model_validate(si) for si in tomllib.load(fobj)["subimages"]
        ]
    specs_fkxky: dict[str, SpectrumFKxKy] = {}
    specs_kxky: dict[str, SpectrumKxKy] = {}
    spec_fphi: dict[str, SpectrumFPhi] = {}
    for subimage in subimages[2:]:
        print(f"--{subimage.name}--")
        with CartCube.from_matfile_path(
            SAMPLE_DIR / "2024-11-12" / "NewportTower_20241112_000004_cart_fftRegion.mat"
            # SAMPLE_DIR / "2024-11-12" / "NewportTower_20241112_050004_cart_fftRegion.mat"
        ) as cube:
            specs_fkxky[subimage.name], snrs = spectra.get_spectral_data(cube, subimage)
        specs_kxky[subimage.name] = spectra.fkxky_to_kxky(specs_fkxky[subimage.name])
        peaks_2d = spectra.kxky_peaks(specs_fkxky[subimage.name], specs_kxky[subimage.name], min_distance=1)
        print(f"2D kxky peaks:\n{peaks_2d}")
        peaks_3d = spectra.fkxky_peaks_3d(specs_fkxky[subimage.name], min_distance=4, threshold_rel=0.5)
        print(f"3D fkxky peaks:\n{peaks_3d}")
        print()
        spec_fphi[subimage.name] = spectra.fkxky_to_fphi(specs_fkxky[subimage.name])
        print_kxky_stats(get_kxky_stats(specs_kxky[subimage.name]), "f-integrated")
        # fig, ax, kxky_info = plot_kxky(specs_kxky[subimage.name], f"{subimage.name} kx-ky")
        # plot_fphi(spec_fphi[subimage.name], f"{subimage.name} f-phi")
        print(f"snrs = {snrs}")
        plot_all_f_kxky(specs_fkxky[subimage.name], subimage.name)
        # plot_fkxky_scatter(specs_fkxky[subimage.name], subimage.name)
        # plt.pause(1)
    # plt.show()
