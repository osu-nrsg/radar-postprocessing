import cProfile
import multiprocessing
import sys
import tomllib
from pathlib import Path
from typing import Literal, assert_never

from wimr_postproc.cube import CartCube
from wimr_postproc.params import SpectraGenParams
from wimr_postproc.spectra import get_spectral_data
from wimr_postproc.spectra.datastructs import SpectrumFKxKy
from wimr_postproc.spectra.stats import SNRs, f_peaks, kxky_peaks
from wimr_postproc.spectra.transforms import fkxky_to_f, fkxky_to_kxky

CONCURRENCY: Literal["serial", "parallel", "GPU"] = "GPU"


with open("example_spectra_params.toml", "rb") as fobj:
    params = SpectraGenParams.model_validate(tomllib.load(fobj))


def get_cube_spectra_data(
    cube_path: Path, use_cupy: bool = False
) -> dict[str, tuple[SpectrumFKxKy, SNRs]]:
    print(f"<!-- Processing {cube_path} -->", flush=True)
    with CartCube.from_matfile_path(cube_path) as cube:
        roi_data = {}
        for roi in params.subimages:
            roi_data[roi.name] = get_spectral_data(cube, roi, use_cupy=use_cupy)
            print(f"<!-- {cube_path} {roi.name} done-->", flush=True)
    return roi_data


def gcsd_wrapper(
    args: tuple[int, Path],
) -> tuple[int, dict[str, tuple[SpectrumFKxKy, SNRs]]]:
    return args[0], get_cube_spectra_data(args[1])


def calc_ffts(cube_paths: list[Path]) -> list[dict[str, tuple[SpectrumFKxKy, SNRs]]]:
    if CONCURRENCY == "serial":
        all_spectra = [get_cube_spectra_data(cube_path) for cube_path in cube_paths]
    elif CONCURRENCY == "GPU":
        all_spectra = [
            get_cube_spectra_data(cube_path, use_cupy=True) for cube_path in cube_paths
        ]
    elif CONCURRENCY == "parallel":
        with multiprocessing.Pool(processes=multiprocessing.cpu_count() // 2) as pool:
            all_spectra_d = dict(pool.imap_unordered(gcsd_wrapper, enumerate(cube_paths)))
            all_spectra = [
                roi_data
                for _, roi_data in sorted(all_spectra_d.items(), key=lambda item: item[0])
            ]
    else:
        assert_never(CONCURRENCY)
    return all_spectra


if __name__ == "__main__":
    cubes_dir = Path(sys.argv[1])
    cube_paths = sorted(cubes_dir.glob("*fftRegion.mat"))[:4]
    all_spectra = calc_ffts(cube_paths)
    print(f"# {cubes_dir.name}\n", flush=True)

    for cube_path, roi_data in zip(cube_paths, all_spectra):
        print(f"## {cube_path.name}\n", flush=True)
        for roi_name, (spec_fkxky, snrs) in roi_data.items():
            print(f"### {roi_name}\n")
            print("#### Peaks from kxky\n")
            spec_kxky = fkxky_to_kxky(spec_fkxky)
            spec_f = fkxky_to_f(spec_fkxky)
            peaks_from_kxky = kxky_peaks(spec_kxky, spec_fkxky)
            peaks_from_f = f_peaks(spec_f)
            for _, peak in peaks_from_kxky.iterrows():
                print(
                    (
                        "    "
                        f"k: {peak['k']:4.3f}, L = {peak['L']:4.0f}"
                        f", f: {peak['f']:0.3f}, T = {peak['T']:3.0f}"
                        f", azi: {peak['azi']:3.0f}"
                        f", Gvalue: {peak['value'] / 1e9:4.0f}"
                        f", norm_value: {peak['norm_value']:.3f}"
                        f", Gvalue/BSA94: {peak['value'] / 1e9 / snrs.BSA94:g}"
                    ),
                    flush=True,
                )
            print("")
            print("#### Peaks from f\n")
            for _, peak in peaks_from_f.iterrows():
                print(
                    (
                        "    "
                        f"f: {peak['f']:0.3f}, T = {peak['T']:3.0f}"
                        f", Gvalue: {peak['value'] / 1e9:4.0f}"
                        f", norm_value: {peak['norm_value']:.3f}"
                        f", Gvalue/BSA94: {peak['value'] / 1e9 / snrs.BSA94:g}"
                    )
                )
            print("")
            print("#### SNRS\n")
            print(f"    AH82: {snrs.AH82:4.1f}, BSA94: {snrs.BSA94:4.1f}")
            print("")
