from pathlib import Path

from wimr_postproc.imagery import plot_and_send_imagery
from wimr_postproc.params import ImageryGenParams, RepeatImageParams

CWD = Path(__file__).parent
SAMPLE_DIR = CWD / ".." / "samples"

if __name__ == "__main__":
    rip = RepeatImageParams(img_type="average", period_s=5)
    igp = ImageryGenParams(img_dir=Path("/tmp"), date_dir=True, repeat_images=[rip])
    plot_and_send_imagery(SAMPLE_DIR / "NewportTower_20241008_181603_pol.mat", igp)
