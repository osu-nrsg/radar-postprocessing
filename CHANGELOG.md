# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.5] - 204-05-20

- New - Return path to created KMZ if generation succeeds

## [0.4.4] - 2024-04-26

- Update **typer** to 0.12.3

## [0.4.3] - 2024-04-24

### Added

- `RsyncParams` - Multiple servers can be provided - an error is only raised if all of
  them fail.

### Fixed

- Pydantic 2 deprecated call

## [0.4.2] - 2024-04-12

- Fix Pydantic 2 model_validator to work correctly

## [0.4.1] - 2024-04-12

- Update MATLAB engine installer for R2022b+
- Update lock file versions

## [0.4.0] - 2024-04-10

- **Breaking change** - Update to pydantic 2
- Minor cleanup

## [0.3.2] - 2024-04-10

- Fix pydantic dependency specification to avoid issues with typing.Literal.

## [0.3.1] - 2024-04-09

### Changed

- Drop pydantic-plus -- just use pydantic
- Change to using `ruff format` over `black` for code formatting
- Change line length to 90
- `TimeSeriesPlots2.m` - intParams file selection pulled out to new `select_iP_paths.m`
  function.
- Log messages for `CalledProcessError` errors (e.g. `rsync` to campus) now include stdout
  and stderr (and they are decoded strings).
- `rsync` command includes a trailing slash on the destination directory

### Added

- Add explicit check for expired MATLAB license
- `accumulate_spectral_stats.m`
  - Stats with invalid timestamps are removed
  - Also return a `timetable` of stats
- `generate_spectral_ts_csv.m` - (currently unused) MATLAB function to generate a
  timeseries of spectral parameters

## [0.3.0] - 2023-01-16

- mat_spectra - calculate peak period from wavenumber-integrated frequency spectrum, not
  peak wavenumber and the dispersion relation.
- **BREAKING CHANGE:** `SpectraParams` string parameter `wave_dir_type` changed to boolean
  parameter `wdir_is_heading`.
- MATLAB-releated install scripts moved to separate module
- Added spectral timeseries plotting functionality
- Python minimum version 3.9 - use tuple, dict, list, etc. for typing
- Fix some ambiguous variable namesSpectraParams
- Add `no_replot` option to `pol_image_create()`
- Convert some constants to top-of-file `ALL_CAPS` vars
- Rework spectra plot dimensions, circle labels, etc.
- Use SNR to pick the best subimage, not just the first one
- Plot multiple peak wavelengths on the plot, and give direction as cardinal (e.g. ENE)
- Add preliminary code to look at frequency-direction spectra
- Reformatted some MATLAB code
- paintFilledCircles.m reworked to filled_ring.m

## [0.2.4] - 2022-10-11

- minor future_plotting development
- Update KMZ generation to work correctly with non-zero startAzi.

## [0.2.3] - 2021-08-04

- Revert to pydantic-plus.

## [0.2.2] - 2021-08-04

- Fix order of classmethod and validate_arguments decorators in BaseModel

## [0.2.1] - 2021-08-03

- Replace pydantic-plus with local module.

## [0.2.0] - 2021-07-27

- Update pydantic-plus to 1.1.2 - (rtoml) to allow mixed arrays in TOML (float and int)
- Add co\[Geo\]Rectify MATLAB scripts, accompanying params classes, and `corectify_cube`
  function to call them.

## [0.1.14] - 2021-07-06

- Ensure r_in and azi0_in are the same type before interpolation
- dunwrap refactor to match unwrap
- Other refactors that shouldn't affect functionality.

## [0.1.13] - 2021-06-16

- Add colorcet to mat_kmz
- In KMZ gen use time instead of timeInt to work for moving polar
- Add timexPeriod param to KMZ to use less than the whole cube for the timex
- Fixed math error in img_norm
- Fixed timezone issue in image_create and plot_and_send_spectra if system is not set to
  UTC.
- moving_avgs script to make moving mean/median movies from moving polar cubes

## [0.1.12] - 2021-06-10

- Added missing timezone_convert script to kmz processing and fixed paths sent to kmz
  MATAB script

## [0.1.11] - 2021-05-23

- spectra - Only write png metadata if file exists.

## [0.1.10] - 2021-05-13

- Add repeat_images option to pol_image_create -- generate multiple periodic images from a
  single recording
- Update some "defaults" in the example params files

## [0.1.9] - 2021-05-12

- Use pydantic.json() rather than json.encode()
- Varios small fixes & refactors
- In pol_image_create use startAzi when calculating azi0

## [0.1.8] - 2021-05-04

- Added KMZ generation facility (make_kmz, polMat2TimexKmz.m)
- Added fn to install MATLAB google earth toolbox

## [0.1.7] - 2021-04-29

- Replaced _pydantic./py with pydantic-plus
- Fixed params model order to not need to update_forward_refs

## [0.1.6] - 2021-04-10

- install_mat_eng moved to mat_eng.py
- deprecated (future) plotting.py
- images and spectra have JSON metadata included in the Description chunk
- plot_and_send_imagery returns Paths

## [0.1.5] - 2021-04-08

- Disable DISPLAY environment var when checking MATLAB license

## [0.1.4] - 2021-03-23

- Mostly minor dependency updates

## 0.1.3 -- deleted

## [0.1.2] -  2021-03-23

- Replace `dataclass-params` with `pydantic`

## [0.1.1] -  2021-03-17

Initial release

[Unreleased]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.4.5...master
[0.4.5]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.4.4...v0.4.5
[0.4.4]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.4.3...v0.4.4
[0.4.3]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.3.2...v0.4.0
[0.3.2]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.2.4...v0.3.0
[0.2.4]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.2.3...v0.2.4
[0.2.3]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.14...v0.2.0
[0.1.14]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.13...v0.1.14
[0.1.13]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.12...v0.1.13
[0.1.12]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.11...v0.1.12
[0.1.11]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.10...v0.1.11
[0.1.10]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.9...v0.1.10
[0.1.9]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.8...v0.1.9
[0.1.8]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.7...v0.1.8
[0.1.7]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.2...v0.1.4
[0.1.2]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/osu-nrsg/radar-postprocessing/-/tags/v0.1.1
