import contextlib
import dataclasses
import inspect
import types
from enum import Enum
from typing import Any, Callable, NamedTuple, get_type_hints

import class_doc
import pydantic
from pydantic_core import PydanticUndefined


class NoDefaultType(Enum):
    """Generic sentinel type for fields with no default or default factory"""

    NoDefault = "NoDefault"


NoDefault = NoDefaultType.NoDefault


@dataclasses.dataclass
class ModelField:
    """A generalized record of model (e.g. dataclass, Pydantic model, or NamedTuple)
    field information.

    Attributes
    ----------
    name
        The name of the field
    annotation
        The type annotation for the field (from typing.get_type_hints())
    default
        The default value, if any, otherwise `NoDefault`.
    default_factory
        The default value factory callable, if any. Otherwise `NoDefault`.
    docstring
        The docstring for the field, if any.

    """

    name: str
    annotation: Any
    default: Any = NoDefault
    default_factory: (
        Callable[[], Any] | Callable[[dict[str, Any]], Any] | NoDefaultType
    ) = NoDefault
    docstring: str = ""

    def __post_init__(self):
        if self.default is not NoDefault and self.default_factory is not NoDefault:
            raise ValueError("Cannot specify both default and default_factory")

    @property
    def optional(self):
        return self.default is not NoDefault or self.default_factory is not NoDefault


def _class_attr_docstrings(cls: type) -> dict[str, str]:
    """Get the docstrings for all class attributes in a class's MRO. (subclass docstrings
    override superclass docstrings).

    Parameters
    ----------
    cls
        The class from which to get the docstrings

    Returns
    -------
    dict[str, str]
        Mapping of attribute names to their docstrings.
    """
    attr_doclines: dict[str, list[str]] = {}
    for klass in reversed(inspect.getmro(cls)):
        with contextlib.suppress(TypeError):  # raised if source isn't available
            attr_doclines |= class_doc.extract_docs_from_cls_obj(klass)
    return {name: "\n".join(lines) for name, lines in attr_doclines.items()}


def _namedtuple_fields(cls: type[NamedTuple]) -> dict[str, ModelField]:
    """Get a dict for every field of a named tuple where the keys are fields names
    and each value is a ModelField entity containing the field name (again), annotation,
    default value (if any), default factory (if any), and attribute docstring.

    If there is no default value it will be set to the `NoDefault` sentinel. NamedTuples
    can't have default factories so that will always be set to the `NoDefault` sentinel.

    Parameters
    ----------
    cls
        A NamedTuple class.

    Returns
    -------
    The dict of defaults

    Raises
    ------
    TypeError
        `cls` is not a named tuple.
    """

    annotations = get_type_hints(cls)
    docstrings = _class_attr_docstrings(cls)
    try:
        fieldnames = cls._fields
        defaults = cls._field_defaults
    except AttributeError:
        raise TypeError("cls is not a named tuple.") from None
    return {
        fieldname: ModelField(
            name=fieldname,
            annotation=annotations[fieldname],
            default=defaults.get(fieldname, NoDefault),
            default_factory=NoDefault,
            docstring=docstrings.get(fieldname, ""),
        )
        for fieldname in fieldnames
    }


def _dataclass_fields(cls: type) -> dict[str, ModelField]:
    """Get a dict for every field of a dataclass where the keys are fields names
    and each value is a ModelField entity containing the field name (again), annotation,
    default value (if any), default factory (if any), and attribute docstring.

    The `NoDefault` sentinel is used in the default and/or default value fields of
    ModelField to indicate they are not provided.

    Parameters
    ----------
    cls
        A dataclass.

    Returns
    -------
    The dict of defaults.

    Raises
    ------
    TypeError
        `cls` is not a dataclass.
    """
    annotations = get_type_hints(cls)
    docstrings = _class_attr_docstrings(cls)
    try:
        fields = dataclasses.fields(cls)
    except AttributeError:
        raise TypeError("cls is not a dataclass") from None
    return {
        field.name: ModelField(
            name=field.name,
            annotation=annotations[field.name],
            default=(
                field.default if field.default is not dataclasses.MISSING else NoDefault
            ),
            default_factory=(
                field.default_factory
                if field.default_factory is not dataclasses.MISSING
                else NoDefault
            ),
            docstring=docstrings.get(field.name, ""),
        )
        for field in fields
    }


def _pydmodel_fields(cls: type["pydantic.BaseModel"]) -> dict[str, ModelField]:
    """Get a dict for every field of a Pydantic model class where the keys are fields
    names and each value is a ModelField entity containing the field name (again),
    annotation, default value (if any), default factory (if any), and attribute docstring.

    The `NoDefault` sentinel is used in the default and/or default value fields of
    ModelField to indicate they are not provided.

    Parameters
    ----------
    cls
        A pydantic model class.

    Returns
    -------
    The dict of defaults.

    Raises
    ------
    TypeError
        `cls` is not a pydantic model class.
    """
    annotations = get_type_hints(cls)
    docstrings = _class_attr_docstrings(cls)
    try:
        fields = cls.model_fields
    except AttributeError:
        raise TypeError("cls is not a dataclass") from None
    return {
        field_name: ModelField(
            name=field_name,
            annotation=annotations[field_name],
            default=(
                field.default if field.default is not PydanticUndefined else NoDefault
            ),
            default_factory=(field.default_factory or NoDefault),
            docstring=docstrings.get(field_name, ""),
        )
        for field_name, field in fields.items()
    }


class UnknownDefaultsError(TypeError):
    def __init__(self, cls: type):
        super().__init__(f"Don't know how to get field defaults from {cls.__name__}")


def model_fields(cls: type) -> dict[str, ModelField]:
    """Get a generic ModelFields object for a named tuple, dataclass, or Pydantic model"""
    with contextlib.suppress(TypeError):
        return _pydmodel_fields(cls)
    with contextlib.suppress(TypeError):
        return _dataclass_fields(cls)
    with contextlib.suppress(TypeError):
        return _namedtuple_fields(cls)
    raise UnknownDefaultsError(cls)


def get_module_classes(module: types.ModuleType) -> dict[str, type]:
    """Get all classes contained in a module (not including any imported classes)"""
    return {
        item[0]: item[1]
        for item in inspect.getmembers(
            module,
            predicate=lambda obj: (
                inspect.isclass(obj) and inspect.getmodule(obj) == module
            ),
        )
    }
