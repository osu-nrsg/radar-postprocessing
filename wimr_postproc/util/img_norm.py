from typing import Literal, overload

import numpy as np
import numpy.typing as npt


@overload
def img_norm[T: np.floating](
    values: npt.NDArray[T],
    vmin: float,
    vmax: float,
    maxval: float = 1.0,
    *,
    clip: bool = True,
    copy: Literal[False] = ...,
    dtype: type[np.generic] | None = None,
) -> None: ...
@overload
def img_norm[T: np.floating](
    values: npt.NDArray[np.floating],
    vmin: float,
    vmax: float,
    maxval: float = 1.0,
    *,
    clip: bool = True,
    copy: Literal[True] = ...,
    dtype: type[T],
) -> npt.NDArray[T]: ...
@overload
def img_norm[T: np.floating](
    values: npt.NDArray[T],
    vmin: float,
    vmax: float,
    maxval: float = 1.0,
    *,
    clip: bool = True,
    copy: Literal[True] = ...,
    dtype: None = None,
) -> npt.NDArray[T]: ...
def img_norm(
    values: npt.NDArray[np.floating],
    vmin: float,
    vmax: float,
    maxval: float = 1.0,
    *,
    clip: bool = True,
    copy: bool = False,
    dtype: object | None = None,
) -> npt.NDArray[np.floating] | None:
    """Linearly normalize an array of values into the [0.0, maxval] interval.

    This is a somewhat simplified version of matplotlib.colors.Normalize.

    Parameters
    ----------
    values
        Array of values to normalize. This is modified in-place.
    vmin
        Minimum value of window.
    vmax
        Maximum value of window.
    maxval
        Maximum value of normalized data. Default is 1.0.
    clip
        If True (default) values outside the window are forced to 0.0 or maxval.
    copy
        If true, the normalization is performed on a copy of `values` and returned.
        Otherwise the `values` is normalized in-place.
    dtype
        If copy is true, this dtype will be used for the output array. Default is to match
        the input type.

    Returns
    -------
    values
        normalized values, if copy is True. Otherwise None.
    """
    if copy:
        if dtype is None:
            dtype = values.dtype
        output = values.astype(dtype)  # type: ignore
    else:
        output = values  # output *is* values

    # Operations:
    # - Optionally clip to vmin, vmax
    # - Scale such that vmin is 0 and vmax is maxval
    if clip:
        values = values.clip(vmin, vmax)
    output[:] = maxval * ((values - vmin) / (vmax - vmin))

    # Nothing to return if not copying
    return output if copy else None
