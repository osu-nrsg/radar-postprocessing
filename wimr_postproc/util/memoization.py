"""Tools to help with manually memoization/caching function inputs/outputs"""

from collections.abc import Callable
from typing import Any


class NotCached(Exception):
    """The hash is in the hash dict"""

    pass


def memoize_outputs(fn: Callable[..., Any], inputs_hash: int, outputs: Any) -> None:
    """Add a hash of function inputs and the resultant outputs to an internal attribute
    `_memoized` of a function."""
    try:
        fn._memoized[inputs_hash] = outputs  # type: ignore [attr-defined]
    except AttributeError:
        fn._memoized = {inputs_hash: outputs}  # type: ignore [attr-defined]


def get_memoized_outputs(fn: Callable[..., Any], hash: int) -> Any:
    """Use a hash of function inputs to Try to get memoized outputs from an internal
    attribute `_memoized` of a function.

    Raises NotCached exception if there
    """
    try:
        return fn._memoized[hash]  # type: ignore [attr-defined]
    except (AttributeError, KeyError):
        raise NotCached from None
