"""Utility functions used by this package."""

import os
from collections.abc import Iterator
from contextlib import contextmanager
from typing import Any, Literal


class OffsetSliceError(ValueError):
    def __init__(
        self,
        err_type: str,
    ) -> None:
        msgs = {
            "undef_start": "slice start is None and undef_start_ok is False",
            "minimum_start": (
                "When the slice start is None it indicates the beginning of a sequence"
                " and cannot be decreased"
            ),
            "start_sign": (
                "Slice start cannot be offset from negative to positive or vis-versa"
            ),
            "undef_stop": "slice stop is None and undef_stop_ok is False",
            "maximum_stop": (
                "When the slice stop is None it indicates the end of a sequence and"
                " cannot be increased"
            ),
            "stop_sign": (
                "Slice stop cannot be offset from negative to positive or vis-versa"
            ),
        }
        super().__init__(msgs[err_type])


def offset_slice(
    slc: slice, offset: int, undef_start_ok: bool = False, undef_stop_ok: bool = False
) -> slice:
    """Apply some offset to the start and stop limits of a slice, checking for some
    possible errors.

    Typically, it is undefined behavior to offset a slice with a start or stop of None.
    However:

    - If undef_start_ok is True, it is assumed that the start of the slice is defined as 0
      and can be added to (though not subtracted from).
    - If undef_stop_ok is True, it is assumed that the stop of the slice is defined as the
      last index of the sequence and may be subtracted from (though not added to).

    """

    def offset_slice_part(name: Literal["start", "stop"], offset: int, undef_ok: bool):
        value = getattr(slc, name)
        if value is None:
            if not undef_ok:
                raise OffsetSliceError(f"undef_{name}")
            if name == "start" and offset < 0:
                raise OffsetSliceError("minimum_start")
            elif name == "stop" and offset > 0:
                raise OffsetSliceError("maximum_stop")
            new_value = offset
        else:
            new_value = value + offset
            if (value >= 0) != (new_value >= 0):  # different signs not ok
                raise OffsetSliceError(f"{name}_sign")
        return new_value

    new_start = offset_slice_part("start", offset, undef_start_ok)
    new_stop = offset_slice_part("stop", offset, undef_stop_ok)
    return slice(new_start, new_stop, slc.step)


def uint_divisor(sum_factor: int, bit_depth_in: int, bit_depth_out: int = 8) -> float:
    """Get the factor or factors by which to divide summed radar pulses of some bit depth
    to make the values in the range 0-255.

    Parameters
    ----------
    sum_factor
        Pulse summing factor value or array of values (for each sweep)
    bit_depth_in
        Bit depth of the source data
    bit_depth_out
        Bit depth for the final data. Default is 8 (0-255).
    """
    bit_depth_max = 2**bit_depth_in - 1
    u8_max = 2**bit_depth_out - 1
    value_max = sum_factor * bit_depth_max
    return float(value_max / u8_max)


@contextmanager
def override_envvar(varname: str, override_val: Any) -> Iterator[None]:
    """Simple contextmanager to override an environment variable"""
    oldval = os.getenv(varname)
    os.environ[varname] = str(override_val)
    yield  # wait for with block to exit
    if oldval is None:
        del os.environ[varname]
    else:
        os.environ[varname] = oldval
