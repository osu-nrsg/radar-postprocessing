from datetime import datetime, timedelta

import numpy as np
import numpy.typing as npt


def dunwrap(
    deg: npt.NDArray[np.floating], discont: float = 180, axis: int = -1
) -> npt.NDArray[np.floating]:
    """Same as np.unwrap, but in degrees"""
    return np.unwrap(deg * np.pi / 180, discont * np.pi / 180, axis) * 180 / np.pi


def datetime2matlabdn(t_in: datetime) -> float:
    """Convert a python datetime (tz-aware or tz-naive) to a MATLAB datenum (same zone, if
    present in t_in)"""
    mdn = t_in + timedelta(days=366)
    td_from_midnight = t_in - datetime(*t_in.timetuple()[:3], tzinfo=t_in.tzinfo)
    frac_s = td_from_midnight.seconds / (24.0 * 60.0 * 60.0)
    frac_us = t_in.microsecond / (24.0 * 60.0 * 60.0 * 1.0e6)
    return mdn.toordinal() + frac_s + frac_us


def cart2pol(x, y):  # type: ignore [no-untyped-def]
    """Convert x, y into phi, r."""
    phi = np.arctan2(y, x)
    r = np.sqrt(x**2 + y**2)
    return phi, r


def pol2cart(phi, r):  # type: ignore [no-untyped-def]
    """Convert phi, r into x, y."""
    return r * np.cos(phi), r * np.sin(phi)


def phi2aziT(phi):  # type: ignore [no-untyped-def]
    """Convert angle(s) in radians CCW from East to degrees CW from North.

    Does not modulo 360.
    """
    return 90 - np.rad2deg(phi)


def aziT2aziH(azi_true, heading):  # type: ignore [no-untyped-def]
    """Convert azimuth CW from North to azimuth CW from radar heading of the same unit
    (typically degrees).

    Does not modulo 360.
    """
    return azi_true - heading


def aziH2azi0(azi_head, start_azi, mod360=True):  # type: ignore [no-untyped-def]
    """Convert azimuth CW from radar heading to azimuth CW from the configured sweep start
    azimuth.

    Does modulo 360 unless mod360 is set to False.
    """
    return np.mod(azi_head - start_azi, 360)


def phi2azi0(phi, heading, start_azi, mod360=True):  # type: ignore [no-untyped-def]
    """Convert angle[s] phi in radians CCW from East to degrees CW from the sweep start
    azimuth, which itself is relative to the radar heading.
    """
    return aziH2azi0(aziT2aziH(phi2aziT(phi), heading), start_azi, mod360)  # type: ignore [no-untyped-call]
