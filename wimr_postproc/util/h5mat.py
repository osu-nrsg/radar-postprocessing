"""h5mat.py - Helpful functions for reading data from HDF5-formatted MATLAB files"""

import posixpath
from typing import Any, Literal, Protocol, overload

import h5py
import numpy as np
import numpy.typing as npt

from wimr_postproc.util.types import (
    NDArr1D,
    NotGiven,
    NotGivenType,
    is_arr_dtype,
    is_ndarr1d,
)


class DatasetLike(Protocol):
    """Protocol for something resembling an h5py Dataset (e.g. a NumPy array)"""

    @property
    def shape(self) -> tuple[int, ...]: ...
    @property
    def ndim(self) -> int: ...
    @property
    def size(self) -> int | None: ...
    @property
    def dtype(self) -> Any: ...
    @property
    def nbytes(self) -> int: ...
    def astype(self, dtype: Any) -> Any: ...
    def __getitem__(self, key: Any, /) -> Any: ...


def _h5pathjoin(root: h5py.Group, path: str) -> str:
    """Get a clean path to a subgroup"""
    assert root.name  # this function doesn't work with anonymous groups
    return posixpath.join(root.name, path)


class H5MatError(Exception):
    pass


class NotADatasetError(H5MatError):
    """A path within a h5py File or Group is expected to be a dataset but is something
    else."""

    def __init__(
        self, root: h5py.Group, path: str, obj: h5py.Group | h5py.Datatype
    ) -> None:
        super().__init__(
            f"'{_h5pathjoin(root, path)}' is not a Dataset (Rather {type(obj).__name__})."
        )


class NonScalarDatasetError(H5MatError):
    """An h5py dataset is expected to be scalar but it is not."""

    def __init__(self, root: h5py.Group, path: str, ds: h5py.Dataset) -> None:
        super().__init__(
            f"{_h5pathjoin(root, path)} is not a scalar. (Shape is {ds.shape})."
        )


class NonVectorDatasetError(H5MatError):
    """An h5py dataset is expected to be 1-D but it is not."""

    def __init__(self, root: h5py.Group, path: str, ds: h5py.Dataset) -> None:
        super().__init__(
            f"{_h5pathjoin(root, path)} is not a vector. (Shape is {ds.shape})"
        )


class DatasetWrongTypeError(H5MatError, TypeError):
    """An h5py dataset is expected to be some type but it is a different type."""

    def __init__(
        self, root: h5py.Group, path: str, type_: type | np.dtype, val: Any
    ) -> None:
        expected_type = type_.name if isinstance(type_, np.dtype) else type_.__name__
        actual_type = (
            val.dtype.name if isinstance(val, np.ndarray) else type(val).__name__
        )
        super().__init__(
            f"{_h5pathjoin(root, path)} is not of type '{expected_type}' but rather"
            f" '{actual_type}'."
        )


class ProprietaryMatlabStringError(H5MatError):
    """An h5py dataset has the MATLAB string type which we don't know how to decode."""

    def __init__(self, root: h5py.Group, path: str) -> None:
        super().__init__(
            f"Don't know how to decode {_h5pathjoin(root, path)} of proprietary MATLAB"
            " string type."
        )


class NotAMatlabStringError(H5MatError):
    """An h5py dataset was expected to be MATLAB character array but it is something else."""

    def __init__(self, root: h5py.Group, path: str) -> None:
        super().__init__(f"{_h5pathjoin(root, path)} is not a string or string array.")


@overload
def dataset(
    root: h5py.Group, path: str, as_array: Literal[False] = False
) -> h5py.Dataset: ...
@overload
def dataset(root: h5py.Group, path: str, as_array: Literal[True]) -> np.ndarray: ...
@overload
def dataset(root: h5py.Group, path: str, as_array: bool) -> h5py.Dataset | np.ndarray: ...
def dataset(
    root: h5py.Group, path: str, as_array: bool = False
) -> h5py.Dataset | np.ndarray:
    """Get a dataset (ensuring type) from an h5py `Group` (or `File`). Basically a
    `TypeGuard` for `h5py.Group.__getitem__`.

    Parameters
    ----------
    root
        HDF5 group (or file)
    path
        string path to the variable in the file (allows group traversal) e.g.
        `'/group_a/dataset_B'`
    as_array
        If true, read the data into a numpy array and return that instead of an
        h5py.Dataset.

    Returns
    -------
    `Dataset` instance

    Raises
    ------
    KeyError
        The path does not point to a valid entity in the group
    NotADatasetError
        The path is valid but is to something other than a dataset (i.e. a group)
    """
    ds = root[path]
    if not isinstance(ds, h5py.Dataset):
        raise NotADatasetError(root, path, ds)
    return ds[()] if as_array else ds


def scalar(root: h5py.Group, path: str, default: Any | NotGivenType = NotGiven) -> Any:
    """Get a scalar from an N-D array with all singleton dimensions.

    Parameters
    ----------
    root
        h5py File or Group
    path
        Path to the dataset
    default
        value to return if the dataset doesn't exist

    Returns
    -------
    The scalar value at `root[path]`

    Raises
    ------
    KeyError
        The path does not point to a valid entity in the group
    NotADatasetError
        The path is valid but is to something other than a dataset.
    NonScalarDatasetError
        The dataset is not a scalar.
    """
    try:
        ds = dataset(root, path)
    except KeyError:
        if not isinstance(default, NotGivenType):
            return default
        raise
    try:
        return ds[...].item()  # works for both HDF5 scalars and N-D arrays of size 1
    except ValueError as err:
        if "array of size 1" in err.args[0]:
            raise NonScalarDatasetError(root, path, ds) from None
        raise


def scalarT[T: np.generic](
    root: h5py.Group,
    path: str,
    type_: type[T],
    cooerce: bool = True,
    default: T | NotGivenType = NotGiven,
) -> T:
    """Typed version of h5mat.scalar.

    Parameters
    ----------
    root
        h5py File or Group
    path
        Path to the dataset
    default
        value to return if the dataset doesn't exist
    type_
        If provided, ensure that the value is this type
    coerce_type
        If true, attempt to apply `type_` to whatever value is in the dataset. If `type_`
        is not provided, `coerce_type` has no effect.

    Returns
    -------
    The scalar value at `root[path]`

    Raises
    ------
    KeyError
        The path does not point to a valid entity in the group
    NotADatasetError
        The path is valid but is to something other than a dataset.
    NonScalarDatasetError
        The dataset is not a scalar.
    DatasetWrongTypeError
        `type_` is provided, `coerce` is `False`, and the type of the dataset is not the
        same as `type_`.
    ValueError
        `coerce` is `True` and the cooercion fails.
    """
    val = scalar(root, path, default)
    if cooerce:
        return type_(val)
    if not isinstance(val, type_):
        raise DatasetWrongTypeError(root, path, type_, val)
    return val


def vector(
    root: h5py.Group,
    path: str,
    default: npt.NDArray[np.generic] | NotGivenType = NotGiven,
) -> NDArr1D[np.generic]:
    """Get a 1-d NumPy array from an N-D HDF5 dataset with only one non-singleton
    dimension.

    Parameters
    ----------
    root
        h5py File or Group
    path
        Path to the dataset
    default
        1-D array to return if the dataset doesn't exist. Shape may be greater than 1-D
        if it "squeezes" down to 1-D.

    Returns
    -------
    The 1-D array at `root[path]`

    Raises
    ------
    KeyError
        The path does not point to a valid entity in the group
    NotADatasetError
        The path is valid but is to something other than a dataset.
    NonVectorDatasetError
        The dataset is not a 1-D array.
    """
    try:
        ds = dataset(root, path)
    except KeyError:
        if not isinstance(default, NotGivenType):
            default = default.squeeze()
            assert is_ndarr1d(default)
            return default
        raise
    vec: np.ndarray = ds[...].squeeze()
    if len(vec.shape) > 1:
        raise NonVectorDatasetError(root, path, ds)
    return vec


def vectorT[T: np.generic](
    root: h5py.Group,
    path: str,
    type_: type[T] | np.dtype[T],
    coerce: bool = True,
    default: npt.NDArray[T] | NotGivenType = NotGiven,
) -> NDArr1D[T]:
    """Typed version of h5mat.vector.

    Parameters
    ----------
    root
        h5py File or Group
    path
        Path to the dataset
    type\\_
        If provided, ensure that the value is this type. Must be a subclass of np.generic.
    coerce
        If true, use `astype()` to convert to `type_`. If `type_` is not provided,
        `coerce` has no effect.
    default
        1-D array to return if the dataset doesn't exist. Shape may be greater than 1-D
        if it "squeezes" down to 1-D.

    Returns
    -------
    The 1-D array at `root[path]`

    Raises
    ------
    KeyError
        The path does not point to a valid entity in the group
    NotADatasetError
        The path is valid but is to something other than a dataset.
    NonVectorDatasetError
        The dataset is not a 1-D array.
    DatasetWrongTypeError
        `type_` is provided, `coerce` is `False`, and the type of the dataset is not the
        same as `type_`.
    ValueError
        `coerce` is `True` and the cooercion fails.
    """
    vec = vector(root, path, default)
    if coerce:
        vec = np.astype(vec, type_)
    elif not is_arr_dtype(vec, type_):
        raise DatasetWrongTypeError(root, path, type_, vec)
    assert is_ndarr1d(vec)
    return vec


def _chararr2str(char_array: np.ndarray) -> str:
    """Decode a MATLAB char array as a string"""
    if char_array.dtype == "uint8":
        return char_array.tobytes().decode("utf_8")
    elif char_array.dtype == "uint16":
        return char_array.tobytes().decode("utf_16")
    elif char_array.dtype == "uint32":
        return char_array.tobytes().decode("utf_32")
    else:
        raise TypeError("Char array is unrecognized type.")


def matstr(
    root: h5py.Group,
    path: str,
    default: NotGivenType | str = NotGiven,
    row: int = 0,
) -> str:
    """Get a string from a MATLAB char array.

    Parameters
    ----------
    root
        h5py File or Group
    path
        Path to the dataset in `root`
    default
        String to return if the dataset doesn't exist in the group.
    row
        For multi-row MATLAB char arrays, return this row. Default is 0. (actually
        column in the HDF5 file)
    """
    try:
        ds = dataset(root, path)
    except KeyError:
        if not isinstance(default, NotGivenType):
            return default
        raise
    if ds.attrs["MATLAB_class"] == b"char":
        return _chararr2str(ds[:, row])
    elif ds.attrs["MATLAB_class"] == b"string":
        raise ProprietaryMatlabStringError(root, path)
    else:
        raise NotAMatlabStringError(root, path)
