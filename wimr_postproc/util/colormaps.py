"""_colormaps.py - Module to import colorcet and matplotlib colormaps and make a
Pydantic-compatible colormap type. Adapted from
<https://docs.pydantic.dev/latest/concepts/types/#handling-third-party-types>"""

from typing import Annotated, Any

import colorcet  # noqa: F401  # import registers colorcet maps with matplotlib
import matplotlib
from matplotlib.colors import Colormap
from pydantic import GetCoreSchemaHandler, GetJsonSchemaHandler
from pydantic.json_schema import JsonSchemaValue
from pydantic_core import core_schema


class _ColormapPydanticAnnotation:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        """
        We return a pydantic_core.CoreSchema that behaves in the following ways:

        - strs will be parsed as `Colormap` instances using the colormap registry
          matplotlib.colormaps
        - `Colormap` instances will be parsed as `Colormap` instances without
          any changes
        - Nothing else will pass validation
        - Serialization will always return the colormap name
        """

        def validate_from_str(value: str) -> Colormap:
            result = matplotlib.colormaps[value]
            return result

        from_str_schema = core_schema.chain_schema(
            [
                core_schema.str_schema(),
                core_schema.no_info_plain_validator_function(validate_from_str),
            ]
        )

        return core_schema.json_or_python_schema(
            json_schema=from_str_schema,
            python_schema=core_schema.union_schema(
                [
                    # check if it's an instance first before doing any further work
                    core_schema.is_instance_schema(Colormap),
                    from_str_schema,
                ]
            ),
            serialization=core_schema.plain_serializer_function_ser_schema(
                lambda instance: instance.name
            ),
        )

    @classmethod
    def __get_pydantic_json_schema__(
        cls, _core_schema: core_schema.CoreSchema, handler: GetJsonSchemaHandler
    ) -> JsonSchemaValue:
        # Use the same schema that would be used for `str`
        return handler(core_schema.str_schema())


PydColormap = Annotated[Colormap, _ColormapPydanticAnnotation]
