from pathlib import Path
from typing import Annotated

import typer

from wimr_postproc.imagery import plot_and_send_imagery
from wimr_postproc.params import ImageryGenParams, SpectraGenParams
from wimr_postproc.spectra import plot_and_send_spectra

app = typer.Typer(no_args_is_help=True)


@app.command()
def imagery_gen(
    cube_path: Annotated[
        Path,
        typer.Argument(
            exists=True,
            dir_okay=False,
            readable=True,
            help="Path to an azimuthally-interpolated WIMR cube file.",
        ),
    ],
    config_file: Annotated[
        Path,
        typer.Argument(
            exists=True,
            dir_okay=False,
            readable=True,
            help="Path to an ImageryGenParams TOML or YAML configuration file.",
        ),
    ],
):
    """Generate one or more plots of radar data and optionally send them to a remote
    destination via rsync.
    """
    # nested_config imported internally as it's not explicitly needed when just calling
    # the API
    import nested_config

    ig_params = ImageryGenParams.model_validate(
        nested_config.expand_config(config_file, ImageryGenParams)
    )
    plot_and_send_imagery(cube_path, ig_params)


@app.command()
def spectra_gen(
    cube_path: Annotated[
        Path,
        typer.Argument(
            exists=True,
            dir_okay=False,
            readable=True,
            help="Path to an Cartesian-interpolated WIMR cube file.",
        ),
    ],
    config_file: Annotated[
        Path,
        typer.Argument(
            exists=True,
            dir_okay=False,
            readable=True,
            help="Path to an SpectraGenParams TOML or YAML configuration file.",
        ),
    ],
):
    """Calculate one or more spectra of radar data, plot them, and optionally send them
    to a remote destination via rsync."""
    # nested_config imported internally as it's not explicitly needed when just calling
    # the API
    import nested_config

    spectra_params = SpectraGenParams.model_validate(
        nested_config.expand_config(config_file, SpectraGenParams)
    )
    plot_and_send_spectra(cube_path, spectra_params)


if __name__ == "__main__":
    app()
