"""cube.py - Classes used for accessing data from a MATLAB cube file.

Classes
-------
BaseCube
    Radar data from a MATLAB cube file - read in with h5py. (abstract base class)
PolarCube
    Subclass of BaseCube with data interpolated to a regular range/azimuth grid.
CartCube
    Subclass of BaseCube with data interpolated to a regular Cartesian grid.
UTMCoords
    Represent a single set of UTM coordinates.
InvalidUTMZoneError
    Exception for invalid zone vale of UTMCoords.

"""

import re
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from datetime import datetime, timezone
from pathlib import Path
from typing import Any, Self

import h5py
import numpy as np
import utm

from wimr_postproc.util import h5mat
from wimr_postproc.util.h5mat import DatasetLike
from wimr_postproc.util.types import F64Arr1D


class InvalidUTMZoneError(ValueError):
    def __init__(self, zone: Any) -> None:
        super().__init__(f"{zone} is not a valid UTM zone")


@dataclass(frozen=True)
class UTMCoords:
    """
    A single location represented in Universal Transverse Mercator coordinates.

    Attributes
    ----------
    easting
        The easting value of the UTM coordinate.
    northing
        The northing value of the UTM coordinate.
    zone
        The UTM zone, which includes the zone number and letter.

    Properties
    ----------
    latlon
        Tuple of (latitude, longitude) calcuated from the UTM coordinates.

    """

    easting: np.float64
    northing: np.float64
    zone: str
    zone_number: int = field(init=False)
    zone_letter: str = field(init=False)

    def __post_init__(self) -> None:
        """Set the zone_number and zone_letter attributes based on the zone attribute."""
        if not (match := re.fullmatch(r"(\d+)([A-Za-z])", self.zone)):
            raise InvalidUTMZoneError(self.zone)
        zone_parts = match.groups()
        object.__setattr__(self, "zone_number", int(zone_parts[0]))
        object.__setattr__(self, "zone_letter", zone_parts[1])
        try:
            utm.check_valid_zone(self.zone_number, self.zone_letter)
        except utm.OutOfRangeError as err:
            raise InvalidUTMZoneError(self.zone) from err

    @property
    def latlon(self) -> tuple[np.float64, np.float64]:
        """Return the latitude and longitude of this UTM coordinate"""
        return utm.to_latlon(  # type: ignore  # returns the specfied type
            self.easting, self.northing, self.zone_number, self.zone_letter
        )


@dataclass(frozen=True, kw_only=True)
class BaseCube(ABC):
    """Abstract Base class for PolarCube and CartCube - contains data from a "cube" MAT
    file.

    Attributes
    ----------
    data
        Interpolated intensity data of dimensions (rotation, azimuth, range)
    origin
        UTMCoords instance of the radar location
    heading
        Radar's installed zero azimuth, CW from North
    start_azi
        Reference azimuth CW from heading to delineate rotations
    station
        Short "station" name for the site
    long_name
        Longer site name
    full_A_file
        True if the cube comes from an ISR recording with a complete A*.txt file.
    run_length
        Total duration of the recording, in seconds

    """

    data: DatasetLike
    origin: UTMCoords
    heading: np.float64
    start_azi: np.float64
    station: str
    location: str | None
    long_name: str
    full_A_file: bool
    run_length: float
    _h5_file: h5py.File | None = None

    @staticmethod
    def base_h5matfile_kwargs(matfile: h5py.File, get_arrays: bool) -> dict[str, Any]:
        """Provides base-class kwargs for from_h5matfile."""
        return {
            "_h5_file": matfile,  # for context manager
            "data": h5mat.dataset(matfile, "/data", get_arrays),
            "origin": UTMCoords(
                h5mat.scalar(matfile, "/results/XOrigin"),
                h5mat.scalar(matfile, "/results/YOrigin"),
                h5mat.matstr(matfile, "/results/UTMZone"),
            ),
            "heading": h5mat.scalar(matfile, "/results/heading"),
            "start_azi": h5mat.scalar(matfile, "/results/startAzi", 0.0),
            "station": h5mat.matstr(matfile, "/station"),
            "location": h5mat.matstr(matfile, "/location", default="") or None,
            "long_name": h5mat.matstr(matfile, "/longName"),
            "full_A_file": bool(h5mat.scalar(matfile, "/results/fullAfile", True)),
            "run_length": h5mat.scalar(matfile, "/results/RunLength"),
        }

    @classmethod
    @abstractmethod
    def from_h5matfile(
        cls: type[Self], matfile: h5py.File, get_arrays: bool = False
    ) -> Self: ...

    @classmethod
    def from_matfile_path(
        cls: type[Self],
        mat_cube_path: Path,
        *,
        get_arrays: bool = False,
    ) -> Self:
        """Open an HDF5-formatted MAT file and return a Cube instance.

        Parameters
        ----------
        mat_cube_path
            Path to the MATLAB cube file.
        get_arrays
            If True, all fields are completely read from the MAT file and the MAT file is
            closed. Otherwise, the MAT file remains open for on-demand reading of some
            multi-dimensional fields like `data`.

        Returns
        -------
        cube
            An instance of the given subclass of BaseCube.

        If `get_arrays` is `False`, use cube in a context manager or otherwise call the
        `close` method on the cube to close the MAT file when no longer reading from it.
        E.g.:

        >>> with PolarCube.from_matfile_path("NewportTower_20240304_020003_pol.mat") as cube:
        >>>     ...  # read data from the cube

        --or--

        >>> cube = PolarCube.from_matfile_path("NewportTower_20240304_020003_pol.mat")
        >>> try:
        >>>    ...  # read data from the cube
        >>> finally:
        >>>     cube.close()

        If `get_arrays` is `True`, you can just read the cube and use it without worrying
        about closing the cube since all data is read at once. (Less memory-efficient, but
        works with multiprocessing without extra mpi stuff h5py requires for parallel
        HDF5).

        >>> cube = CartCube.from_matfile_path(
        >>>     "NewportTower_20240304_020003_cart_fftRegion.mat", get_arrays=True
        >>> )

        """
        if get_arrays:
            with h5py.File(mat_cube_path, "r") as h5_mat:
                cube = cls.from_h5matfile(h5_mat, True)
            return cube
        return cls.from_h5matfile(h5py.File(mat_cube_path, "r"), False)

    def close(self):
        if self._h5_file:
            self._h5_file.close()

    @property
    @abstractmethod
    def start_dt(self) -> datetime: ...

    @property
    @abstractmethod
    def end_dt(self) -> datetime: ...

    def __enter__(self):
        """If the cube is managing an HDF5 file it will have been opened in the
        from_matfile_path classmethod"""
        return self

    def __exit__(self, *args):
        self.close()


@dataclass(frozen=True, kw_only=True)
class PolarCube(BaseCube):
    """Data from a azimuthally-interpolated "cube" MAT file

    Attributes (* indicates specific to PolarCube)
    ----------
    ground_range*
        Vector of distances from the radar to each range bin, on the ground
    azi0*
        Vector of azimuth values relative to the reference starting azimuth
    azi_oob*
        Logical array of dimensions (rotation, azimuth) indicating which azimuths (if any)
        for a given rotation contain no data
    data
        Interpolated intensity data of dimensions (rotation, azimuth, range)*
    timeInt*
        Interpolated time data of dimensions (rotation, azimuth)
    origin
        UTMCoords instance of the radar location
    heading
        Radar's installed zero azimuth, CW from North
    start_azi
        Reference azimuth CW from heading to delineate rotations
    station
        Short "station" name for the site
    long_name
        Longer site name

    """

    ground_range: F64Arr1D
    azi0: F64Arr1D
    azi_oob: DatasetLike
    timeInt: DatasetLike

    def __str__(self):
        return (
            f"PolarCube from {self.station} of shape (nrot, n_azi, n_rg) = "
            f" {self.data.shape}"
        )

    @classmethod
    def from_h5matfile(
        cls: type[Self], matfile: h5py.File, get_arrays: bool = False
    ) -> Self:
        """Populate a PolarCube instance from an open h5py.File cube instance.

        Parameters
        ----------
        matfile
            The h5py File for the cube MAT file
        get_arrays
            If False (default), 2+d datasets are returned as h5py datasets. If true, they
            are read from the file and returned as numpy arrays.

        """
        base_kwargs = cls.base_h5matfile_kwargs(matfile, get_arrays)
        return cls(
            **base_kwargs,
            ground_range=h5mat.vectorT(matfile, "/Rg", np.float64),
            azi0=h5mat.vectorT(matfile, "/Azi", np.float64) - base_kwargs["start_azi"],
            azi_oob=h5mat.dataset(matfile, "/Azi_oob", get_arrays),
            timeInt=h5mat.dataset(matfile, "/timeInt", get_arrays),
        )

    @property
    def start_dt(self) -> datetime:
        return datetime.fromtimestamp(self.timeInt[0, 0], tz=timezone.utc)

    @property
    def end_dt(self) -> datetime:
        return datetime.fromtimestamp(self.timeInt[-1, -1], tz=timezone.utc)


@dataclass(frozen=True, kw_only=True)
class CartCube(BaseCube):
    """Data from a Cartesian-interpolated "cube" MAT file

    Attributes (* indicates specific to CartCube)
    ----------
    data
        Interpolated intensity data of dimensions (rotation, x, y)*
    origin
        UTMCoords instance of the radar location
    heading
        Radar's installed zero azimuth, CW from North
    start_azi
        Reference azimuth CW from heading to delineate rotations
    station
        Short "station" name for the site
    long_name
        Longer site name
    grid_name*
        Name given to the cube subregion (for plot title purposes)
    d_xy*
        Grid spacing, in meters
    grid_x*
        x-coordinate values for the data
    grid_y*
        y-coordinate values for the data
    time*
        (1-D) Starting time of each rotation
    site_wave_dir_range
        Start and end bound of azimuth range waves may come from (filter peaks outside
        of this range.)
    """

    grid_name: str
    d_xy: np.float64
    grid_x: F64Arr1D
    grid_y: F64Arr1D
    time: F64Arr1D
    site_wave_dir_range: tuple[float, float]
    maxL: float

    def __str__(self):
        return (
            f"CartCube from {self.station}-{self.grid_name} of shape (nrot, nx, ny) ="
            f" {self.data.shape}"
        )

    @classmethod
    def from_h5matfile(
        cls: type[Self], matfile: h5py.File, get_arrays: bool = False
    ) -> Self:
        """Populate a PolarCube instance from an open h5py.File cube instance.

        Parameters
        ----------
        matfile
            The h5py File for the cube MAT file
        get_arrays
            If False (default), 2+d datasets are returned as h5py datasets. If true, they
            are read from the file and returned as numpy arrays.

        """
        base_kwargs = cls.base_h5matfile_kwargs(matfile, get_arrays)
        swdr = h5mat.vectorT(
            matfile,
            "/site_wave_dir_range",
            np.float64,
            default=np.array([0.0, 360.0], dtype=np.float64),
        )
        return cls(
            **base_kwargs,
            grid_name=h5mat.matstr(matfile, "/grid_name"),
            d_xy=h5mat.scalar(matfile, "/d_xy"),
            grid_x=h5mat.vectorT(matfile, "/grid_x", np.float64),
            grid_y=h5mat.vectorT(matfile, "/grid_y", np.float64),
            time=h5mat.dataset(matfile, "/time")[:, 0],
            site_wave_dir_range=(swdr[0], swdr[-1]),
            maxL=h5mat.scalar(matfile, "/maxL", default=300.0),
        )

    @property
    def start_dt(self) -> datetime:
        """Start time of the cube, timezone-aware (UTC)"""
        return datetime.fromtimestamp(self.time[0], tz=timezone.utc)

    @property
    def end_dt(self) -> datetime:
        """Start time of the cube, timezone-aware (UTC)"""
        return datetime.fromtimestamp(self.time[-1], tz=timezone.utc)
