"""__init__.py - Root of the wimr_postproc package -- imports some public classes and
functions from submodules."""

from pathlib import Path as _Path

from single_version import get_version as _get_version

# from wimr_postproc.co_rect import corectify_cube
from wimr_postproc.errors import ImageCreationError, RadarPostProcError, SpectraGenError
from wimr_postproc.imagery import (
    image_create,
    image_from_pol_cube,
    plot_and_send_imagery,
    repeat_to_single_img_params,
)

# from wimr_postproc.kmz import make_and_send_kmz
from wimr_postproc.params import (
    CoGeoRectParams,
    CoRectParams,
    ImageryGenParams,
    KmzParams,
    SpectraGenParams,
    SpectralTimeseriesParams,
)
from wimr_postproc.spectra import (
    plot_and_send_spectra,
    plot_and_send_spectral_timeseries,
)

__version__ = _get_version(__name__, _Path(__file__).parent.parent)
