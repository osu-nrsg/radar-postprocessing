"""This module generates documentation for the configuration models in
`wimr_postproc.params`.

The idea is to generate markdown documentation for the configuration models in
`wimr_postproc.params` that can be included in the README. This is done by introspecting
the models and generating markdown documentation for each model.
"""

import inspect
import types
from enum import Enum
from typing import (
    Annotated,
    Any,
    Callable,
    List,
    Literal,
    NewType,
    TypeAliasType,
    Union,
    get_args,
    get_origin,
)

import markdown
import matplotlib.colors

from wimr_postproc.params import (
    ImageryGenParams,
    RepeatImageParams,
    RsyncParams,
    SingleImageParams,
)
from wimr_postproc.util.model_field_info import ModelField, NoDefault, model_fields


def _fmt_ann_args(ann: Any, base_modname: str | None = None, *, delim: str = ", ") -> str:
    """Format the args of an annotation"""
    return delim.join(format_object(arg, base_modname) for arg in get_args(ann))


def _format_bare_literal(literal, base_modname: str | None) -> str:
    return f"[{_fmt_ann_args(literal, base_modname)}]"


def _format_type_alias(type_alias: TypeAliasType, base_modname: str | None) -> str:
    if get_origin(type_alias.__value__) is Literal:
        # Aliased literal: AliasName[Literal_arg1, Literal_arg2, ...]
        return f"{type_alias}[{_fmt_ann_args(type_alias.__value__, base_modname)}]"
    else:
        return f"{type_alias}[{format_object(type_alias.__value__, base_modname)}]"


def _strip_annotated(annotated: Any, base_modname: str | None) -> str:
    """shed Annotated (just format the first arg)"""
    return format_object(get_args(annotated)[0], base_modname)


def _format_generic_alias(alias: Any, base_modname: str | None) -> str:
    return (
        f"{format_object(get_origin(alias), base_modname)}"
        f"[{_fmt_ann_args(alias, base_modname)}]"
    )


def _format_newtype(new_type: NewType, base_modname: str | None) -> str:
    return (
        f"{new_type.__qualname__}[{format_object(new_type.__supertype__, base_modname)}]"
    )


def _format_union(union: Any, base_modname: str | None) -> str:
    return _fmt_ann_args(union, base_modname, delim=" | ")


def _format_none(none: Any, base_modename: str | None) -> str:
    return "None"


def _format_enumval(enumval: Enum, base_modname: str | None) -> str:
    return f"{format_object(type(enumval), base_modname)}.{enumval.name}"


def _format_colormap(cmap: matplotlib.colors.Colormap, base_modname: str | None) -> str:
    return f"matplotlib.colormaps[{cmap.name}]"


def _format_type_or_callable(obj: Any, base_modname: str | None) -> str:
    if obj.__module__ in (
        None,
        base_modname,
        "pathlib",
        "__builtin__",
        "builtins",
        "exception",
    ):
        return obj.__qualname__
    else:
        return f"{obj.__module__}.{obj.__qualname__}"


_obj_format_lookup: list[
    tuple[
        Callable[
            [
                Any,
            ],
            bool,
        ],
        Callable[[Any, str | None], str],
    ]
] = [
    (lambda o: isinstance(o, TypeAliasType), _format_type_alias),
    (lambda o: get_origin(o) is Literal, _format_bare_literal),
    (lambda o: get_origin(o) is Annotated, _strip_annotated),
    (
        lambda o: isinstance(o, (type(List[int]), types.GenericAlias)),
        _format_generic_alias,
    ),
    (lambda o: isinstance(o, NewType), _format_newtype),
    (lambda o: get_origin(o) in (types.UnionType, Union), _format_union),
    (lambda o: o in (None, types.NoneType), _format_none),
    (lambda o: isinstance(o, Enum), _format_enumval),
    (lambda o: isinstance(o, matplotlib.colors.Colormap), _format_colormap),
    (lambda o: isinstance(o, (type, Callable)), _format_type_or_callable),
]


def format_object(obj: Any, base_modname: str | None = None) -> str:
    for o_test, formatter in _obj_format_lookup:
        if o_test(obj):
            return formatter(obj, base_modname)
    return repr(obj)


def format_default(field: ModelField):
    """Return a representation of the default value or default factory of a model field"""
    if field.default is not NoDefault:
        return f"Default: `{format_object(field.default)}`."
    elif field.default_factory is not NoDefault:
        return f"Default factory: `{format_object(field.default_factory)}`"
    else:
        return ""


imagery_header = """\
# Image Generation Configuration

A configuration is required in order to generate imagery from a WIMR cube file. This may
be provided as a TOML, YAML, or JSON configuration file to the `wimr-imagery` CLI or as an
`ImageryGenParams` object to `plot_and_send_imagery()`.

The fields in the configuration file must be as shown in the below sections that define
`ImageryGenParams` and the sub-structures it encloses. At the end of this file is an
example configuration file using TOML syntax.

"""

models: list[type] = [
    ImageryGenParams,
    SingleImageParams,
    RepeatImageParams,
    RsyncParams,
]

model_names: list[str] = [model.__name__ for model in models]


def _format_field_annotation(field: ModelField, base_modname: str):
    formatted = format_object(field.annotation, base_modname)
    if not field.optional:
        formatted += ", required."
    return f"<span class='classifier'>{formatted}</span>"


def format_model_md(model: type, base_modname: str):
    model_section_header = f'<h3 id="{model.__name__}">{model.__name__}</h3>'
    model_doc = inspect.cleandoc(model.__doc__ or "")
    # Generate markdown definition list syntax for each field
    field_definitions = [
        "\n".join(
            [
                f"{field_name}{_format_field_annotation(field, base_modname)}",
                f": {field.docstring} {format_default(field)}",
                "",
            ]
        )
        for field_name, field in model_fields(model).items()
    ]

    return "\n".join(
        [
            f"{model_section_header}",
            "",
            f"{model_doc}",
            "",
            "#### Fields",
            "",
            f"{'\n'.join(field_definitions)}",
        ]
    )


def gen_config_params_html(param_models: list[type], base_module_name: str, html_path):
    html_header = inspect.cleandoc(
        """
        <html>
        <head>
        <style type="text/css">
            body {
                font-family: BlinkMacSystemFont, Segoe UI, "Helvetica Neue", Arial, sans-serif;
            }
            dt {
                font-weight: 600;
            }
            .classifier:before {
                font-style: normal;
                margin: 0 0.5em;
                content: ":";
                display: inline-block;
            }
            .classifier { font-style: oblique; }
        </style>
        </head>
        <body>
        """
    )
    with open(html_path, "w") as htmlout:
        htmlout.write(html_header)
        for model in param_models:
            md = format_model_md(model, base_module_name)
            htmlout.write(markdown.markdown(md, extensions=["def_list"]))
        htmlout.write("</body></html>")


if __name__ == "__main__":
    gen_config_params_html(
        [
            ImageryGenParams,
            SingleImageParams,
            RepeatImageParams,
            RsyncParams,
        ],
        "wimr_postproc.params",
        "tmp.html",
    )
