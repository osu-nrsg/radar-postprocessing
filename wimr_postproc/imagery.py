"""image_create.py - Functions for generating imagery from radar data.

Public functions
----------------
image_from_pol_cube
    Plot radar intensity from a "polar" cube MAT file onto a rectilinear lat/lon grid.
repeat_to_single_img_params
    Convert an instance of RepeatImageParams into a list of SingleImageParams based
    on the repetition criteria and the cube time information.
image_create
    Create [a] radar plot[s] from an azimuthally-interpolated cube
plot_and_send_imagery
    Creates plots of radar imagery from a cube file and (optionally) sends the imagery
    to some destination with rsync.

"""

import contextlib
import json
import logging
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from datetime import datetime, timezone, tzinfo
from itertools import chain, repeat
from pathlib import Path
from subprocess import CalledProcessError
from typing import Any, Literal, NewType, Self, assert_never, cast

import numpy as np
import numpy.typing as npt
import pymap3d
import scipy.interpolate as sci_interp
import scipy.spatial
from matplotlib.colors import Colormap
from PIL import Image
from PIL.PngImagePlugin import PngInfo
from pydantic import FilePath, validate_call

from wimr_postproc._logging import log_called_process_error
from wimr_postproc.cube import PolarCube
from wimr_postproc.errors import image_create as ic_errors
from wimr_postproc.params import (
    ImageryGenParams,
    ImageTypeLiteral,
    RepeatImageParams,
    SingleImageParams,
)
from wimr_postproc.util import img_norm
from wimr_postproc.util.conversions import cart2pol, phi2azi0
from wimr_postproc.util.h5mat import DatasetLike
from wimr_postproc.util.memoization import (
    NotCached,
    get_memoized_outputs,
    memoize_outputs,
)
from wimr_postproc.util.types import F64Arr, Floating, Numeric, is_listof

JsonText = NewType("JsonText", str)

logger = logging.getLogger(__name__)

USE_TPE = True  # Use ThreadPoolExecutor


@dataclass
class _RotIdxs:
    """Validated rotation selection parameters."""

    snap_rot: int
    start_rot: int
    end_rot: int

    @classmethod
    def from_img_params(
        cls: type[Self], img_params: SingleImageParams, n_rot: int
    ) -> Self:
        """Create a _RotIdxs instance from image parameters and the known number of
        rotations in a cube."""
        # Check all fields are correct
        for param in ["snap_rot", "start_rot", "end_rot"]:
            val = getattr(img_params, param)
            if val is not None and val >= n_rot:
                raise ic_errors.InvalidRotationIndexError(param, val, n_rot)
        return cls(
            img_params.snap_rot,
            img_params.start_rot,
            img_params.end_rot if img_params.end_rot is not None else (n_rot - 1),
        )


@dataclass
class _ImageTimestrs:
    """Strings for various uses related to image time

    Members
    ----------
    date
        Date str for use in directory name (UTC) - "YYYY-mm-dd"
    img_timestamp
        image timestamp (for filenames) "YYYYmmdd_HHMMSS"
    iso_utc_timestamp
        ISO UTC datetime string - "YYYY-mm-ddTHH:MM:SSZ"
    local_timestr
        local friendly-format datetime string - "YYYY-mm-dd HH:MM:SS <tzname>"
    image_title
        title string for the image
    """

    date: str
    img_timestamp: str
    iso_utc_timestamp: str
    local_timestr: str
    image_title: str

    @classmethod
    def from_cube_time(
        cls: type[Self],
        cube_time: DatasetLike,
        img_type: ImageTypeLiteral,
        rot_idxs: _RotIdxs,
        user_timezone: tzinfo,
    ) -> Self:
        """Get the strings that are determined from the time and duration of the data

        Parameters
        ----------
        cube_time
            cube.timeInt - time (UNIX seconds) of each set of range bins in the cube
        img_type
            The type of image we're making
        rot_idxs
            Validated rotation selection indexes from image parameters
        user_timezone
            The timezone to use for the "local" timestring

        Returns
        -------
        _ImageTimestrs instance of the strings
        """
        if img_type == "snapshot":
            img_dt_utc = datetime.fromtimestamp(
                cube_time[rot_idxs.snap_rot, 0],
                tz=timezone.utc,
            )
            title_str = f"Rotation {rot_idxs.snap_rot}"
        else:
            img_dt_utc = datetime.fromtimestamp(
                cube_time[rot_idxs.end_rot, 0],
                tz=timezone.utc,
            )
            duration_min = (
                cube_time[rot_idxs.end_rot, -1] - cube_time[rot_idxs.start_rot, 0]
            ) / 60
            if img_type == "brightest":
                title_str = f"{duration_min:.1f}-min brightest-pixel image"
            elif img_type == "average":
                title_str = f"{duration_min:.1f}-min time exposure image"
            else:
                # Make sure we've addressed all image types
                assert_never(img_type)
        img_dt_local = img_dt_utc.astimezone(user_timezone)
        tzname = user_timezone.tzname(img_dt_utc) or str(user_timezone)
        return cls(
            date=f"{img_dt_utc:%Y-%m-%d}",
            img_timestamp=f"{img_dt_utc:%Y%m%d_%H%M%S}",
            iso_utc_timestamp=f"{img_dt_utc:%Y-%m-%dT%H:%M:%SZ}",
            local_timestr=f"{img_dt_local:%Y-%m-%d %H:%M:%S} {tzname}",
            image_title=title_str,
        )


def _image_pngname(
    station: str,
    img_timestamp: str,
    img_type: Literal["snapshot", "average", "brightest"],
) -> str:
    suffixes = {"snapshot": "snap", "average": "avg", "brightest": "bright"}
    return f"{station}_{img_timestamp}_{suffixes[img_type]}.png"


@dataclass
class _GeoGridLLRect:
    """A geographic grid, rectilinear in latitude and longitude.

    Attributes
    ----------
    north
        Maximum latitude boundary, in decimal degrees
    south
        Minimum latitude boundary, in decimal degrees
    east
        Maximum longitude boundary, in decimal degrees
    west
        Minimum longitude boundary, in decimal degrees
    n_lat
        Size of the latitude domain
    n_lon
        Size of the longitude domain
    origin_latlon
        (lat,lon) coordinates of a point to use as an origin when rotating the image.
    """

    north: Numeric
    south: Numeric
    east: Numeric
    west: Numeric
    n_lat: int
    n_lon: int
    center_latlon: tuple[Numeric, Numeric]

    @property
    def lat_dom(self) -> F64Arr:
        """Longitude domain of the grid"""
        return np.linspace(self.south, self.north, self.n_lat, dtype=np.float64)

    @property
    def lon_dom(self) -> F64Arr:
        """Latitude domain of the grid"""
        return np.linspace(self.west, self.east, self.n_lon, dtype=np.float64)

    @property
    def dlat(self) -> np.float64:
        """Grid latitude spacing"""
        return np.float64((self.north - self.south) / self.n_lat)

    @property
    def dlon(self) -> np.float64:
        """Grid longitude spacing"""
        return np.float64((self.east - self.west) / self.n_lat)

    @property
    def meshgrid(self) -> tuple[F64Arr, F64Arr]:
        """meshgrid of lon_dom and lat_dom"""
        grid_x, grid_y, *_ = np.meshgrid(self.lon_dom, self.lat_dom)
        return grid_x, grid_y

    @classmethod
    def from_image_spec(
        cls: type[Self],
        lat0: Numeric,
        lon0: Numeric,
        max_r: Numeric,
        d_xy: Numeric,
        lat_min: Numeric | None,
        lat_max: Numeric | None,
        lon_min: Numeric | None,
        lon_max: Numeric | None,
    ) -> Self:
        """Get a lat/lon grid given an origin, max radius, target spacing in meters, and
        optional lat/lon limits"""
        if lon_min is None:
            # Default: western boundary is max_r meters west of the origin. Determine the
            # longitude of that boundary.
            west_m = -max_r
            _, west, _ = pymap3d.enu2geodetic(west_m, 0, 0, lat0, lon0, 0)
        else:
            # If a minimum longitude is specified, that is the western boundary. Determine how
            # many meters that is from the origin.
            west = lon_min
            west_m, _ = pymap3d.geodetic2enu(lat0, west, 0, lat0, lon0, 0)
        # Similar for east, south, north.
        if lon_max is None:
            east_m = max_r
            _, east, _ = pymap3d.enu2geodetic(east_m, 0, 0, lat0, lon0, 0)
        else:
            east = lon_max
            east_m, _ = pymap3d.geodetic2enu(lat0, east, 0, lat0, lon0, 0)
        if lat_min is None:
            south_m = -max_r
            south, *_ = pymap3d.enu2geodetic(0, south_m, 0, lat0, lon0, 0)
        else:
            south = lat_min
            south_m, *_ = pymap3d.geodetic2enu(south, lon0, 0, lat0, lon0, 0)
        if lat_max is None:
            north_m = max_r
            north, *_ = pymap3d.enu2geodetic(0, north_m, 0, lat0, lon0, 0)
        else:
            north = lat_max
            north_m, *_ = pymap3d.geodetic2enu(north, lon0, 0, lat0, lon0, 0)
        # Use the size in meters of the E and N domains and the target pixel size in meters to
        # get the number of pixels in each dimension.
        n_lon = int((east_m - west_m) / d_xy)
        n_lat = int((north_m - south_m) / d_xy)
        return cls(north, south, east, west, n_lat, n_lon, center_latlon=(lat0, lon0))


def _image_grid(
    img_params: SingleImageParams,
    lat0: Floating,
    lon0: Floating,
    data_max_r: Floating,
    heading: Floating,
    start_azi: Floating,
) -> tuple[_GeoGridLLRect, npt.NDArray[np.floating], npt.NDArray[np.floating]]:
    """Get a grid in radar coordinates (azi0, r) and boundaries to use for plotting an
    image of radar data

    Parameters
    ----------
    img_params
        SingleImageParams including configured image boundaries and pixel size
    lat0
        Origin latitude
    lon0
        Origin longitude
    data_max_r
        Maximum range of the radar data
    heading
        Radar's zero azimuth
    start_azi
        Configured sweep starting azimuth for recording (relative to heading)

    Returns
    -------
    ll_grid
        Rectilinear lat/lon grid for the image
    grid_azi0
        Azimuth relative to start_azi for every grid location
    grid_r
        Range of every grid location
    """
    max_r = img_params.max_r if img_params.max_r is not None else data_max_r
    input_hash = hash(
        (
            lat0,
            lon0,
            max_r,
            img_params.d_xy,
            img_params.south,
            img_params.north,
            img_params.west,
            img_params.east,
            heading,
            start_azi,
        )
    )
    with contextlib.suppress(NotCached):
        # Use the memoized outputs if available
        return get_memoized_outputs(_image_grid, input_hash)  # type: ignore
    ll_grid = _GeoGridLLRect.from_image_spec(
        lat0,
        lon0,
        max_r,
        img_params.d_xy,
        img_params.south,
        img_params.north,
        img_params.west,
        img_params.east,
    )
    grid_lon, grid_lat = ll_grid.meshgrid
    grid_x, grid_y, _ = pymap3d.geodetic2enu(grid_lat, grid_lon, 0, lat0, lon0, 0)
    grid_phi, grid_r = cart2pol(grid_x, grid_y)  # type: ignore [no-untyped-call]
    grid_azi0 = phi2azi0(grid_phi, heading, start_azi)  # type: ignore [no-untyped-call]
    # memoize the output
    memoize_outputs(_image_grid, input_hash, (ll_grid, grid_azi0, grid_r))
    return ll_grid, grid_azi0, grid_r


@dataclass
class _SourceFrame:
    azi0: F64Arr
    rg: F64Arr
    intensity: np.ndarray

    @classmethod
    def from_cube(
        cls: type[Self],
        cube_data: DatasetLike,
        cube_azi0: DatasetLike,
        cube_azi_oob: DatasetLike,
        cube_rg: DatasetLike,
        rot_idxs: _RotIdxs,
        img_type: ImageTypeLiteral,
        img_max_r: Floating | None = None,
    ) -> Self:
        """Get the 2D (azi0, r) array of intensity data that will be plotted

        Parameters
        ----------
        cube_data
            cube.data - radar intensity
        cube_azi0
            Vector cube azimuths relative to start_azi i.e.:
                `cube_azi0 = (cube.Azi - cube.results.startAzi) % 360`
        cube_azi_oob
            cube.Azi_oob - boolean mask specifying azimuths in each rotation for which there
                        is no data.
        cube_rg
            cube.Rg - Ground range of radar intensity bins
        rot_idxs
            Validated rotation selection indexes from image parameters
        img_type
            Image type we're plotting, e.g. "snapshot" or "average", from image parameters
        img_max_r
            Configured maximum range to use for this image, if any, from image parameters

        Returns
        -------
        _SourceFrame
            azi0, rg, and intensity data to be used for plotting
        """
        if img_max_r is None:
            img_max_r = cube_rg[()].max()
        r_mask = (cube_rg[:] > 0) & (cube_rg[:] <= img_max_r)
        if img_type == "snapshot":
            azi_mask = ~cube_azi_oob[rot_idxs.snap_rot, :].astype(bool)
            raw_frame = cube_data[rot_idxs.snap_rot, azi_mask][:, r_mask]
        else:
            # end idx is inclusive
            rot_slc = slice(rot_idxs.start_rot, rot_idxs.end_rot + 1)
            azi_mask = ~cube_azi_oob[rot_slc, :].min(axis=0).astype(bool)
            img_data = cube_data[rot_slc, azi_mask, :][:, :, r_mask]
            if img_type == "brightest":
                raw_frame = img_data.max(axis=0)
            elif img_type == "average":
                raw_frame = img_data.mean(axis=0)
            else:
                assert_never(img_type)
        return cls(cube_azi0[azi_mask], cube_rg[r_mask], raw_frame)


def _interp_frame(
    frame_in: _SourceFrame, azi0_out: np.ndarray, rg_out: np.ndarray
) -> F64Arr:
    """Interpolate radar imagery from a rectilinear azi0,r grid to scattered (azi0, r)
    points

    Parameters
    ----------
    frame_in
        _SourceFrame instance: `azi0` and `rg` domains and `data` intensity values
    azi0_out
        1xN vector of azi0 coordinate values for output points
    rg_out
        1XN vector of rg coordinate values for output points

    Returns
    -------
    float64 array with dimensions `(len(azi0_out), len(rg_out))`
    """
    interpfn = sci_interp.RegularGridInterpolator(
        (frame_in.azi0, frame_in.rg), frame_in.intensity, bounds_error=False
    )
    return interpfn((azi0_out, rg_out))  # type: ignore  # assume the type is right for now.


def _create_frame(
    cube: PolarCube,
    grid_azi0: npt.NDArray[np.floating],
    grid_r: npt.NDArray[np.floating],
    img_params: SingleImageParams,
    rot_idxs: _RotIdxs,
) -> F64Arr:
    """Create a frame of intensity data on a lat/lon grid from a cube

    Parameters
    ----------
    cube
        The source data (typically from a WIMR "polar" cube MAT file)
    grid_azi0
        Azimuth values for output grid coordinates
    grid_r
        range values of output grid coordinates
    img_params
        Image generation parameters
    rot_idxs
        Validated rotation selection parameters

    Returns
    -------
    frame_out
        2-D float64 array of interpolated intensity data
    """
    frame_in = _SourceFrame.from_cube(
        cube.data,
        cube.azi0,
        cube.azi_oob,
        cube.ground_range,
        rot_idxs,
        img_params.img_type,
        img_params.max_r,
    )
    interp_data = _interp_frame(frame_in, grid_azi0.ravel(), grid_r.ravel())
    return interp_data.reshape(grid_r.shape)


def _png_descr_match(png_path: Path, other_descr: str) -> bool:
    """Check if the Description tEXt chunk of a PNG file (where we store JSON metadata)
    matches some string."""
    with Image.open(png_path) as im:
        png_descr = str(im.info.get("Description", ""))
        im.close()
    return other_descr == png_descr


def _plotframe2image(
    frame: F64Arr,
    clim: tuple[int, int],
    cmap: Colormap,
) -> Image.Image:
    """Convert a 2-D float array to a pillow Image.

    Parameters
    ----------
    frame
        Data array
    clim
        Minimum and maximum data values to use for normalization
    cmap
        matplotlib colormap to use for creating the RGBA values.

    Returns
    -------
    pillow Image object
    """
    frame_norm = img_norm.img_norm(frame, *clim, 255, copy=True)
    rgba = np.uint8(cmap(np.flipud(frame_norm) / 255) * 255)
    return Image.fromarray(rgba)


def _json4png(
    img_type: str,
    timestrs: _ImageTimestrs,
    rot_idxs: _RotIdxs,
    ll_grid: _GeoGridLLRect,
    site_longname: str,
    png_name: str,
) -> JsonText:
    """Serialize the JSON metadata to be included in the PNG's Description tEXt chunk"""
    img_meta: dict[str, Any] = {
        "type": img_type,
        "data_timestamp": timestrs.iso_utc_timestamp,
        "data_timestr": timestrs.local_timestr,
        "West": ll_grid.west,
        "East": ll_grid.east,
        "South": ll_grid.south,
        "North": ll_grid.north,
        "center_lat": ll_grid.center_latlon[0],
        "center_lon": ll_grid.center_latlon[1],
        "site_name": site_longname,
        "image_name": png_name,
    }
    if img_type == "snapshot":
        img_meta["snap_rot"] = rot_idxs.snap_rot
    else:
        img_meta["num_rot"] = rot_idxs.end_rot - rot_idxs.start_rot + 1
    return JsonText(json.dumps(img_meta))


def _image2png(
    image: Image.Image, png_path: Path, description: str | None = None
) -> None:
    """Create a PNG file from a PIL image, including an optional Description tEXt chunk"""
    png_path.parent.mkdir(exist_ok=True, parents=True)
    kwargs = {}
    if description:
        kwargs["pnginfo"] = PngInfo()
        kwargs["pnginfo"].add_text("Description", description)
    image.save(png_path, format="png", **kwargs)


def image_from_pol_cube(
    cube: PolarCube,
    img_params: SingleImageParams,
    img_dir: Path,
    date_dir: bool = True,
    replot: bool = True,
) -> tuple[Path, JsonText]:
    """Plot radar intensity from a "polar" cube MAT file onto a rectilinear lat/lon grid.

    Parameters
    ----------
    cube
        Data from a MAT file with azimuthally-interpolated radar data
    img_params
        Parameters for image creation
    img_dir
        Base directory for saving images
    date_dir
        If true, save images in a yyyy-mm-dd folder within img_dir
    replot
        If true, overwrite existing images with the same name

    Returns
    -------
    png_path
        Path to the generated PNG file
    png_json
        String of JSON metadata included in the image's Description tag.
    """
    rot_idxs = _RotIdxs.from_img_params(img_params, cube.data.shape[0])
    timestrs = _ImageTimestrs.from_cube_time(
        cube.timeInt,
        img_params.img_type,
        rot_idxs,
        img_params.local_timezone,
    )
    png_name = _image_pngname(cube.station, timestrs.img_timestamp, img_params.img_type)
    if date_dir:
        img_dir /= timestrs.date
    png_path = img_dir / png_name
    ll_grid, grid_azi0, grid_r = _image_grid(
        img_params,
        *cube.origin.latlon,
        cube.ground_range.max(),
        cube.heading,
        cube.start_azi,
    )
    plot_frame = _create_frame(cube, grid_azi0, grid_r, img_params, rot_idxs)
    png_json = _json4png(
        img_params.img_type,
        timestrs,
        rot_idxs,
        ll_grid,
        cube.long_name,
        png_name,
    )
    if not replot and png_path.is_file() and _png_descr_match(png_path, png_json):
        logger.info("%s already exists. Will not replot.", png_path)
        return png_path, png_json
    image = _plotframe2image(plot_frame, img_params.clim, img_params.cmap)
    _image2png(image, png_path, png_json)
    return png_path, png_json


def repeat_to_single_img_params(
    rpt_params: RepeatImageParams, cube_time: DatasetLike
) -> list[SingleImageParams]:
    """Convert an instance of RepeatImageParams into a list of SingleImageParams based
    on the repetition criteria and the cube time information.
    """
    duration_s = cube_time[-1, -1] - cube_time[0, 0]
    n_images = int(duration_s / rpt_params.period_s)
    img_params: list[SingleImageParams] = []
    for img_i in range(n_images):
        params_dict = rpt_params.model_dump()  # no deepcopy necessary
        search_start_t = cast(float, cube_time[0, 0] + img_i * rpt_params.period_s)
        search_end_t = cast(float, search_start_t + rpt_params.duration_s)
        # Find the indexes of the times in cube_time closest to search_start_t and
        # end_start_t.
        _, idxs = scipy.spatial.KDTree(cube_time[:, 0]).query(
            [search_start_t, search_end_t]
        )
        assert isinstance(idxs, np.ndarray)  # narrow type for checkers
        match rpt_params.img_type:
            case "snapshot":
                params_dict["snap_rot"] = idxs[0]
            case "average" | "brightest":
                params_dict["start_rot"] = idxs[0]
                params_dict["end_rot"] = idxs[1]
            case _:
                assert_never(rpt_params.img_type)
        del params_dict["period_s"]
        del params_dict["duration_s"]
        img_params.append(SingleImageParams.model_validate(params_dict))
    return img_params


def _save_json(cube: PolarCube, params: ImageryGenParams, images_json: str) -> Path:
    """Save the json metadata from the images to a file"""
    json_filename = f"{cube.station}_{cube.start_dt:%Y%m%d_%H%M}.json"
    if params.date_dir:
        json_path = params.img_dir / f"{cube.start_dt:%Y-%m-%d}" / json_filename
    else:
        json_path = params.img_dir / json_filename
    json_path.parent.mkdir(exist_ok=True, parents=True)
    with open(json_path, "w") as fobj:
        fobj.write(images_json)
    return json_path


@validate_call
def image_create(
    cube_path: FilePath, params: ImageryGenParams
) -> tuple[list[Path], Path | None, datetime]:
    """Create [a] radar plot[s] from an azimuthally-interpolated cube

    Parameters
    ----------
    cube_path
        Path to the cube file
    params
        Specification for what plots to make

    Returns
    -------
    image_paths
        A list of paths to each image
    json_path
        A path to the json file if params.make_json is True, otherwise None
    start_dt
        Starting datetime of the cube

    Raises
    ------
    ValidationError
        If the provided parameters are invalid.
    Exception (various)
        If there is some problem with the image creation

    """
    with PolarCube.from_matfile_path(cube_path) as cube:
        all_image_params = chain(
            params.images,
            chain.from_iterable(
                repeat_to_single_img_params(p, cube.timeInt) for p in params.repeat_images
            ),
        )
        if USE_TPE:
            tpe = ThreadPoolExecutor()
            image_paths, image_jsons = zip(
                *tpe.map(
                    image_from_pol_cube,
                    repeat(cube),
                    all_image_params,
                    repeat(params.img_dir),
                    repeat(params.date_dir),
                    repeat(params.replot),
                )
            )
        else:
            image_paths, image_jsons = zip(
                *[
                    image_from_pol_cube(
                        cube, ip, params.img_dir, params.date_dir, params.replot
                    )
                    for ip in all_image_params
                ]
            )
        json_path = (
            _save_json(cube, params, f"[{','.join(image_jsons)}]")
            if params.make_json
            else None
        )
        start_dt = cube.start_dt
    image_paths_l = list(image_paths)  # convert tuple to list
    assert is_listof(image_paths_l, Path)
    return image_paths_l, json_path, start_dt


def _rsync_imagery(
    params: ImageryGenParams,
    image_paths: list[Path],
    json_path: Path | None,
    start_dt: datetime,
) -> None:
    """Send imagery and the JSON file to some destination.

    If the rsync parameters are provided in `params`, rsync the image files to the
    specified destination. If `params.date_dir` is True, put the files in a dated
    subfolder based on the start time of the recording from which the images were
    generated."""
    if not params.rsync_params:
        return
    dest_subdir = start_dt.strftime("%Y-%m-%d") if params.date_dir else ""
    logger.info("Sending imagery and json files to campus.")
    fpaths = image_paths + [json_path] if json_path else image_paths
    try:
        params.rsync_params.send(fpaths, dest_subdir, check=True)
    except CalledProcessError as err:
        # Report rsync errors but do not stop execution.
        log_called_process_error(
            logger, err, "There was an error sending the imagery to campus."
        )


@validate_call
def plot_and_send_imagery(
    cube_path: FilePath, params: ImageryGenParams
) -> tuple[list[Path], Path | None]:
    """Creates plots of radar imagery from a cube file and (optionally) sends the imagery
    to some destination with rsync.

    Parameters
    ----------
    cube_path
        The path to the data cube from which imagery will be generated.
    params
        Parameters to dictate how the imagery should be created.

    Returns
    -------
    image_paths
        List of paths to the created images
    json_path
        Path to the created JSON file, if `params.make_json` is `True` (otherwise `None`)

    Raises
    ------
    ValidatationError
        If the provided parameters are invalid.
    ImageCreationError
        If the imagery generation fails.

    Examples
    --------
    >>> plot_and_send_imagery(Path('/path/to/cube'), ImageryGenParams(...))

    """
    try:
        image_paths, json_path, start_dt = image_create(cube_path, params)
    except Exception as exc:
        raise ic_errors.ImageCreationError(cube_path) from exc
    _rsync_imagery(params, image_paths, json_path, start_dt)
    return image_paths, json_path
