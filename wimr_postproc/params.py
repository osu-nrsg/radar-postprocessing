"""params.py - Single location for all parameters classes."""

import shlex
import subprocess
import sys
import zoneinfo
from pathlib import Path, PurePosixPath
from typing import Annotated, Any, Literal, NamedTuple, NewType, Self

import matplotlib
from pydantic import (
    BaseModel,
    DirectoryPath,
    Field,
    FilePath,
    field_validator,
    validate_call,
)
from typing_extensions import deprecated

from wimr_postproc.util import colormaps

Meters = NewType("Meters", float)
Degrees = NewType("Degrees", float)

type ImageTypeLiteral = Literal["snapshot", "average", "brightest"]
type NonNegativeInt = Annotated[int, Field(ge=0)]
type PositiveFloat = Annotated[float, Field(gt=0)]

# # FFR: how to make a frozen BaseModel that is hashable - use cls.__setattr__ in
# # validators if needed to set stuff.
# from typing import TYPE_CHECKING, dataclass_transform
# from pydantic import ConfigDict
# if TYPE_CHECKING:

#     @dataclass_transform(
#         frozen_default=True, kw_only_default=True, field_specifiers=(Field,)
#     )
#     class FrozenModel: ...
# else:

#     class FrozenModel(BaseModel):
#         model_config = ConfigDict(
#             frozen=True,
#         )


class SSHKeygenError(subprocess.CalledProcessError, ValueError):
    def __str__(self) -> str:
        str_ = super().__str__()
        if self.stderr:
            str_ += f" stderr: {self.stderr}"
        return str_


class SSHKeyHasPassphraseError(ValueError):
    def __init__(self, ssh_key: Path) -> None:
        super().__init__(
            f"'{ssh_key}' has a passphrase when it is expected to have none."
        )


class RsyncDestsFailed(ExceptionGroup):
    def __new__(cls, excs: list[Exception]) -> Self:
        return super().__new__(cls, "Transfer failed to all rsync destinations.", excs)

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)


class RsyncParams(BaseModel):
    """Simple class to rsync local files over ssh to a some remote system."""

    dest_user: str
    """Username on the destination machine."""
    dest_server: Annotated[list[str], Field(min_length=1)]
    """List of one or more destination servers to try for the rsync operation. (Ok to
    provide as a single string)"""
    ssh_key: FilePath
    """Path to an SSH key to use (must not have a passphrase)."""
    dest_base_dir: PurePosixPath = PurePosixPath("")
    """Base directory on the destination server prepended to `dest_subdir` if
    `dest_subdir` is not an absolute path. Default is "" (which usually means the user's
    home directory)."""
    options: str = ""
    """Options to pass to `rsync`."""
    ssh_options: str = ""
    """Options to use with `rsync -e "ssh ..."`. Already included:
    `-i <ssh_key> -o PasswordAuthentication=no`."""

    @field_validator("dest_server", mode="before")
    @classmethod
    def ensure_dest_server_list(cls, v: Any) -> list[str]:
        """Allow a single string argument by converting it to a list."""
        return [v] if isinstance(v, str) else v

    @field_validator("ssh_key")
    @classmethod
    def _ensure_passphraseless_ssh_key(cls, ssh_key: Path) -> Path:
        """Try to read the ssh key with an empty passphrase and raise an error if this
        fails."""
        cmd = f'ssh-keygen -y -P "" -f "{ssh_key}"'
        try:
            subprocess.run(
                shlex.split(cmd),
                check=True,
                stderr=subprocess.PIPE,
                stdout=subprocess.DEVNULL,
                encoding="utf-8",
            )
        except subprocess.CalledProcessError as cperr:
            if "incorrect passphrase" in cperr.stderr.lower():
                raise SSHKeyHasPassphraseError(ssh_key) from None
            else:
                raise SSHKeygenError(cperr.returncode, cmd, stderr=cperr.stderr) from None
        return ssh_key

    @validate_call
    def send(
        self,
        fpaths: FilePath | list[FilePath],
        dest_dir: str | PurePosixPath = "",
        check: bool = False,
    ) -> subprocess.CompletedProcess[str]:
        """Send some files using this rsync config

        Parameters
        ----------
        fpaths
            File or files to send. Directories are not allowed.
        dest_dir
            Destination directory on the rsync server. If dest_dir is relative (or not
            provided), it is first appended to `dest_base_dir`. Thus the default is to
            just write to `dest_base_dir`.
        check
            Raise a subprocess.CalledProcessError if the rsync command has a non-zero
            return code.

        Returns
        -------
        subprocess.CompletedProcess
            subprocess result object, with stdout and stderr included.
        """
        if isinstance(fpaths, (Path, str)):
            fpaths = [fpaths]
        fpaths_s = shlex.join(str(fp) for fp in fpaths)
        if not PurePosixPath(dest_dir).is_absolute():
            dest_dir = self.dest_base_dir / dest_dir
        # Only use the specified key for auth.
        ssh_options = (
            f"{self.ssh_options} -i '{self.ssh_key}'"
            " -o PasswordAuthentication=no -o IdentitiesOnly=yes"
        )
        rsync_cmd_template = (
            f'rsync {self.options} -e "ssh {ssh_options}"'
            f" {fpaths_s} "
            f'"{self.dest_user}@{{dest_server}}:{dest_dir}/"'
        )
        # try each server when there are multiple options
        excs = []
        for dest_server in self.dest_server:
            cmd = rsync_cmd_template.format(dest_server=dest_server)
            try:
                return subprocess.run(
                    cmd, shell=True, check=check, capture_output=True, encoding="utf-8"
                )
            except Exception as e:
                excs.append(e.with_traceback(sys.exc_info()[2]))
        # Only get here if we exhausted all of the servers and got a CalledProcessError
        # every time.
        if excs:
            raise RsyncDestsFailed(excs)
        # validation should preclude reaching this point.
        raise RuntimeError("No destination servers were specified.")


class SpectrumRoiParams(BaseModel):
    """Region of interest (AKA subimage) from which to calculate an integrated wavenumber
    spectrum."""

    center_x: float
    """UTM Eastings coordinate of center of the region Same zone as cube assumed."""
    center_y: float
    """UTM Northings coordinate of center of the region. Same zone as cube assumed."""
    name: str
    """Name to give the region."""
    size: int
    """Number of pixels per side. E.g. if side=128 and d_xy for the cube is 10, the roi
    will be 1280 m x 1280 m."""
    h: Annotated[
        float,
        deprecated("Average water depth is no longer used in spectral calculations."),
    ] = 0.0
    """Average water depth in the region."""


class SpectraGenParams(BaseModel):
    """Parameters for spectrum plot generation."""

    spectra_dir: Path
    """Path to where the spectra will be saved locally."""
    subimages: list[SpectrumRoiParams]
    """List of objects defining the boundaries of each region of interest from which a
    spectrum will be calcuated and plotted."""
    date_dir: bool = True
    """Should the spectra be saved in spectra_dir in a yyyy-mm-dd subfolder?"""
    no_replot: bool = True
    """If the plot already exists, should it be recreated?"""
    plot_timezone: zoneinfo.ZoneInfo = zoneinfo.ZoneInfo("UTC")
    """Timezone to use for the timestamps on the spectrum plots."""
    si_name_in_title: bool = True
    """Should the subimage name parameter be included in the title of each spectrum
    plot?"""
    wdir_is_heading: bool = False
    """Should the wave angle be the direction the waves are heading (true), or the
    direction from which the waves are coming (false, default)."""
    rsync_params: RsyncParams | None = None
    """Object with the configuration for how and where to send the plots after they have
    been generated."""

    @property
    def rois(self):
        """Regions of interest for spectrum generation. Alias for subimages."""
        return self.subimages


class SpectralTimeseriesParams(BaseModel):
    """Parameters for plotting spectral statistics timeseries."""

    days_to_plot: float
    """Number of days (can be fractional) of history to plot. Note the following behavior
    triggered by this value:
    - 2 or less:
        Plots have x-tick marks every 4 hours and no date is specified on the tick
        marks.
    - 7 or less (greater than 2):
        Plots have tick marks every 6 hours which display the date and time
    - Greater than 7
        Plots have tick marks every day, displaying the date only.
    """
    spectra_dir: Path
    """Path to search for dated subfolders containing the intParams files. Subfolders
    should be named in the format `yyyy-mm-dd`."""
    wdir_lims: tuple[float, float]
    """Limits of reasonable wave direction angles for the site (degrees from North). This
    should be relative to the wdir_is_heading parameter, below."""
    plot_timezone: str = "UTC"
    """(Optional) Timezone ID to use for the plot timestamps. Note that if the timezone is
    of the form 'US/Pacific', then the 'US/' will be stripped off for the plot title.
    Defaults to 'UTC'."""
    wdir_is_heading: bool = False
    """(Optional) Defines whether the wave angle is the direction the waves are heading
    (true), or the direction from which the waves are coming (false, default)."""
    rsync_params: RsyncParams | None = None
    """Optional RsyncParams object with the configuration for how and where to send the
    plots."""


class ImageParams(BaseModel):
    """Parameters for radar plot generation."""

    img_type: ImageTypeLiteral  # Required
    """Plot type: 'snapshot', 'average', or 'brightest'."""
    d_xy: float = 10.0
    """Grid spacing in meters."""
    max_r: float | None = None
    """Max range to plot (otherwise max data range)."""
    clim: tuple[int, int] = (0, 255)
    """Color axis limits. Default is [0, 255]."""
    cmap: colormaps.PydColormap = matplotlib.colormaps["cet_fire"]
    """matplotlib or colorcet colormap. Pydantic string validator included. If specifying
    a colorcet colormap, prefix with `"cet_"`, e.g. `"cet_fire"`."""
    west: float | None = None
    """Western longitude boundary to cut off plot."""
    east: float | None = None
    """Eastern longitude boundary to cut off plot."""
    south: float | None = None
    """Southern latitude boundary to cut off plot."""
    north: float | None = None
    """Northern latitude boundary to cut off plot."""
    local_timezone: zoneinfo.ZoneInfo = zoneinfo.ZoneInfo("UTC")
    """Timezone to use in image title. A string tz name will be converted to a ZoneInfo
    instance. E.g. `"America/Los_Angeles"`. Default is UTC."""


class SingleImageParams(ImageParams):
    """Parameters for generating a single image."""

    snap_rot: NonNegativeInt = 0
    """Rotation to plot for snapshot img_type."""
    start_rot: NonNegativeInt = 0
    """Start rot for average or brightest."""
    end_rot: NonNegativeInt | None = None
    """End rot for average or brightest (omit for last rot [default])."""


class RepeatImageParams(ImageParams):
    """Parameters for repeated plot generation."""

    period_s: PositiveFloat
    """Number of seconds between each plot."""
    duration_s: PositiveFloat = Field(default_factory=lambda data: data["period_s"])
    """Number of seconds to use for each plot if "average" or "brightest". Defaults to
    `period_s` if missing."""


class ImageryGenParams(BaseModel):
    """Parameters for plotting and optionally sending a group of radar images."""

    img_dir: Path
    """Path to save images."""
    images: list[SingleImageParams] = Field(default_factory=list)
    """List of SingleImageParams that specifies images to plot."""
    repeat_images: list[RepeatImageParams] = Field(default_factory=list)
    """List of RepeatImageParams that specifies a series of images to plot."""
    make_json: bool = True
    """Should a JSON file with info about each plot be generated?."""
    date_dir: bool = True
    """Should the imagery be saved in a yyyy-mm-dd subdir?."""
    rsync_params: RsyncParams | None = None
    """RsyncParams object that specifies where and how to sync imagery."""
    replot: bool = True
    """Should already-created images be replotted? Default is True."""


class KmzParams(BaseModel):
    """Arguments for polMat2TimexKmz.m. The actual configuration parameters are in
    kmz_params_path. See example_kmz_params.m for actual configuration of the script."""

    kmz_params_path: FilePath
    """Path to the configuration for polMat2TimexKmz.m."""
    kmz_dir: DirectoryPath | None
    """Optional directory in which to save KMZ file (cube's dir is used if not
    provided)."""
    rsync_params: RsyncParams | None = None
    """Optional `wimr_postproc.RsyncParams` instance with information on how and where
        to rsync the KMZ file after has been created."""


class RefCoords(NamedTuple):
    """Boundaries of feature to use for co[geo]rectification. d_azi should be smaller than
    cube azimuth resolution. Typical `d_azi` is 0.05 for 0.5 degree cube. d_range
    typically is larger, like 100m.
    """

    start_azi: Degrees
    """Starting azimuth boundary for reference grid."""
    end_azi: Degrees
    """Ending azimuth boundary for reference grid."""
    d_azi: Degrees
    """Azimuth spacing for reference grid."""
    start_range: Meters
    """Starting range boundary for reference grid."""
    end_range: Meters
    """Ending range boundary for reference grid."""
    d_range: Meters
    """Range spacing for reference grid."""


class CoRectParams(BaseModel):
    """Parameters for Corectification."""

    ref_coords: RefCoords
    """Corectification feature boundaries."""
    search_width: Degrees
    """Azimuth degrees to search outside the feature boundaries."""
    thresh_lag: Degrees
    """Maximum azimuth shift to allow."""
    do_overwrite: bool = True
    """Should the the data and timex fields in the cube be overwritten? (Default True.)"""
    interp_method: Literal[
        "linear", "nearest", "next", "previous", "spline", "pchip", "cubic", "maxima"
    ] = "spline"
    """Interpolation method to use for replotting shifted data."""


class CoGeoRectParams(CoRectParams):
    """Parameters for CoGeoRectification."""

    control_cube: FilePath
    """Reference "known-good" georectified cube."""
