"""Namespace for image_create module errors"""

from pathlib import Path

from wimr_postproc.errors import RadarPostProcError


class ImageCreationError(RadarPostProcError):
    def __init__(self, cube_path: Path | str) -> None:
        super().__init__(
            f"There was an error generating imagery from the cube file {cube_path}."
        )


class InvalidRotationIndexError(ImageCreationError, ValueError):
    def __init__(self, idx_name: str, idx_val: int, n_rot: int) -> None:
        super().__init__(
            f"Rotation selection parameter '({idx_name} = {idx_val})' indexes a rotation"
            " that is greater than the number of rotations in the cube"
            f" ({n_rot})."
        )
