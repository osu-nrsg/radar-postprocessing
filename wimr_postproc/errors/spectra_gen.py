from wimr_postproc.errors import RadarPostProcError


class SpectraGenError(RadarPostProcError):
    pass
