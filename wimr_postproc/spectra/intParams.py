from datetime import datetime, timedelta, timezone
from typing import Any

import numpy as np
import pandas as pd

from wimr_postproc.cube import CartCube
from wimr_postproc.params import SpectrumRoiParams
from wimr_postproc.spectra.datastructs import ROIBounds
from wimr_postproc.spectra.stats import SNRs

_kpeaks_types = {
    "xi": np.float64,
    "yi": np.float64,
    "fi": np.float64,  # new in v1.1
    "kx": np.float64,
    "ky": np.float64,
    "f": np.float64,  # new in v1.1
    "peak": np.float64,
    "peak_norm": np.float64,
    "k": np.float64,
    "l": np.float64,
    "ang": np.float64,
}

#: dtype of kpeaks structured array
kpeaks_dtype = np.dtype(list(_kpeaks_types.items()))

_subimages_types = {
    "center_x": np.float64,
    "center_y": np.float64,
    "name": pd.StringDtype,
    "size": np.float64,
    "h": np.float64,
    "UTMZone": pd.StringDtype,
    "nsname": pd.StringDtype,
    "side": np.float64,
    "x1": np.float64,
    "x2": np.float64,
    "y1": np.float64,
    "y2": np.float64,
    "kpeaks": np.object_,
    "int_peak": np.float64,
    "int_angpeak": np.float64,
    "int_kpeak": np.float64,
    "int_lpeak": np.float64,
    "int_Tpeak": np.float64,
    "clutter_AH82": np.float64,
    "snr_AH82": np.float64,
    "clutter_BSA94": np.float64,
    "snr_BSA94": np.float64,
    "valid": bool,
    "pngName": pd.StringDtype,
}

#: dtype of subimages structured array
subimages_dtype = np.dtype(list(_subimages_types.items()))


#: Mapping of peaks dataframe field names to kpeaks structured array field names
kxky_peaks_kpeaks_mapping = {
    "kx_i": "xi",
    "ky_i": "yi",
    "f_i": "fi",
    "value": "peak",
    "value_norm": "peak_norm",
    "L": "l",
    "azi": "ang",
}


def _kxky_peaks_to_kpeaks(kxky_peaks: pd.DataFrame) -> np.ndarray:
    return kxky_peaks.rename(columns=kxky_peaks_kpeaks_mapping).to_records(
        index=False, column_dtypes=_kpeaks_types
    )


type ROIResult = tuple[SNRs, pd.DataFrame, ROIBounds]


def get_intParams_subimages(
    rois: list[SpectrumRoiParams],
    rois_results: list[ROIResult],
    png_names: list[str],
    UTM_zone: str,
) -> np.ndarray:
    # Note: "subimages" here is not the same as SpectraGenParams.subimages, since now it's
    # a Numpy structured array.
    subimages = np.ndarray(len(rois), dtype=subimages_dtype)
    for roi_i, (roi, roi_results, png_name) in enumerate(
        zip(rois, rois_results, png_names, strict=True)
    ):
        snrs, kxky_peaks, roi_bounds = roi_results
        subimage = subimages[roi_i]
        for k, v in roi.model_dump() | snrs.subimages_vals | roi_bounds.subimages_vals:
            subimage[k] = v
        peak0 = (
            kxky_peaks[0]
            if len(kxky_peaks)
            else {k: np.nan for k in ["value", "azi", "k", "L", "T"]}
        )
        subimage["nsname"] = roi.name.replace(" ", "")
        subimage["UTMZone"] = UTM_zone
        subimage["int_peak"] = peak0["value"]
        subimage["int_angpeak"] = peak0["azi"]
        subimage["int_kpeak"] = peak0["k"]
        subimage["int_lpeak"] = peak0["L"]
        subimage["int_Tpeak"] = peak0["T"]
        subimage["valid"] = ~np.isnan(peak0["value"])
        subimage["pngName"] = png_name
        subimage["kpeaks"] = kxky_peaks.rename(
            columns=kxky_peaks_kpeaks_mapping
        ).to_records(index=False, column_dtypes=_kpeaks_types)
    return subimages
    # TODO - test me!


def epoch2Matlab(posix_time: float) -> float:
    """Convert a posix timestamp to a MATLAB datenum."""
    dt = datetime.fromtimestamp(posix_time, tz=timezone.utc)
    mdn = dt + timedelta(days=366)
    frac_seconds = (
        dt - datetime(dt.year, dt.month, dt.day, 0, 0, 0, tzinfo=timezone.utc)
    ).seconds / 86400
    frac_microseconds = dt.microsecond / (86400 * 1e6)
    return mdn.toordinal() + frac_seconds + frac_microseconds


def get_intParams(
    cube: CartCube,
    rois: list[SpectrumRoiParams],
    rois_results: list[ROIResult],
    png_names: list[str],
) -> dict[str, Any]:
    intParams = {
        "best_subimage": [],  # TODO
        "int_time": epoch2Matlab(cube.time[0]),
        "maxL": 0.0,  # TODO
        "runLength": cube.time[-1] - cube.time[0],
        "site": {
            "station": cube.station,
            "longName": cube.long_name,
            "x1": cube.grid_x[0],
            "x2": cube.grid_x[-1],
            "y1": cube.grid_y[0],
            "y2": cube.grid_y[-1],
            "UTMZone": cube.origin.zone,
        },
        "subimages": get_intParams_subimages(
            rois, rois_results, png_names, cube.origin.zone
        ),
    }
    if cube.location is not None:
        intParams["site"]["location"] = cube.location
    return intParams
