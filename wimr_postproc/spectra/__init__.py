"""wimr_postproc.spectra: Tools for generating and plotting spectral data.

This module provides tools for generating and plotting spectral data from WIMR cube files.
The main entry points are `plot_and_send_spectra` and `plot_and_send_spectral_timeseries`,
which generate and plot spectral data from a single cube file and a time series of cube
files, respectively.

The module is organized into the following submodules:

- `generation`: Functions for generating spectral data from WIMR cube files.
- `plotting`: Functions for plotting spectral data.
- `stats`: Functions for computing statistics on spectral data.
- `transforms`: Functions for transforming between different spectral representations.
- `main`: The main entry points for generating and plotting spectral data.

"""

from wimr_postproc.spectra.generation import get_spectral_data
from wimr_postproc.spectra.main import (
    plot_and_send_spectra,
    plot_and_send_spectral_timeseries,
)
from wimr_postproc.spectra.plotting import plot_fphi_spectrum, plot_kxky_spec_contour
from wimr_postproc.spectra.stats import find_kxky_peaks
from wimr_postproc.spectra.transforms import (
    fkphi_to_fphi,
    fkxky_to_f,
    fkxky_to_fkphi,
    fkxky_to_fphi,
    fkxky_to_kxky,
)
