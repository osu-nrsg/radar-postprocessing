"""spectra/constants.py - Maximum and minimum period and wavelength limits
for the spectra."""

from typing import Final

L_MIN: Final[int] = 30  # [m] minimum shallow-water wavelength to consider
L_MAX: Final[int] = 400  # [m] maximum shallow-water wavelength to consider
T_MIN: Final[int] = 5  # [s] minimum wave period to analyze
T_MAX: Final[int] = 20  # [s] maximum wave period to analyze

SPECTRUM_DPI: Final[int] = 120
SPECTRUM_PNG_H: Final[int] = 910
SPECTRUM_PNG_W: Final[int] = 900
