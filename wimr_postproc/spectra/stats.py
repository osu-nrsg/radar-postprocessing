import dataclasses
import itertools
from collections.abc import Iterator, Sequence

import numpy as np
import pandas as pd
from scipy.signal import find_peaks
from skimage.feature import peak_local_max

from wimr_postproc.spectra.datastructs import (
    HasSpecData,
    SpectrumF,
    SpectrumFKxKy,
    SpectrumKxKy,
)
from wimr_postproc.util.types import (
    F64Arr3D,
    NDArr2D,
    Nx2Arr,
    is_ndarr2d,
    is_nx2_arr,
)

type CoordArray = NDArr2D[np.int64]


@dataclasses.dataclass
class SNRs:
    clutter_AH82: float
    clutter_BSA94: float
    snr_AH82: float
    snr_BSA94: float

    @property
    def subimages_vals(self) -> dict[str, float]:
        """Dict of values to save to the intParams subimages struct."""
        return dataclasses.asdict(self)


def calc_SNRs(raw_data: F64Arr3D, cropped_data: F64Arr3D) -> SNRs:
    """Calculate the signal-to-noise ratio of the spectrum according to noise
    calculations from two different algorithms.

    In both cases the signal is considered to be the mean of the cropped spectrum (where
    we expect wave information).

    The first clutter algorithm (AH82) is from Alpers & Hasselmann (1982) "Spectral signal
    to clutter and thermal noise properties of ocean wave imaging synthetic aperture
    radars". In this case the clutter signal is considered to be the mean of the bottom 2%
    of the raw spectrum (meaning all values <= 2% of the max).

    The second clutter algorithm (BSA94) is from Brüning, Schmidt, & Alpers (1994) -
    "Estimation of the ocean wave-radar modulation transfer function from synthetic
    aperture radar imagery". In this case the clutter signal E_N is taken as

        E_N = mean(E_C) + 2σ_E_C

    where E_C is the spectral intensity of a region of wavenumbers where there is no wave
    information present. In the BSA paper this is taken as a 51x51 grid of wavenumbers
    centered on (k_x, k_y) = (0.475, 0). In Guillermo's original code he used a 33x33 grid
    in the corner of the spectrum, so (with 128 bins and d_xy = 10m) wavenumbers from
    -0.314 to -0.167, i.e. -20m to -37m. This seems a bit broad to me, but for now we'll
    stick with it. Another idea could be to use the information excluded from the cropped
    spectrum.

    Parameters
    ----------
    raw_data
        The full (not cropped) 3D (f, kx, ky) spectrum
    cropped_data
        The spectrum cropped in wavenumber and frequency.

    Returns
    -------
    snrs
        Dataclass of signal-to-noise ratios according to the two algorithms ("AH82" for
        Alpers & Hasselmann (1982) and and "BSA94" for Brüning, Schmidt, & Alpers (1994)).

    """
    signal = np.nanmean(cropped_data)

    clutter_AH82_mask = raw_data <= np.nanmax(raw_data) * 0.02
    clutter_AH82 = np.mean(raw_data[clutter_AH82_mask])

    clutter_data_BSA94 = raw_data[:, :33, :33]
    clutter_BSA94 = np.nanmean(clutter_data_BSA94) + 2 * np.nanstd(clutter_data_BSA94)

    return SNRs(
        float(clutter_AH82),
        float(clutter_BSA94),
        snr_AH82=float(signal / clutter_AH82),
        snr_BSA94=float(signal / clutter_BSA94),
    )


def _get_peaks(
    spec: HasSpecData,
    *,
    min_distance: int = 1,
    threshold_rel: float | None = None,
    thresh_abs_nstd: int | None = None,
) -> CoordArray:
    """Get the coordinates of peaks in a spectrum.

    This function finds peaks in the spectrum using the `peak_local_max` function from
    scikit-image. It handles NaN values and allows for setting thresholds based on either
    a relative value or a multiple of the standard deviation.

    Parameters
    ----------
    spec
        An object with a `data` attribute that is an N-D array of spectral data.
    min_distance
        Minimum distance between peaks. (Search window size is 2*min_distance + 1).
        Default is 1.
    threshold_rel
        Relative threshold value for peak_local_max. If None (default), an absolute
        threshold is used instead (see `thresh_abs_nstd`).
    thresh_abs_nstd
        Number of standard deviations above the mean to use as an absolute threshold if
        `threshold_rel` is None. If `None`, no threshold is used.

    Returns
    -------
    CoordArray
        A 2-D array of peak coordinates. The number of columns is equal to the number of
        dimensions in the spectrum data and the number of rows is equal to the number of
        peaks found.
    """

    spec_data = spec.data.copy()
    spec_data[np.isnan(spec_data)] = 0
    if threshold_rel is None and thresh_abs_nstd is not None:
        # float conversion makes pyright happy
        threshold_abs = float(np.mean(spec_data) + thresh_abs_nstd * np.std(spec_data))
    else:
        threshold_abs = None
    peak_coords = peak_local_max(
        spec_data,
        min_distance=min_distance,
        threshold_abs=threshold_abs,
        threshold_rel=threshold_rel,
    )
    assert is_ndarr2d(peak_coords, np.int64)
    return peak_coords


def hsplit_coord2(nx2_coords: Nx2Arr[np.int64]) -> tuple[list[int], list[int]]:
    """Split an n-by-2 array of coordinates into two lists."""
    a, b = zip(*nx2_coords)
    return list(a), list(b)


class IterPairsSizeError(ValueError):
    def __init__(self):
        super().__init__("A sequence of two or more elements is required.")


def _iter_pairs[T](seq: Sequence[T]) -> Iterator[tuple[T, T]]:
    """Iterate over the pairs of elements in a sequence of length greater than one,
    including finally the last and first element (unless there are only two items.)

    If a sequence with fewer than two elements is provided, an IterPairsSize error is
    raised.

    Examples
    --------
    >>> seq = ["A", "B", "C"]
    >>> print(list(_iter_pairs(seq)))
    [('A', 'B'), ('B', 'C'), ('C', 'A')]

    >>> seq = ["A", B"]
    >>> print(list(_iter_pairs(seq)))
    [('A', 'B')]

    >>> seq = ["A"]
    >>> print(list(_iter_pairs(seq)))
    IterPairsSizeError: A sequence of two or more elements is required.
    """
    if len(seq) < 2:
        raise IterPairsSizeError
    yield from itertools.pairwise(seq)
    if len(seq) > 2:
        yield seq[-1], seq[0]


def _azi_diff(azi_a: float, azi_b: float, circle_unit: float = 360.0) -> float:
    """Return the smallest difference between two angles (irrespective of order).

    By default this function works with degrees. To work with radians use
    `circle_unit`=2*np/pi.

    """
    return min((azi_a - azi_b) % circle_unit, (azi_b - azi_a) % circle_unit)


def _filter_peaks_angular(peaks: pd.DataFrame, min_angle: float):
    """Iterate over every pair of peaks and if they are closer than min_angle, only keep
    the larger. The peaks are first sorted by azimuth."""
    peaks = peaks.sort_values(by="azi")
    peaks_to_remove = []
    for (peak_a_i, peak_a), (peak_b_i, peak_b) in _iter_pairs(list(peaks.iterrows())):
        azi_diff = _azi_diff(peak_a["azi"], peak_b["azi"])
        if azi_diff < min_angle:
            peaks_to_remove.append(
                peak_a_i if peak_a["value"] < peak_b["value"] else peak_b_i
            )
    return peaks.drop(peaks_to_remove)


def find_kxky_peaks(
    spec_kxky: SpectrumKxKy,
    spec_fkxky: SpectrumFKxKy,
    *,
    threshold_rel: float = 0.5,
    min_azi: float = 0.0,
) -> pd.DataFrame:
    """Find peaks in a kxky spectrum. Peaks closer to each other than min_azi are
    filtered (only the larger of the two is retained).

    Parameters
    ----------
    spec_kxky
        The frequency-integrated (kx, ky) spectrum
    spec_fkxky
        The full 3d spectrum, used to get the frequency of each peak.
    threshold_rel
        Only keep peaks this fraction of the maximum.
    min_azi
        The minimum angle between peaks, in degrees.

    Returns
    -------
    peaks
        A dataframe where each row represents a found peak in the spectrum.

        Columns:

        - ky_i, ky_i, f_i - indices of the peak in the spectra
        - f - Frequency of the peak
        - T - period of the peak
        - kx, ky - wavenumber of the peak in x & y
        - k - magnitude of the wavenumber of the peak
        - L - period the peak
        - phi - Polar angle of the peak (radians CCW from E)
        - azi - Azimuth of the peak (degrees CW from N)
        - value - value of the spectrum at the peak
        - norm_value - value normalized by the maximum of the spectrum

    """
    peak_coords = _get_peaks(spec_kxky, threshold_rel=threshold_rel)
    assert is_nx2_arr(peak_coords, np.int64)
    peaks = pd.DataFrame()
    peaks["kx_i"], peaks["ky_i"] = hsplit_coord2(peak_coords)
    peaks["f_i"] = [
        np.nanargmax(spec_fkxky.data[:, kx_i, ky_i])
        for (kx_i, ky_i) in zip(peaks["kx_i"], peaks["ky_i"])
    ]
    peaks["f"] = spec_fkxky.f[peaks["f_i"]]
    peaks["T"] = 1 / peaks["f"]
    peaks["kx"] = spec_kxky.k[peaks["kx_i"]]
    peaks["ky"] = spec_kxky.k[peaks["ky_i"]]
    peaks["k"] = np.sqrt(peaks["kx"] ** 2, peaks["ky"] ** 2)  # type: ignore [call-overload]
    peaks["L"] = 2 * np.pi / peaks["k"]
    peaks["phi"] = np.mod(np.atan2(peaks["ky"], peaks["kx"]), 2 * np.pi)
    peaks["azi"] = np.mod(90 - np.rad2deg(peaks["phi"]), 360.0)
    peaks["value"] = spec_kxky.data[peaks["kx_i"], peaks["ky_i"]]
    peaks["norm_value"] = peaks["value"] / np.nanmax(spec_kxky.data)
    if len(peaks) > 1 and min_azi > 0:
        peaks = _filter_peaks_angular(peaks, min_azi)
    peaks = peaks.sort_values(by="norm_value", ascending=False)
    return peaks


def find_f_peaks(spec_f: SpectrumF):
    """Find peaks in a wavenumber-integrated frequency spectrum.

    Parameters
    ----------
    spec_f
        The wavenumber-integrated, one-sided frequency spectrum.

    Returns
    -------
    peaks
        A Pandas dataframe with each row representing a peak in the spectrum.

        Columns:
        - f_i - Index of the frequency of the peak
        - f - Frequency of the peak
        - T - period of the peak
        - value - value of the spectrum at the peak
        - norm_value - Value normalized by the maximum of the spectrum

    """
    data_max = np.nanmax(spec_f.data)
    peak_locations, _ = find_peaks(spec_f.data, height=data_max * 0.5, distance=5)
    peaks = pd.DataFrame()
    peaks["f_i"] = peak_locations
    peaks["f"] = spec_f.f[peak_locations]
    peaks["T"] = 1 / peaks["f"]
    peaks["value"] = spec_f.data[peak_locations]
    peaks["norm_value"] = peaks["value"] / data_max
    peaks = peaks.sort_values(by="norm_value", ascending=False)
    return peaks
