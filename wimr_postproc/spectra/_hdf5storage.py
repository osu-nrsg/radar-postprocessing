"""_hdf5storage.py - Monkey-patches numpy to restore np.unicode_ before import of
hdf5storage, which depends on that old name still."""

import numpy as np
from packaging.version import Version

if Version(np.__version__) >= Version("2.0.0"):
    np.unicode_ = np.str_  # type: ignore # enable backward compatibility for hdf5storage
import hdf5storage as hdf5storage  # noqa: E402
