"""spectra/datastructs.py - Data structures for spectra calculations."""

from dataclasses import dataclass
from typing import Protocol

import numpy as np
import numpy.typing as npt

from wimr_postproc.util.types import F64Arr1D, F64Arr2D, F64Arr3D


class HasSpecData(Protocol):
    @property
    def data(self) -> npt.NDArray[np.float64]: ...


@dataclass
class ROIBounds:
    """Calculated bounds of the region of interest

    Attributes
    ----------
    side
        Length of the side of the region of interest (in meters)
    x_i
        x indices of the roi in the cube data.
    y_i
        y indices of the roi in the cube data.
    x1
        West boundary of subimage, in UTM eastings.
    x2
        East boundary of subimage, in UTM eastings.
    y1
        South boundary of subimage, in UTM northings.
    y2
        North boundary of subimage, in UTM northings.
    """

    side: float
    x_i: slice | list[int]
    y_i: slice | list[int]
    x1: float
    x2: float
    y1: float
    y2: float

    @property
    def subimages_vals(self) -> dict[str, float]:
        """Dict of values to save to the intParams subimages struct."""
        return {n: getattr(self, n) for n in ["side", "x1", "x2", "y1", "y2"]}


@dataclass
class SpectrumFKxKy:
    """3-D (f, kx, ky) spectrum

    Attributes
    ----------
    data
        The spectral data. One-sided in frequency, kx and ky centered on 0.
    f
        The frequency domain.
    k
        The wavenumber domain (same for kx and ky)

    """

    data: F64Arr3D
    f: F64Arr1D
    k: F64Arr1D


@dataclass
class SpectrumF:
    """Wavenumber-integrated 1-D (f) spectrum

    Attributes
    ----------
    data
        The spectral data, one-sided in frequency.
    f
        The frequency domain.

    """

    data: F64Arr1D
    f: F64Arr1D


@dataclass
class SpectrumKxKy:
    """Frequency-integrated 2-D (kx, ky) spectrum

    Attributes
    ----------
    data
        The spectral data. kx and ky centered on 0.
    k
        The wavenumber domain (same for kx and ky)

    """

    data: F64Arr2D
    k: F64Arr1D


@dataclass
class SpectrumFKPhi:
    """3-D (f, k, phi) spectrum

    Attributes
    ----------
    data
        The spectral data. One-sided in frequency, kx and ky interpolated to k and phi.
    f
        The frequency domain.
    k
        The positive wavenumber domain.
    phi
        The wavenumber direction domain.
    """

    data: F64Arr3D
    f: F64Arr1D
    k: F64Arr1D
    phi: F64Arr1D


@dataclass
class SpectrumFPhi:
    """Wavenumber-integrated 2-D (f, phi) spectrum

    Attributes
    ----------
    data
        The spectral data. One-sided in frequency.
    f
        The frequency domain.
    phi
        The wavenumber direction domain.
    """

    data: F64Arr2D
    f: F64Arr1D
    phi: F64Arr1D
