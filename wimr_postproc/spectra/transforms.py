"""Functions to transform 3D f,kx,ky spectra to other forms"""

import numpy as np
import numpy.typing as npt
from scipy.interpolate import RegularGridInterpolator

from wimr_postproc.spectra.datastructs import (
    SpectrumF,
    SpectrumFKPhi,
    SpectrumFKxKy,
    SpectrumFPhi,
    SpectrumKxKy,
)
from wimr_postproc.util.conversions import cart2pol, pol2cart
from wimr_postproc.util.types import is_F64Arr1D, is_F64Arr2D, is_ndarr1d


def _zero_nans_with_siblings(
    arr: npt.NDArray[np.floating], *, axis: int = 0, copy: bool = True
) -> npt.NDArray[np.floating]:
    """Replace each NaN value in a numpy array with zero if and only if there is at least
    one non-NaN value at that same location along a specified axis.

    Created with the help of ChatGPT. -Randy

    Parameters
    ----------
    arr
        Input array
    axis
        Axis along which to search for non-NaN values when deciding to set to zero.
    copy
        If True (default) the replacement is performed on a copy of `arr` and that result
        is returned. If False, the replacement is performed on the input array itself.

    Returns
    -------
    numpy.ndarray
        The modified array (or a copy of it, see `copy`) with NaNs replaced as specified.

    """
    nan_mask = np.isnan(arr)
    # Along axis `axis`, get a mask of locations with at least one non-NaN value
    valid_mask = np.any(~nan_mask, axis=axis, keepdims=True)
    # Create a mask of locations that are NaN but also for which there is at least one
    # non-NaN value along axis `axis`.
    zero_mask = nan_mask & valid_mask
    # Replace NaNs in a copy or in-place
    out = arr.copy() if copy else arr
    out[zero_mask] = 0
    return out


def fkxky_to_kxky(spectrum: SpectrumFKxKy) -> SpectrumKxKy:
    """Calculate the frequency-integrated spectrum.

    Parameters
    ----------
    spectrum
        3D (f, kx, ky) spectrum

    Returns
    -------
    spectrum_kxky
        The frequency-integrated 2-D (kx, ky) spectrum

    """
    nanzeroed_data = _zero_nans_with_siblings(spectrum.data)
    spec_data_kxky = np.trapezoid(y=nanzeroed_data, x=spectrum.f, axis=0)
    assert is_F64Arr2D(spec_data_kxky)
    return SpectrumKxKy(spec_data_kxky, spectrum.k)


def fkxky_to_f(spectrum: SpectrumFKxKy) -> SpectrumF:
    """Calculate the wavenumber-integrated spectrum.

    Parameters
    ----------
    spectrum
        3D (f, kx, ky) spectrum

    Returns
    -------
    spectrum_f
        The wavenumber-integrated 1-D (f) spectrum

    """
    nanzeroed_3D_data = _zero_nans_with_siblings(spectrum.data, axis=2)
    spec_data_fkx = np.trapezoid(y=nanzeroed_3D_data, x=spectrum.k, axis=2)
    assert isinstance(spec_data_fkx, np.ndarray)  # for type checkers
    nanzeroed_2D_data = _zero_nans_with_siblings(spec_data_fkx, axis=1)
    spec_data_f = np.trapezoid(y=nanzeroed_2D_data, x=spectrum.k, axis=1)
    assert is_F64Arr1D(spec_data_f)
    return SpectrumF(spec_data_f, spectrum.f)


def fkxky_to_fkphi(
    spectrum: SpectrumFKxKy, *, d_phi: float | None = None
) -> SpectrumFKPhi:
    """Interpolate a 3-D (f, kx, ky) spectrum to a 3-D (f, k, phi) spectrum. Note that
    k==0 is excluded.

    Parameters
    ----------
    spectrum
        3D (f, kx, ky) spectrum
    d_phi
        Optional angular spacing for the grid. The default is to determine the angle
        between two coordinates in the corner of the (kx, ky) grid.

    Returns
    -------
    SpectrumFKPhi
        3D (f, k, phi) spectrum
    """
    kx = spectrum.k
    ky = spectrum.k
    if d_phi is None:
        # Determine dphi using the angle betwen two points in the top-right corner of the
        # domain
        x0, y0 = kx[-1], ky[-2]
        x1, y1 = kx[-2], ky[-1]
        phi0, _ = cart2pol(x0, y0)  # type: ignore [no-untyped-call]
        phi1, _ = cart2pol(x1, y1)  # type: ignore [no-untyped-call]
        d_phi = float(phi1 - phi0)
    n_phi = int(2 * np.pi / d_phi)
    phi = np.linspace(0, 2 * np.pi, n_phi, endpoint=False, dtype=np.float64)
    k_pol = spectrum.k[spectrum.k > 0]
    grid_phi, grid_k = np.meshgrid(phi, k_pol)
    grid_x, grid_y = pol2cart(grid_phi, grid_k)  # type: ignore [no-untyped-call]
    n_f = len(spectrum.f)
    n_k = len(k_pol)
    spec_data_fkphi = np.zeros((n_f, n_k, n_phi), dtype=np.float64)
    for f_i in range(n_f):
        data = spectrum.data.copy()
        data[np.isnan(data)] = 0
        f_int = RegularGridInterpolator((kx, ky), data[f_i, :, :], method="slinear")
        spec_data_fkphi[f_i, :, :] = f_int((grid_x.ravel(), grid_y.ravel())).reshape(
            (n_k, n_phi)
        )
    assert is_ndarr1d(k_pol) and is_ndarr1d(phi)  # until numpy can preserve shape
    return SpectrumFKPhi(spec_data_fkphi, spectrum.f, k_pol, phi)


def fkphi_to_fphi(spec_fkphi: SpectrumFKPhi) -> SpectrumFPhi:
    """Integrate a 3-D (f, k, phi) spectrum to (f, phi)."""
    data_fkphi = spec_fkphi.data.copy()
    nanzeroed_data = _zero_nans_with_siblings(data_fkphi, axis=1)
    data_fphi = np.trapezoid(y=nanzeroed_data, x=spec_fkphi.k, axis=1)
    assert is_F64Arr2D(data_fphi)
    return SpectrumFPhi(data_fphi, spec_fkphi.f, spec_fkphi.phi)


def fkxky_to_fphi(
    spec_fkxky: SpectrumFKxKy, *, d_phi: float | None = None
) -> SpectrumFPhi:
    """Convert and integrate a 3-D (f, kx, ky) spectrum to (f, phi)"""
    spec_fkphi = fkxky_to_fkphi(spec_fkxky, d_phi=d_phi)
    return fkphi_to_fphi(spec_fkphi)
