import logging
from datetime import datetime
from pathlib import Path

import pandas as pd
import utm
from matplotlib import pyplot as plt
from pydantic import FilePath, validate_call

from wimr_postproc.cube import CartCube
from wimr_postproc.params import (
    SpectraGenParams,
    SpectralTimeseriesParams,
    SpectrumRoiParams,
)
from wimr_postproc.spectra import get_spectral_data
from wimr_postproc.spectra._hdf5storage import hdf5storage
from wimr_postproc.spectra.datastructs import ROIBounds
from wimr_postproc.spectra.intParams import ROIResult, get_intParams
from wimr_postproc.spectra.plotting import plot_skxky_figure
from wimr_postproc.spectra.stats import SNRs, find_kxky_peaks
from wimr_postproc.spectra.transforms import fkxky_to_kxky

logger = logging.getLogger(__name__)


def _plot_and_save_spec(
    cube: CartCube,
    spectrum_dt_bounds: tuple[datetime, datetime],
    roi: SpectrumRoiParams,
    png_path: Path | str,
) -> tuple[SNRs, pd.DataFrame, ROIBounds]:
    spec_fkxky, snrs, roi_bounds = get_spectral_data(cube, roi)
    spec_kxky = fkxky_to_kxky(spec_fkxky)
    kxky_peaks = find_kxky_peaks(spec_kxky, spec_fkxky)
    # TODO: Filter peaks with angle and maxL
    roi_origin_ll = utm.to_latlon(
        roi.center_x, roi.center_y, cube.origin.zone_number, cube.origin.zone_letter
    )
    fig, *_ = plot_skxky_figure(
        spec_kxky, spectrum_dt_bounds, cube.long_name, roi.name, roi_origin_ll, kxky_peaks
    )
    fig.savefig(png_path)
    plt.close(fig)
    return snrs, kxky_peaks, roi_bounds


def _lists_same_len(*lists: list) -> bool:
    """Confirm that a group of lists are all the same length."""
    return all(len(lst) == len(lists[0]) for lst in lists[1:])


@validate_call
def plot_and_send_spectra(cube_path: FilePath, params: SpectraGenParams) -> None:
    with CartCube.from_matfile_path(cube_path) as cube:
        date_dirname = f"{cube.start_dt:%Y-%m-%d}"
        cube_timestr = f"{cube.start_dt:%Y%m%s_%H%M%S}"
        spectra_dir = params.spectra_dir
        if params.date_dir:
            spectra_dir /= date_dirname
        spectra_dir.mkdir(exist_ok=True, parents=True)
        spectrum_dt_bounds = (
            cube.start_dt.astimezone(params.plot_timezone),
            cube.end_dt.astimezone(params.plot_timezone),
        )
        subimage_results: list[ROIResult] = []
        png_names: list[str] = []
        for roi in params.rois:
            png_name = (
                f"{cube.station}_{cube_timestr}_Skxky_{roi.name.replace(' ', '')}.png"
            )
            png_path = spectra_dir / png_name
            if png_path.is_file():
                if params.no_replot:
                    logger.info("%s has already been plotted. Skipping.", png_name)
                    continue
                else:
                    logger.info("%s has already been plotted. Replotting.", png_name)
            snrs, kxky_peaks, roi_bounds = _plot_and_save_spec(
                cube, spectrum_dt_bounds, roi, png_path
            )
            subimage_results.append((snrs, kxky_peaks, roi_bounds))
            png_names.append(png_name)
            logger.info("%s plotted.", png_name)
        if _lists_same_len(params.subimages, subimage_results, png_names):
            logger.info("Creating intParams file.")
            intParams = get_intParams(cube, params.subimages, subimage_results, png_names)
            intParams_fname = (
                f"{cube.station}_{cube_timestr}_{cube.grid_name}_intParams.mat"
            )
            hdf5storage.savemat(
                spectra_dir / intParams_fname, intParams, truncate_existing=True
            )
        else:
            logger.info("Not all spectra were computed so intParams cannot be generated.")


def plot_and_send_spectral_timeseries(
    timeseries_params: SpectralTimeseriesParams,
) -> None: ...


# class InvalidSelectedFramesError(ValueError):
#     def __init__(self) -> None:
#         super().__init__(
#             "selected_frames may be a slice with step size of 1 or a tuple of"
#             " (start_frame_i, end_frame_i + 1)"
#         )


def main():
    ...

    # selected_frames: slice = slice(None)

    # @field_validator("selected_frames", mode="before")
    # @classmethod
    # def selected_frames_from_tuple(cls, v: Sequence[int] | slice) -> slice:
    #     if (
    #         isinstance(v, Sequence)
    #         and all(isinstance(vi, int) for vi in v)
    #         and len(v) == 2
    #     ):
    #         return slice(v[0], v[1])
    #     elif isinstance(v, slice) and (v.step in [None, 1]):
    #         return v
    #     raise InvalidSelectedFramesError


def cli_main(): ...


if __name__ == "__main__":
    cli_main()
