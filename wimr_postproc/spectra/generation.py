"""spectra/generation.py - Functions to calculate, filter, and crop spectral data.

Public functions
----------------
calculate_spectrum
    Calculate the frequency-wavenumber spectrum from Cartesian-interpolated radar
    data.
crop_spectrum
    Crop a 3-D frequency-wavenumber spectrum in the wavenumber and frequency
    domains via _crop_wavenumbers and _crop_frequencies.
get_spectral_data
    Calculate, smooth, and crop an (f,kx,ky) spectrum and get SNR values for the
    spectrum.
"""

from typing import Any

import numpy as np
from scipy import ndimage

from wimr_postproc.cube import CartCube
from wimr_postproc.params import (
    SpectrumRoiParams,
)
from wimr_postproc.spectra.constants import L_MAX, L_MIN, T_MAX, T_MIN
from wimr_postproc.spectra.datastructs import ROIBounds, SpectrumFKxKy
from wimr_postproc.spectra.stats import SNRs, calc_SNRs
from wimr_postproc.util.types import (
    F64Arr1D,
    F64Arr3D,
    NDArr3D,
    is_F64Arr1D,
    is_F64Arr3D,
    is_ndarr3d,
)


class InvalidSelectedFramesError(IndexError):
    def __init__(self, sf: slice) -> None:
        super().__init__(
            f"{sf} is not a valid slice for this cube. Start must be positive, stop must"
            " be less than the number of cube rotations, and there must be at least four"
            " rotations selected."
        )


class ShapeError(ValueError):
    def __init__(self, *, expected: Any, shape: tuple[int, ...]) -> None:
        super().__init__(f"Wrong shape. Expected: {expected}. Actual: {shape}")


class SpectrumRoiBoundsError(ValueError):
    def __init__(self, spectrum_roi: SpectrumRoiParams) -> None:
        super().__init__(
            f"Spectrum region of interest {spectrum_roi} is incompatible with this cube"
            " as it selects a region not entirely within the bounds of the cube."
        )


def _validate_selected_frames(cube: CartCube, selected_frames: slice) -> slice:
    """Coerce selected_frames to work with cube_len and be an even length"""
    cube_len = cube.data.shape[0]
    start, stop, stride = selected_frames.indices(cube_len)
    if (stop - start) % 2 == 1:
        stop -= 1
    if start < 0 or stop > cube_len or stride <= 0 or stop - start < 4:
        raise InvalidSelectedFramesError(selected_frames)
    return slice(start, stop, stride)


def _get_roi_bounds(cube: CartCube, roi: SpectrumRoiParams) -> ROIBounds:
    side = roi.size * cube.d_xy
    x1_search, y1_search = (roi.center_x - side / 2, roi.center_y - side / 2)
    if x1_search < cube.grid_x.min() or y1_search < cube.grid_x.min():
        raise SpectrumRoiBoundsError(roi)
    # get the cube coordinate closest to x1 and y1
    x1_i, y1_i = (
        np.argmin(abs(cube.grid_x - x1_search)),
        np.argmin(abs(cube.grid_y - y1_search)),
    )
    x_i = slice(x1_i, x1_i + roi.size)
    y_i = slice(y1_i, y1_i + roi.size)
    x1, x2 = cube.grid_x[[x1_i, x1_i + roi.size]]
    y1, y2 = cube.grid_y[[y1_i, y1_i + roi.size]]
    return ROIBounds(side, x_i, y_i, x1, y1, x2, y2)


def _calc_spectrum(roi_data: NDArr3D[np.number]) -> F64Arr3D:
    """Calculate the 3-D power spectrum of the cube data.

    Parameters
    ----------
    roi_data
        3D array of radar intensity data from the region of interest with dimensions
        (time, x, y). The length of the time dimension must be even.

    Returns
    -------
    3D array representing the power spectrum, single-sided in frequency, with wavenumber
    dimensions centered on 0 m⁻¹, and the wavenumber plane rotated 180 degrees. Shape is
    (ntime/2, nx, ny)

    Steps
    -----
    1. Calculate the FFT
    2. Convert the FFT to a power spectrum
    3. Convert the power spectrum to a single sideband power spectrum
    4. Shift the wavenumber dimensions of the spectrum to be centered on zero
    5. Rotate the wavenumber plane 180 degrees.
    """
    if np.mod(roi_data.shape[0], 2) != 0:
        raise ShapeError(expected="even first dimension", shape=roi_data.shape)
    fft_ = np.fft.fftn(roi_data)
    power_spectrum = np.abs(fft_) ** 2.0
    assert is_F64Arr3D(power_spectrum)
    return power_spectrum


def _calc_wavenumber_domain(n_k: int, d_xy: np.float64) -> F64Arr1D:
    """Calculate the angular wavenumber domain

    Parameters
    ----------
    n_k
        Size of the domain
    d_xy
        The size of each x/y bin, in meters.

    Returns
    -------
    The angular wavenumber domain (unshifted) (rad/m)
    """
    wavenumbers = 2 * np.pi * np.fft.fftfreq(n_k, d=d_xy)
    assert is_F64Arr1D(wavenumbers)
    return wavenumbers


def _shift_kspectrum(
    spectrum_data: F64Arr3D, wavenumbers: F64Arr1D
) -> tuple[F64Arr3D, F64Arr1D]:
    """Shift the 3D spectrum in the wavenumber dimensions (center on zero)

    Parameters
    ----------
    spectrum_data
        3D power spectrum in dimensions (f, kx, ky)

    Returns
    -------
    spec_shifted
        `spectrum_data` with `fftshift` applied to the wavenumber dimensions
    k_shifted
        `wavenumbers` with `fftshift` applied
    """
    spec_shifted = np.fft.fftshift(spectrum_data, axes=(1, 2))
    k_shifted = np.fft.fftshift(wavenumbers)
    assert is_F64Arr3D(spec_shifted) and is_F64Arr1D(k_shifted)
    return spec_shifted, k_shifted


def _crop_wavenumbers(
    spectrum_data: F64Arr3D, wavenumbers: F64Arr1D, crop_val: float = np.nan
) -> tuple[F64Arr3D, F64Arr1D]:
    """Crop the spectrum and wavenumber domain to the wavelength boundaries.

    Since the spectrum is centered around 0, we won't remove the too-small wavenumbers
    from the domain. Also, radial wavenumbers outside the minimum wavelength bound but
    inside the kx,ky bounds will be assigned to `crop_val`.

    Parameters
    ---------
    spectrum_data
        The 3D (f, kx, ky) spectrum - centered on 0 in the wavenumber plane. Remains
        unmodified by this function.
    wavenumbers
        The (angular) wavenumber domain, centered on 0.
    crop_val
        Value to set at locations where absolute wavenumbers are smaller than k_min.

    Returns
    -------
    cropped_spectrum
        The 3D spectrum cropped to the minimum and maximum wavelengths
    cropped_wavenumbers
        The wavenumber domain, cropped to the minimum and maximum wavelengths
    """
    # Don't modify the input
    spectrum_data = spectrum_data.copy()
    # Use the L_MAX and L_MIN constants to determine the minimum and maximum wavenumbers.
    k_min = 2 * np.pi / L_MAX
    k_max = 2 * np.pi / L_MIN
    # Calculate grid of 2D radial wavenumbers using nifty numpy indexing and broadcasting
    k_grid = np.sqrt(wavenumbers[:, None] ** 2 + wavenumbers[None, :] ** 2)
    # Set values outside the bounds to crop_val
    k_oob_mask_2d = (k_grid < k_min) | (k_grid > k_max)
    spectrum_data[:, k_oob_mask_2d] = crop_val
    # crop with the max wavenumber (one dimension at a time)
    k_mask_1d = np.abs(wavenumbers) <= k_max
    cropped_spectrum = spectrum_data[:, k_mask_1d, :][:, :, k_mask_1d]
    cropped_wavenumbers = wavenumbers[k_mask_1d]
    assert is_F64Arr3D(cropped_spectrum) and is_F64Arr1D(cropped_wavenumbers)
    return cropped_spectrum, cropped_wavenumbers


def _calc_frequency_domain(
    cube: CartCube, selected_frames: slice = slice(None)
) -> F64Arr1D:
    start, stop, _ = selected_frames.indices(cube.data.shape[0])
    """Calculate the frequency domain using the average rotational period."""
    n_rot = stop - start
    if cube.full_A_file and (n_rot == cube.data.shape[0]):
        dt = cube.run_length / n_rot
    else:
        seconds_per_rots = [val for val in np.diff(cube.time) if val <= 2.4]
        dt = np.mean(seconds_per_rots)
    freq = np.fft.fftfreq(n_rot, d=dt)
    assert is_F64Arr1D(freq)
    return freq


def _crop_frequencies(
    spectrum_data: F64Arr3D, frequencies: F64Arr1D
) -> tuple[F64Arr3D, F64Arr1D]:
    """Crop the spectrum and frequencies to just the positive frequencies in the preset
    wave period boundaries."""

    # Get all the non-DC positive frequencies
    ssb_freq_slice = slice(1, spectrum_data.shape[0] // 2 + 1)
    # Crop the spectrum and frequencies to only positive frequencies
    # FUTURE: This is the same as a mask of frequencies > 0, right?
    spec_ssb = 2 * spectrum_data[ssb_freq_slice, :, :]
    f_ssb = frequencies[ssb_freq_slice]
    # Crop the spectrum and frequencies to out wave period boundaries
    f_min = 1 / T_MAX
    f_max = 1 / T_MIN
    freq_mask = (f_min <= f_ssb) & (f_ssb <= f_max)
    spec_cropped = spec_ssb[freq_mask, :, :]
    f_cropped = f_ssb[freq_mask]
    assert is_F64Arr3D(spec_cropped) and is_F64Arr1D(f_cropped)
    return spec_cropped, f_cropped


def calculate_spectrum(
    cube: CartCube, spectrum_roi: SpectrumRoiParams, selected_frames: slice = slice(None)
) -> tuple[SpectrumFKxKy, ROIBounds]:
    """Calculate the frequency-wavenumber spectrum from Cartesian-interpolated radar
    data.

    This function selects a subregion of a Cartesian-interpolated "cube", calculates the
    power spectrum in (f, kx, ky), where only the positive frequencies are retained and
    the kx and ky domains are centered on 0.

    Parameters
    ----------
    cube
        The Cartesian-interpolated radar data structure.
    spectrum_roi
        Parameters for selecting the region of interest from the cube.
    selected_frames
        Selection of frames to include. Step size must be 1.
    """
    selected_frames = _validate_selected_frames(cube, selected_frames)
    roi_bounds = _get_roi_bounds(cube, spectrum_roi)
    try:
        roi_data = cube.data[
            selected_frames,
            roi_bounds.x_i,
            roi_bounds.y_i,
        ]
        assert is_ndarr3d(roi_data, np.uint8)
    except IndexError:
        raise SpectrumRoiBoundsError(spectrum_roi) from None
    spectrum_data = _calc_spectrum(roi_data)
    wavenumbers = _calc_wavenumber_domain(spectrum_roi.size, cube.d_xy)
    frequencies = _calc_frequency_domain(cube, selected_frames)
    spectrum_data, wavenumbers = _shift_kspectrum(spectrum_data, wavenumbers)
    return SpectrumFKxKy(spectrum_data, frequencies, wavenumbers), roi_bounds


def smooth_spectrum(spectrum_data: F64Arr3D) -> F64Arr3D:
    """Apply the 3x3x3 window mean to the spectrum.

    Parameters
    ----------
    spectrum
        The 3D (f, kx, ky) spectrum data.

    Returns
    -------
    F64Arr3D - Filtered data, same type and shape as `spectrum`.

    """
    filtered = ndimage.uniform_filter(spectrum_data, size=(3, 3, 3))
    assert is_ndarr3d(filtered, np.float64)
    return filtered


def crop_spectrum(spectrum: SpectrumFKxKy) -> SpectrumFKxKy:
    """Crop a 3-D frequency-wavenumber spectrum in the wavenumber and frequency
    domains via _crop_wavenumbers and _crop_frequencies."""
    data_cropped, k_cropped = _crop_wavenumbers(spectrum.data, spectrum.k)
    data_cropped, f_cropped = _crop_frequencies(data_cropped, spectrum.f)
    return SpectrumFKxKy(data_cropped, f_cropped, k_cropped)


def get_spectral_data(
    cube: CartCube, spectrum_roi: SpectrumRoiParams
) -> tuple[SpectrumFKxKy, SNRs, ROIBounds]:
    """Calculate, smooth, and crop an (f,kx,ky) spectrum and get SNR values for the
    spectrum."""
    spectrum_raw, roi_bounds = calculate_spectrum(cube, spectrum_roi)
    spectrum_raw.data = smooth_spectrum(spectrum_raw.data)
    spectrum_cropped = crop_spectrum(spectrum_raw)
    # TODO - just calculate noise?
    snrs = calc_SNRs(spectrum_raw.data, spectrum_cropped.data)
    return spectrum_cropped, snrs, roi_bounds
