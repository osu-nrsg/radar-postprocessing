import csv
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from typing import Any, Iterable, Literal

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.collections import QuadMesh
from matplotlib.colors import Colormap, LinearSegmentedColormap
from matplotlib.contour import QuadContourSet
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.patches import FancyArrow
from matplotlib.text import Text

from wimr_postproc.spectra.constants import (
    SPECTRUM_DPI,
    SPECTRUM_PNG_H,
    SPECTRUM_PNG_W,
    T_MAX,
    T_MIN,
)
from wimr_postproc.spectra.datastructs import SpectrumFPhi, SpectrumKxKy
from wimr_postproc.util.conversions import pol2cart

SEMI_TRANSPARENT_BBOX = {"facecolor": "#F8F8F8", "alpha": 0.5, "linewidth": 0}
"""Dict of parameters for a matlpotlib bbox that makes a semi-transparent gray box."""


def _load_specIJRS_cmap() -> LinearSegmentedColormap:
    """Load the SpecIJRS colormap Guillermo created for the spectrum contour plot from a
    CSV file.
    """
    with open(Path(__file__).parent / "SpecIJRS.csv", "r") as fobj:
        spec_IJRS = np.array([[float(val) for val in line] for line in csv.reader(fobj)])
    return LinearSegmentedColormap.from_list("spec_IJRS", spec_IJRS)


def _draw_circle(
    axes: Axes,
    radius: float,
    *plot_args: Any,
    npts: int = 128,
    **plot_kwargs: Any,
) -> Line2D:
    """Draw a pseudo-circle on a set of axes at some radius.

    Parameters
    ----------
    axes
        Axes on which to draw the circle.
    radius
        Radius of the circle to draw.
    *plot_args
        Additional positional arguments for axes.plot after x and y.
    npts
        The number of points in the "circle". Default is 128.
    **plot_kwargs
        Keyword-arguments for axes.plot.

    Returns
    -------
    circle
        The plot object for the line.
    """
    phi = np.linspace(0, 2 * np.pi, npts, endpoint=True)
    x = radius * np.cos(phi)
    y = radius * np.sin(phi)
    return axes.plot(x, y, *plot_args, **plot_kwargs)[0]


def _draw_radial(
    ax: Axes, radius: float, phi: float, *plot_args: Any, **plot_kwargs: Any
) -> Line2D:
    """Draw a radial line on a set of axes out to some radius."""
    end_x, end_y = (radius * np.cos(phi), radius * np.sin(phi))
    return ax.plot([0, end_x], [0, end_y], *plot_args, **plot_kwargs)[0]


@dataclass
class CardinalPolarAxes:
    """A set of plots to simulate Cardinal (NSEW) polar axes on Cartesian axes with
    circular and radial grid lines and labels."""

    circles: list[Line2D]
    """List of the circular "grid lines" of the axes, from smallest to largest radius"""
    radials: list[Line2D]
    """List of equally-spaced (in angle) radial "grid lines" of the axes."""
    circle_labels: list[Text]
    """List of labels for each circle"""
    dir_labels: dict[str, Text]
    """Text labels for cardinal direction"""


def _draw_cardinal_polar_axes(
    ax: Axes,
    circle_radii: Iterable[float],
    circle_label_strs: Iterable[str],
    outer_label_prefix: str = "",
    cardinal_dirs: dict[str, float] | None = None,
) -> CardinalPolarAxes:
    # Default cardinal direction labels
    if cardinal_dirs is None:
        cardinal_dirs = {"W": 270, "S": 180, "E": 90}
    # Draw circle grid and add labels
    circles = []
    circle_labels = []
    for radius, label_str in zip(circle_radii, circle_label_strs, strict=True):
        if radius == max(circle_radii):
            color, linewidth = ("black", 1)
            label_str = f"{outer_label_prefix}{label_str}"
        else:
            color, linewidth = ("gray", 0.5)
        circles.append(_draw_circle(ax, radius, "-", linewidth=linewidth, color=color))
        circle_labels.append(
            ax.text(
                radius * np.cos(7 * np.pi / 4),
                radius * np.sin(7 * np.pi / 4),
                label_str,
                fontweight="bold",
                bbox=SEMI_TRANSPARENT_BBOX,
            )
        )
    # Draw labels
    radials = [
        _draw_radial(ax, max(circle_radii), phi, "-", linewidth=0.5, color="gray")
        for phi in np.linspace(0, 2 * np.pi, 16, endpoint=False)
    ]
    # Create Cardinal direction labels
    r = max(circle_radii) * 1.05
    dir_labels: dict[str, Text] = {}
    for dir_str, azimuth in cardinal_dirs.items():
        azimuth %= 360
        phi = np.deg2rad(90 - azimuth)
        dir_labels[dir_str] = ax.text(r * np.cos(phi), r * np.sin(phi), dir_str)
        if 0 < azimuth < 180:
            dir_labels[dir_str].set_horizontalalignment("right")
        elif 180 < azimuth < 360:
            dir_labels[dir_str].set_horizontalalignment("left")
        if azimuth > 270 or azimuth < 90:
            dir_labels[dir_str].set_verticalalignment("top")
        elif 90 < azimuth < 270:
            dir_labels[dir_str].set_verticalalignment("bottom")
    return CardinalPolarAxes(circles, radials, circle_labels, dir_labels)


def plot_kxky_spec_contour(
    ax: Axes,
    spectrum_kxky: SpectrumKxKy,
    *,
    vmax: float | None = None,
    cmap: Colormap | None = None,
) -> tuple[CardinalPolarAxes, QuadContourSet]:
    """Create a faux polar axes and plot a frequency-integrated (kx, ky) spectra as filled
    contours.

    Parameters
    ----------
    ax
        Axes on which to plot.
    spectrum_kxky
        The frequency-integrated spectrum to plot.
    vmax
        Highest value for the contour plot's colormap. Default is to use the data maximum.
    cmap
        Colormap to use for the contour plot.

    Returns
    -------
    polar_axes
        The plot objects that make up the faux polar axes.
    kxky_contour
        The contour plot object.
    """
    if cmap is None:
        cmap = _load_specIJRS_cmap()
    circle_wavelengths = [75, 100, 150, 200, 300, 500]
    k_max = 2 * np.pi / min(circle_wavelengths)

    ax.set_xlim(-k_max, k_max)
    ax.set_ylim(-k_max, k_max)
    ax.axis("off")
    ax.axis("image")

    circle_wavenumbers = [2 * np.pi / L for L in circle_wavelengths[::-1]]
    circle_label_strings = [f"{L}" for L in circle_wavelengths[::-1]]
    polar_axes = _draw_cardinal_polar_axes(
        ax, circle_wavenumbers, circle_label_strings, outer_label_prefix="L = "
    )
    levels = np.linspace(0, vmax, 30) if vmax is not None else 30
    kxky_contour = ax.contourf(
        spectrum_kxky.k,
        spectrum_kxky.k,
        spectrum_kxky.data.swapaxes(0, 1),
        levels=levels,
        vmax=vmax,
        cmap=cmap,
    )
    return polar_axes, kxky_contour


def plot_fphi_spectrum(
    axes: Axes,
    spec_fphi: SpectrumFPhi,
    vmax: float | None = None,
    cmap: Colormap | None = None,
) -> tuple[CardinalPolarAxes, QuadMesh]:
    """Create a faux polar axes and plot a wavenumber-integrated (f, phi) spectra as
    a pseudocolor plot.

    Parameters
    ----------
    ax
        Axes on which to plot.
    spectrum_fphi
        The frequency-integrated spectrum to plot.
    vmax
        Highest value for the pcolor plot's colormap. Default is to use the data maximum.
    cmap
        Colormap to use for the pcolor plot.

    Returns
    -------
    polar_axes
        The plot objects that make up the faux polar axes.
    pcolor
        The pseudocolor plot object.
    """
    if cmap is None:
        cmap = _load_specIJRS_cmap()
    f_max = 1 / T_MIN

    axes.set_xlim(-f_max, f_max)
    axes.set_ylim(-f_max, f_max)
    axes.axis("off")
    axes.axis("image")

    # plot circles
    circle_frequencies = np.linspace(1 / T_MAX, 1 / T_MIN, 5)
    circle_period_strings = [f"{1 / f:.0f}" for f in circle_frequencies]
    polar_axes = _draw_cardinal_polar_axes(
        axes, circle_frequencies, circle_period_strings, outer_label_prefix="T = "
    )

    # plot spectrum
    grid_f, grid_phi = np.meshgrid(spec_fphi.f, spec_fphi.phi)
    grid_x, grid_y = pol2cart(grid_phi, grid_f)  # type: ignore [no-untyped-call]
    pcolor = axes.pcolormesh(
        grid_x,
        grid_y,
        spec_fphi.data.swapaxes(0, 1),
        vmax=vmax,
        shading="nearest",
        cmap=cmap,
    )
    return polar_axes, pcolor


def _spectrum_figax(
    width_px: float = SPECTRUM_PNG_W, height_px: float = SPECTRUM_PNG_H, dpi=SPECTRUM_DPI
):
    """Create a figure with a single set of axes with a given pixel width and height and
    dpi. Defaults are taken from the constants module."""
    return plt.subplots(figsize=(width_px / dpi, height_px / dpi), dpi=dpi)


def _add_spec_title(
    ax: Axes,
    spec_type: Literal["kxky", "fphi"],
    spectrum_dt_bounds: tuple[datetime, datetime],
    cube_long_name: str,
    roi_name: str,
    roi_origin_ll: tuple[float, float],
) -> Text:
    """Add a title to the spectrum plot axes.

    Parameters
    ----------
    ax
        The axes.
    spectrum_dt_bounds
        Start and stop datetime of the data from which the spectrum was taken. Must be
        timezone-aware.
    cube_long_name
        The full name of the site from the cube.
    roi_name
        Name of the ROI from which the spectrum was taken.
    roi_origin_ll
        Latitude and longitude of the center of the ROI.

    Returns
    -------
    title
        The Text object for the created title.
    """
    start_dt, end_dt = spectrum_dt_bounds
    if not start_dt.tzinfo or not end_dt.tzinfo or start_dt.tzinfo != end_dt.tzinfo:
        raise ValueError(
            "spectrum_dt_bounds must be timzone-aware and have the same timezone."
        )
    timestr = f"{start_dt:%Y-%m-%d %-I:%M} - {end_dt:%-I:%M %p %Z}"
    first_line = (
        "Frequency-integrated Directional Wavenumber Spectrum"
        if spec_type == "kxky"
        else "Wavenumber-integrated directional frequency spectrum"
    )
    center_lat, center_lon = roi_origin_ll
    title_str = "\n".join(
        [
            first_line,
            f"{cube_long_name} -- {timestr}",
            f"{roi_name} region - {center_lat:.5g}°, {center_lon:.5g}°",
        ]
    )
    return ax.set_title(
        title_str,
        bbox=SEMI_TRANSPARENT_BBOX,
        fontweight="bold",
        y=1.0,
        verticalalignment="bottom",
    )


def _kpeak_arrow(ax: Axes, peak: pd.Series, *, secondary=False) -> FancyArrow:
    """Plot an arrow indicating the primary or secondary peak of the spectrum on a kxky
    plot."""
    facecolor = (1, 0.6, 0.6) if secondary else (0.85, 0, 0)
    zorder = 2 if secondary else 3
    return ax.arrow(
        peak["kx"],
        peak["ky"],
        -peak["kx"],
        -peak["ky"],
        width=0.002,
        facecolor=facecolor,
        edgecolor="black",
        length_includes_head=True,
        zorder=zorder,
    )


def _fpeak_arrow(ax: Axes, peak: pd.Series, *, secondary=False) -> FancyArrow:
    """Plot an arrow indicating the primary or secondary peak of the spectrum on an fphi
    plot."""
    fx = peak["f"] * np.cos(peak["phi"])
    fy = peak["f"] * np.sin(peak["phi"])
    facecolor = (1, 0.6, 0.6) if secondary else (0.85, 0, 0)
    zorder = 2 if secondary else 3
    return ax.arrow(
        fx,
        fy,
        -fx,
        -fy,
        width=0.002,
        facecolor=facecolor,
        edgecolor="black",
        length_includes_head=True,
        zorder=zorder,
    )


def _azi2card(azi_deg: float, num_letters: Literal[1, 2, 3] = 3) -> str:
    """Convert an azimuth angle in degrees to a cardinal direction of up to three
    letters."""
    # fmt: off
    dirs = [
        "N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W",
        "WNW", "NW", "NNW",
    ]
    # fmt: on
    if num_letters == 1:
        dirs = dirs[::4]
    elif num_letters == 2:
        dirs = dirs[::2]
    dir_width = 360 / len(dirs)
    dir_i = round(azi_deg / dir_width)
    return dirs[dir_i % len(dirs)]


def _peaks_text(fig: Figure, peaks: pd.DataFrame) -> Text:
    """Create a Text object displaying the spectrum's peaks on the figure."""
    text = "Peaks:"
    for _, peak in peaks.iterrows():
        if peak["norm_value"] < 0.5:
            continue
        card = _azi2card(peak["azi"], 3)
        text += f"\n- {card} @ {peak['T']:.1f}s\n    (L = {peak['L']:.0f}m)"
    return fig.text(
        0.02,
        0.789,
        text,
        bbox={"facecolor": "white", "alpha": 0.5, "linewidth": 0},
        verticalalignment="center",
        fontweight="bold",
    )


def plot_skxky_figure(
    spec_kxky: SpectrumKxKy,
    spectrum_dt_bounds: tuple[datetime, datetime],
    cube_long_name: str,
    roi_name: str,
    roi_origin_ll: tuple[float, float],
    peaks: pd.DataFrame,
) -> tuple[
    Figure, Axes, tuple[CardinalPolarAxes, QuadContourSet, Text, list[FancyArrow], Text]
]:
    """Create a figure with a plot of the frequency-integrated spectrum on a faux set of
    polar axes along with peak arrows, a descriptive title, and a textual list of the
    peaks.

    Parameters
    ----------
    spec_kxky
        Frequency-integrated spectrum.
    spectrum_dt_bounds
        Start and stop datetime of the data from which the spectrum was taken. Must be
        timezone-aware.
    cube_long_name
        The full name of the site from the cube.
    roi_name
        Name of the ROI from which the spectrum was taken.
    roi_origin_ll
        Latitude and longitude of the center of the ROI.
    peaks
        The peaks found in the spectrum, from `kxky_peaks()`.

    Returns
    -------
    fig
        The figure
    ax
        The axes
    plot_elements
        A tuple of the following:
        - polar_axes - The plot objects that make up the faux polar axes.
        - kxky_contour - The spectrum contour plot object.
        - title - The title's text object
        - peak_arrows - The arrow plot objects for the peaks
        - peaks_text - The text object that lists the peaks on the figure.

    """
    fig, ax = _spectrum_figax()
    # Draw the polar axes and plot the spectra
    polar_axes, kxky_contour = plot_kxky_spec_contour(ax, spec_kxky)
    # Add title to axes
    title = _add_spec_title(
        ax, "kxky", spectrum_dt_bounds, cube_long_name, roi_name, roi_origin_ll
    )
    # Add peak arrows
    # FUTURE: Add some criteria to decide if there are no valid peaks
    peak_arrows = [_kpeak_arrow(ax, peaks.iloc[0])] if len(peaks) > 0 else []
    if len(peaks) > 1 and peaks.iloc[1]["norm_value"] >= 0.5:
        peak_arrows.append(_kpeak_arrow(ax, peaks.iloc[1], secondary=True))
    peaks_text = _peaks_text(fig, peaks)
    return fig, ax, (polar_axes, kxky_contour, title, peak_arrows, peaks_text)


def plot_sfphi_figure(
    spec_fphi: SpectrumFPhi,
    spectrum_dt_bounds: tuple[datetime, datetime],
    cube_long_name: str,
    roi_name: str,
    roi_origin_ll: tuple[float, float],
    peaks: pd.DataFrame,
) -> tuple[
    Figure, Axes, tuple[CardinalPolarAxes, QuadMesh, Text, list[FancyArrow], Text]
]:
    """Create a figure with a plot of the frequency-integrated spectrum on a faux set of
    polar axes along with peak arrows, a descriptive title, and a textual list of the
    peaks.

    Parameters
    ----------
    spec_fphi
        Wavenumber-integrated spectrum.
    spectrum_dt_bounds
        Start and stop datetime of the data from which the spectrum was taken. Must be
        timezone-aware.
    cube_long_name
        The full name of the site from the cube.
    roi_name
        Name of the ROI from which the spectrum was taken.
    roi_origin_ll
        Latitude and longitude of the center of the ROI.
    peaks
        The peaks found in the spectrum, from `kxky_peaks()`.

    Returns
    -------
    fig
        The figure
    ax
        The axes
    plot_elements
        A tuple of the following:
        - polar_axes - The plot objects that make up the faux polar axes.
        - kxky_contour - The spectrum contour plot object.
        - title - The title's text object
        - peak_arrows - The arrow plot objects for the peaks
        - peaks_text - The text object that lists the peaks on the figure.

    """
    fig, ax = _spectrum_figax()
    # Draw the polar axes and plot the spectra
    polar_axes, kxky_contour = plot_fphi_spectrum(ax, spec_fphi)
    # Add title to axes
    title = _add_spec_title(
        ax, "fphi", spectrum_dt_bounds, cube_long_name, roi_name, roi_origin_ll
    )
    # Add peak arrows
    # FUTURE: Add some criteria to decide if there are no valid peaks
    peak_arrows = [_fpeak_arrow(ax, peaks.iloc[0])]
    if len(peaks) > 1 and peaks.iloc[1]["norm_value"] >= 0.5:
        peak_arrows.append(_fpeak_arrow(ax, peaks.iloc[1], secondary=True))
    peaks_text = _peaks_text(fig, peaks)
    return fig, ax, (polar_axes, kxky_contour, title, peak_arrows, peaks_text)
