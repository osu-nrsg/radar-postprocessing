import logging
import subprocess


def log_called_process_error(
    logger: logging.Logger, err: subprocess.CalledProcessError, msg: str
) -> None:
    logger.exception("%s\nSTDOUT: %s\nSTDERR: %s", msg, err.stdout, err.stderr)
