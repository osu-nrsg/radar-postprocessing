"""matlab.py - Interact with MATLAB using the subprocessing module

The MATLAB Engine for Python only works for certain versions of Python, and is usually at
least one Python version behind (e.g. MATLAB R2024a doesn't support Python 3.12, which was
released in Fall 2023.) The scripts/functions used in our processing don't need to
exchange much info in the interface (mostly reading from and writing to files) so it's
easy to do the interaction with JSON. m_func_wrapper.m handles the MATLAB side of this.

Important public stuff:

- license_checker - An instance of the _LicenseChecker class, created (but not started) on
  import of this module. Use license_checker.start() to start the check for a licensed
  MATLAB version (via a background subprocess.Popen instance). Use license_checker.check()
  to check that MATLAB has been found and the current user is licensed to run it. This
  will return the Path to the MATLAB executable.
- MatlabRunner - A class for executing a MATLAB function in an m-file from Python using
  the subprocessing module, JSON, and m_func_wrapper.m for interfacing with MATLAB. Use
  the license checker to the the MATLAB executable path to initialize the runner.

"""

import json
import logging
import os
import shutil
import subprocess
import time
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Final

MAT_CHECK_TIMEOUT_S: Final = 20

logger = logging.getLogger(__name__)


class MatlabError(Exception):
    pass


class MatlabLicensingError(Exception):
    pass


@dataclass
class _LicenseCheckInfo:
    matlab_exe_path: Path
    popen: subprocess.Popen[str]
    tstart: float


class _LicenseChecker:
    """Class for checking for a licensed MATLAB installation in the background."""

    def __init__(self) -> None:
        """Initialize some variables."""
        # self._mat_exe_path_error: Exception | None = None
        self._mat_exe_searched = False
        self._check_info: _LicenseCheckInfo | None = None

    @staticmethod
    def get_mat_exe_path(matlab_root: Path | None = None) -> Path | None:
        """Try to get a path to a MATLAB executable. Return None if nothing is found."""
        maybe_mat_exe_path: str | Path | None
        if matlab_root:
            if os.name == "nt":
                maybe_mat_exe_path = matlab_root / "bin" / "matlab.exe"
            else:
                maybe_mat_exe_path = matlab_root / "bin" / "matlab"
        else:
            maybe_mat_exe_path = shutil.which("matlab")
        if maybe_mat_exe_path is None or not os.access(maybe_mat_exe_path, os.X_OK):
            return None
        return Path(maybe_mat_exe_path)

    def start(self, matlab_root: Path | None = None) -> None:
        """Start checking for a MATLAB executable and see if the current user is able
        to run MATLAB with it. If there is no executable found, an error string is saved
        to be raised in `check()`.

        Parameters
        ----------
        matlab_root
            Path to the root folder of the MATLAB installation to use (e.g.
            /opt/MATLAB2022a) If not specified, the PATH is searched for a MATLAB
            executable (using `shutil.which()`)

        """
        matlab_exe_path = self.get_mat_exe_path(matlab_root)
        self._mat_exe_searched = True
        if matlab_exe_path is None:
            return
        check_tstart = time.time()
        popen = subprocess.Popen(
            [matlab_exe_path, "-batch", "license('inuse'); exit;"],
            encoding="utf-8",
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        self._check_info = _LicenseCheckInfo(
            matlab_exe_path=matlab_exe_path, popen=popen, tstart=check_tstart
        )

    def check(self, timeout: float | None = MAT_CHECK_TIMEOUT_S) -> Path:
        """Wait for the license check subprocess to complete and, if sucessful, return the
        path to the MATLAB executable.

        Parameters
        ----------
        timeout
            Maximum amount of time to wait since the checking process began before issuing
            an error. Defaults to matlab.MAT_CHECK_TIMEOUT_S. If None, wait indefinitely.

        Returns
        -------
        matlab_exe_path
            The Path to the MATLAB executable

        Raises
        ------
        FileNotFoundError
            The MATLAB executable could not be found.
        MatlabLicensingError
            The current user is not licensed to use MATLAB.
        TimeoutError
            The check for the MATLAB license timed out.
        """
        while self._check_info is None:
            if self._mat_exe_searched:
                raise FileNotFoundError(
                    "Cannot a MATLAB executable in the system path or at the specified"
                    "MATLAB root path."
                )
            self.start()
        if timeout is not None:
            # Make the timeout relative to the elapsed time
            elapsed = time.time() - self._check_info.tstart
            timeout = timeout - elapsed if timeout > elapsed else 0
        try:
            out, err = self._check_info.popen.communicate("\n\n", timeout=timeout)
        except subprocess.TimeoutExpired:
            raise TimeoutError("Check for MATLAB license timed out.") from None
        finally:
            self._check_info.popen.terminate()
        if "matlab" not in out:
            raise MatlabLicensingError(
                f"Licensing failed. MATLAB stdout: {out}\nMATLAB stderr: {err}"
            )
        return self._check_info.matlab_exe_path

    def __del__(self) -> None:
        """Make sure the MATLAB process is killed, if necessary."""
        if self._check_info is None:
            return
        if self._check_info.popen.poll() is None:
            # This isn't nice, but should be effective
            self._check_info.popen.kill()


license_checker = _LicenseChecker()


class MatlabRunner:
    _m_func_wrapper_dir: Final = Path(__file__).parent  # same as this script!

    def __init__(self, mat_exe_path: Path | str) -> None:
        """Initialize the runner. Ensure m_func_wrapper.m and the MATLAB executable
        exist.

        Parameters
        ----------
        mat_exe_path
            Path to the MATLAB executable, from e.g. matlab.license_check.check()

        Raises
        ------
        FileNotFoundError
            Either m_func_wrapper.m is not found in this module's directory, or the
            MATLAB executable is not at the specified path.
        """
        if not (self._m_func_wrapper_dir / "m_func_wrapper.m").is_file():
            raise FileNotFoundError(
                f"m_func_wrapper.m is missing from {__name__.split('.')[0]}"
            )
        if not os.access(mat_exe_path, os.X_OK):
            raise FileNotFoundError(
                f"The specified mat_exe_path '{mat_exe_path}' does not exist or is not"
                " exectuable."
            )
        self.mat_exe_path = mat_exe_path

    def run_m_func(
        self,
        func_path: Path | str,
        *func_args: Any,
        func_path_relative_to_module: bool = True,
        timeout: float | None = None,
    ) -> list[Any]:
        """Run a MATLAB function m-file with arguments. The arguments are transferred as
        JSON.

        Parameters
        ----------
        func_path
            Full path to the m-file. The function name must be the same as the m-file
            name.
        *func_args
            Arguments for the MATLAB function
        func_path_relative_to_module
            If True, `func_path` is relative to this module (default). Otherwise, it is
            assumed to be absolute or relative to the current working directory.
        timeout
            The amount of time to wait for the MATLAB call to complete. If None (default),
            wait indefinitely.

        Returns
        -------
        outputs
            The return values from the MATLAB function.

        Raises
        ------
        FileNotFoundError
            The function m-file was not found.
        TimeoutError
            The MATLAB subprocess did not complete before the timeout elapsed.
        MatlabError
            There was an error in the MATLAB subprocess (possibly in the called m-file.)
        """
        if func_path_relative_to_module:
            func_path = Path(__file__).parent / func_path
        if not Path(func_path).is_file():
            raise FileNotFoundError(
                f"The specified MATLAB function m-file '{func_path}' does not exist."
            )
        argin_json = json.dumps(func_args)
        mat_cmd = f"m_func_wrapper('{func_path}', '{argin_json}'); exit;"
        try:
            proc = subprocess.run(
                [self.mat_exe_path, "-batch", mat_cmd],
                cwd=self._m_func_wrapper_dir,
                encoding="utf-8",
                timeout=timeout,
                capture_output=True,
                input="",
            )
        except subprocess.TimeoutExpired:
            raise TimeoutError(
                f"The call to the MATLAB function {func_path} timed out."
            ) from None
        if proc.returncode != 0:
            if "Licensing error" in proc.stderr or "Mathworks Account" in proc.stdout:
                raise MatlabLicensingError(
                    "There appears to be a licensing error running MATLAB."
                    f"\nMATLAB stdout: {proc.stdout}"
                    f"\nMATLAB stderr: {proc.stderr}"
                )
            raise MatlabError(
                f"There was an error in the call to the MATLAB function {func_path}."
                f"\nMATLAB stdout: {proc.stdout}"
                f"\nMATLAB stderr: {proc.stderr}"
            )
        logger.info("MATLAB %s output: %s", func_path, proc.stderr)
        outputs = json.loads(proc.stdout)
        if not isinstance(outputs, list):
            outputs = [outputs]
        assert isinstance(outputs, list)  # help mypy...
        return outputs
