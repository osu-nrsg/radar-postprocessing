import multiprocessing
import multiprocessing.pool
import os
import shutil
import subprocess
from collections.abc import Iterable
from dataclasses import dataclass
from datetime import datetime, timezone, tzinfo
from functools import partial
from itertools import starmap
from pathlib import Path
from typing import Literal, overload
from zoneinfo import ZoneInfo

import colorcet
import h5py
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from scipy.interpolate import RegularGridInterpolator

from wimr_postproc.util import h5mat
from wimr_postproc.util.conversions import cart2pol, phi2aziT
from wimr_postproc.util.types import is_arr_dtype


class FakePool:
    def imap(self, *args, **kwargs):
        return map(*args, **kwargs)

    def starmap(self, *args, **kwargs):
        return starmap(*args, **kwargs)

    def __enter__(self):
        """Placeholder - do nothing"""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Placeholder - do nothing"""
        pass


def get_cubegroup_cart_domains(
    cube_paths: list[Path], xdom_shrink: float = 1.0, ydom_shrink: float = 1.0
):
    x_min = float("inf")
    x_max = float("-inf")
    y_min = float("inf")
    y_max = float("-inf")

    for cube_path in cube_paths:
        with h5py.File(cube_path, mode="r") as cube:
            cube_x = h5mat.scalar(cube, "/results/XOrigin")
            cube_y = h5mat.scalar(cube, "/results/YOrigin")
            r_max = np.max(h5mat.vector(cube, "/Rg"))
            if cube_x + r_max > x_max:
                x_max = cube_x + r_max
            if cube_y + r_max > y_max:
                y_max = cube_y + r_max
            if cube_x - r_max < x_min:
                x_min = cube_x - r_max
            if cube_y - r_max < y_min:
                y_min = cube_y - r_max

    if xdom_shrink < 1.0:
        xmax_raw = x_max - x_min
        x_center = x_min + xmax_raw / 2
        xmax_raw_shrunk = xmax_raw * xdom_shrink
        x_min = x_center - xmax_raw_shrunk / 2
        x_max = x_center + xmax_raw_shrunk / 2

    if ydom_shrink < 1.0:
        ymax_raw = y_max - y_min
        y_center = y_min + ymax_raw / 2
        ymax_raw_shrunk = ymax_raw * ydom_shrink
        y_min = y_center - ymax_raw_shrunk / 2
        y_max = y_center + ymax_raw_shrunk / 2

    xdom_utm = np.arange(x_min, x_max, 10.0)
    ydom_utm = np.arange(y_min, y_max, 10.0)
    return xdom_utm, ydom_utm


def get_cube_pol_dest_coords(
    cube: h5py.File, xdom_utm, ydom_utm
) -> tuple[np.ndarray, np.ndarray]:
    xdom_rel = xdom_utm - h5mat.scalar(cube, "/results/XOrigin")
    ydom_rel = ydom_utm - h5mat.scalar(cube, "/results/YOrigin")
    x_gd, y_gd = np.meshgrid(xdom_rel, ydom_rel)
    phi_gd, r_gd = cart2pol(x_gd, y_gd)
    return np.mod(phi2aziT(phi_gd), 360), r_gd


@dataclass
class FrameInfo:
    i: int
    azi_in: np.ndarray
    rg_in: np.ndarray
    data_in: np.ndarray
    azi_q: np.ndarray
    rg_q: np.ndarray


def interpolate_frame(
    i: int,
    data_in: np.ndarray,
    azi_in: np.ndarray,
    rg_in: np.ndarray,
    azi_q: np.ndarray,
    rg_q: np.ndarray,
):
    print(f"\rFrame {i}", end="", flush=True)
    f = RegularGridInterpolator(
        (azi_in, rg_in), data_in, bounds_error=False, fill_value=np.nan
    )
    return f((azi_q, rg_q))


def get_cube_frames(
    cube_path,
    xdom_utm: np.ndarray,
    ydom_utm: np.ndarray,
    pool: multiprocessing.pool.Pool | None = None,
):
    print(f"Getting interpolated frames from {cube_path}...")
    with h5py.File(cube_path, "r") as cube:
        nsweep = h5mat.dataset(cube, "/data").shape[0]
        azi_q, rg_q = get_cube_pol_dest_coords(cube, xdom_utm, ydom_utm)
        frames = np.zeros((nsweep, *azi_q.shape), np.uint8)

        rg = h5mat.vector(cube, "/Rg")
        assert is_arr_dtype(rg, np.floating)
        r_mask = rg > 0
        azi_in = h5mat.vector(cube, "/Azi")
        rg_in = rg[r_mask]
        interp_frame_thiscube = partial(
            interpolate_frame, azi_in=azi_in, rg_in=rg_in, azi_q=azi_q, rg_q=rg_q
        )
        interp_args = (
            (i, h5mat.dataset(cube, "/data")[i, :, r_mask.nonzero()[0]])
            for i in range(nsweep)
        )

        itermap: Iterable
        if pool:
            itermap = pool.starmap(interp_frame_thiscube, interp_args)
        else:
            itermap = map(interp_frame_thiscube, *zip(interp_args))
        for i, frame in enumerate(itermap):
            frames[i, :, :] = frame
        print(f" -- All {nsweep} Done.")
        sweep_times = h5mat.dataset(cube, "/time")[:, 0]
    return frames, sweep_times


def remove_overlapping_sweeps(
    all_frames: list[np.ndarray], all_sweep_times: list[np.ndarray]
):
    for i, sweep_times in enumerate(all_sweep_times):
        if i == 0:
            continue
        mask = np.ones_like(sweep_times, dtype=bool)
        last_t_max = all_sweep_times[i - 1].max()
        # Any times in the current set of sweep times lte the maximum of the last times must be removed
        mask[sweep_times <= last_t_max] = False
        all_frames[i] = all_frames[i][mask, :, :]
        all_sweep_times[i] = all_sweep_times[i][mask]


def nearest_idx(arr: np.ndarray, v: float):
    return np.argmin(np.abs(arr - v))


def get_moving_window_slices(times_s: np.ndarray, time_window_s: float):
    return [
        slice(
            i,  # start index is the current sweep index
            nearest_idx(times_s, t + time_window_s)
            + 1,  # closest to current time plus time interval
        )
        for i, t in enumerate(times_s)
        if (t + time_window_s) <= times_s.max()
    ]


@overload
def img_norm(
    values: np.ndarray,
    vmin: float,
    vmax: float,
    maxval: float | int = 1.0,
    clip: bool = True,
    copy: Literal[False] = False,
    dtype: object | None = None,
) -> None: ...
@overload
def img_norm(
    values: np.ndarray,
    vmin: float,
    vmax: float,
    maxval: float | int = 1.0,
    clip: bool = True,
    copy: Literal[True] = True,
    dtype: object | None = None,
) -> np.ndarray: ...
def img_norm(
    values: np.ndarray,
    vmin: float,
    vmax: float,
    maxval: float | int = 1.0,
    clip: bool = True,
    copy: bool = False,
    dtype: object | None = None,
) -> np.ndarray | None:
    """Linearly normalize an array of values into the [0.0, maxval] interval.

    This is a somewhat simplified version of matplotlib.colors.Normalize.

    Parameters
    ----------
    values
        Array of values to normalize. This is modified in-place.
    vmin
        Minimum value of window.
    vmax
        Maximum value of window.
    maxval
        Maximum value of normalized data. Default is 1.0.
    clip
        If True (default) values outside the window are forced to 0.0 or maxval.
    copy
        If true, the normalization is performed on a copy of `values` and returned. Otherwise the `values` is
        normalized in-place.
    dtype
        If copy is true, this dtype will be used for the output array. Default is to match the input type.

    Returns
    -------
    values
        normalized values, if copy is True. Otherwise None.
    """
    if copy:
        if dtype is None:
            dtype = values.dtype
        assert isinstance(dtype, np.dtype)
        output = values.astype(dtype)  # astype() always copies
    else:
        output = values  # output *is* values

    # Operations:
    # - Optionally clip to vmin, vmax
    # - Scale such that vmin is 0 and vmax is maxval
    if clip:
        output[:] = maxval * ((values.clip(vmin, vmax) - vmin) / (vmax - vmin))
    else:
        output[:] = maxval * ((values - vmin) / (vmax - vmin))

    # Nothing to return if not copying
    return output if copy else None


frame_font = ImageFont.truetype("LiberationSans-Regular.ttf", size=36)


def plot_frame(
    frame: np.ndarray,
    t: float,
    movwindow_dir: Path,
    tz_plot: tzinfo = timezone.utc,
    vmin=0,
    vmax=255,
):
    frame_norm = img_norm(frame, vmin, vmax, 255, copy=True)
    frame_im = np.uint8(colorcet.cm["fire"](frame_norm.astype(np.single) / 255.0) * 255)
    frame_dt = datetime.fromtimestamp(t, tz=timezone.utc)
    frame_name = frame_dt.strftime("%Y%m%d_%H%M%S_%f") + ".png"
    frame_path = movwindow_dir / frame_name
    im = Image.fromarray(np.flipud(frame_im))
    draw = ImageDraw.Draw(im)
    draw.text(
        (0, 0),
        frame_dt.astimezone(tz_plot).strftime("%Y-%m-%d %H:%M:%S.%f %Z"),
        fill=(255, 0, 0),
        font=frame_font,
    )
    im.save(frame_path)
    print(f"Generated {frame_path}.", flush=True)
    return frame_path


if __name__ == "__main__":
    os.nice(15)  # Relent to other processing
    cube_dir = Path("/data/cubes")
    mov_basedir = Path("/data/movmedian")
    moving_fn = partial(np.median, axis=0)
    min_cubename = "PointSur_20210612_185000_moving_pol.mat"
    max_cubename = "PointSur_20210612_192000_moving_pol.mat"
    xdom_shrink = 0.5
    ydom_shrink = 0.5
    decim_step = 10
    vmin = 30
    vmax = 275
    framerate = 10

    # Set to step to start at if re-running in ipython with `%run -i moving_avgs.py`
    current_step = 6  # NOTE: Reset to 1 before running fresh.
    if current_step == 1:
        print("Getting all frames in common grid.")
        # important to sort cubes or this breaks.
        cube_paths = sorted(
            [
                p
                for p in cube_dir.glob("**/*_moving_pol.mat")
                if p.name >= min_cubename and p.name <= max_cubename
            ]
        )
        xdom_utm, ydom_utm = get_cubegroup_cart_domains(
            cube_paths, xdom_shrink=xdom_shrink, ydom_shrink=ydom_shrink
        )
        all_frames = []
        all_sweep_times = []
        with multiprocessing.Pool() as pool:
            # with FakePool() as pool:
            for cube_path in cube_paths:
                frames, sweep_times = get_cube_frames(cube_path, xdom_utm, ydom_utm, pool)
                all_frames.append(frames)
                all_sweep_times.append(sweep_times)
        current_step += 1
    if current_step == 2:
        remove_overlapping_sweeps(all_frames, all_sweep_times)
        current_step += 1
    if current_step == 3:
        all_frames = np.concatenate(all_frames)
        all_sweep_times = np.concatenate(all_sweep_times)
        current_step += 1
    if current_step == 4:
        # make sure vars are loaded
        all_sweep_times = locals()["all_sweep_times"]
        all_frames = locals()["all_frame_times"]
        movwindow_slices = get_moving_window_slices(all_sweep_times, 90.0)[::decim_step]
        movwindow_frames = np.zeros(
            (len(movwindow_slices), *all_frames.shape[1:]), dtype=np.uint8
        )
        sliced_frames = (all_frames[slc, :, :] for slc in movwindow_slices)
        with multiprocessing.Pool() as pool:
            # with FakePool() as pool:
            itermap = pool.imap(moving_fn, sliced_frames)
            for i, frame in enumerate(itermap):
                movwindow_frames[i, :, :] = frame
                print(f"Got {moving_fn} of slice {movwindow_slices[i]}")
        movwindow_times = np.array(
            [moving_fn(all_sweep_times[slc]) for slc in movwindow_slices]
        )
        current_step += 1
    if current_step == 5:
        movwindow_dir: Path = mov_basedir / datetime.fromtimestamp(
            movwindow_times[0], tz=timezone.utc
        ).strftime("%Y%m%d_%H%M%S")
        if movwindow_dir.exists():
            shutil.rmtree(movwindow_dir)
        movwindow_dir.mkdir(parents=True)
        with multiprocessing.Pool() as pool:
            # with FakePool() as pool:
            frames = (movwindow_frames[i, :, :] for i in range(movwindow_frames.shape[0]))
            frame_paths = list(
                pool.starmap(
                    partial(
                        plot_frame,
                        movwindow_dir=movwindow_dir,
                        tz_plot=ZoneInfo("America/Chicago"),
                        vmin=vmin,
                        vmax=vmax,
                    ),
                    zip(frames, movwindow_times),
                )
            )
        current_step += 1
    if current_step == 6:
        frame_paths = locals()["frame_path"]
        frames_dir = frame_paths[0].parent
        mkv_name = f"{frames_dir.stem}.mkv"
        ffmpeg_cmd = f"ffmpeg -y -f image2 -pattern_type glob -framerate {framerate} -i '*.png' -vf scale=-1:1080 -c:v libx264 {mkv_name} 2>/dev/null"
        subprocess.run(ffmpeg_cmd, cwd=str(frames_dir), shell=True)
