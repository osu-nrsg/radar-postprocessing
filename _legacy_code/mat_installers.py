import os
import re
import shutil
import subprocess
from pathlib import Path
from xml.dom.minidom import parse

import typer
from typing_extensions import Annotated

install_mat_eng = typer.Typer()


@install_mat_eng.command()
def _install_mat_eng(
    matlab_root: Annotated[
        Path | None,
        typer.Option(
            help="Optional path to MATLAB installation. E.g. /opt/MATLAB/R2020b"
        ),
    ] = None,
):
    """Install the MATLAB Engine for Python in the current Python environment."""
    if not matlab_root:
        # Find MATLAB
        matlab_path = shutil.which("matlab") or shutil.which("matlab.exe")
        if not matlab_path:
            raise RuntimeError(
                "MATLAB executable not found in PATH; cannot find MATLAB Engine for"
                " Python"
            )
        matlab_root = Path(matlab_path).resolve().parent.parent
        print(f"Using MATLAB installation at {matlab_root}")

    # Get numeric version string
    with parse(str(matlab_root / "VersionInfo.xml")) as dom:
        version_str = dom.getElementsByTagName("version")[0].lastChild.data  # type: ignore  # should be an xml minidom Text node
        release_str = dom.getElementsByTagName("release")[0].lastChild.data  # type: ignore
    r2022a_or_older = re.match(r"R20\d\d[ab]", release_str) and release_str <= "R2022a"

    if r2022a_or_older:
        # Ask user to update setup() call in setup.py
        mat_engine_setup = matlab_root / "extern/engines/python/setup.py"
        print("")
        print(
            f"Edit {mat_engine_setup} in a text editor.\n"
            "At the end of the file in the setup() call, set the version= argument to\n"
            f'"{version_str}". Then after saving and closing the file, continue here.'
        )
        print("")
        input("Press Return/Enter to continue...")

    cwd = Path.cwd()
    os.chdir(matlab_root / "extern/engines/python")
    try:
        if r2022a_or_older:
            subprocess.check_call(["python", "setup.py", "install"])
        else:
            subprocess.check_call(["pip", "install", "."])
    except subprocess.CalledProcessError as err:
        raise RuntimeError("Error installing the MATLAB Engine for Python") from err
    finally:
        os.chdir(cwd)


install_ge_toolbox = typer.Typer()


@install_ge_toolbox.command()
def _install_ge_toolbox(
    install_dir: Path = typer.Option(
        Path("/shared/matlab_toolboxes/googleearth"),
        help="Path where the toolbox is to be installed",
    ),
    repo_url: str = typer.Option(
        "https://gitlab.com/osu-nrsg/google-earth-toolbox.git",
        help="URL to the git repo with the toolbox",
    ),
):
    install_dir.mkdir(exist_ok=True, parents=True)
    clone_cmd = f"git clone {repo_url} {install_dir}"
    subprocess.run(clone_cmd, shell=True)
    chmod_cmd = f"chmod -R ug+rwX {install_dir}"
    subprocess.run(chmod_cmd, shell=True)


if __name__ == "__main__":
    install_mat_eng()
