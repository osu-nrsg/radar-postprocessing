from __future__ import annotations

import io
import logging
from datetime import datetime, timezone
from pathlib import Path
from subprocess import CalledProcessError
from typing import Any

from typing_extensions import TypeGuard

# prevent error loading matlab engine
# isort: off
from wimr_postproc.mat_eng import MatlabExecutionError, get_mat_eng
import h5py

# isort: on

from wimr_postproc._logging import log_called_process_error
from wimr_postproc.errors import SpectraGenError
from wimr_postproc.params import SpectraGenParams, SpectralTimeseriesParams
from wimr_postproc.util.types import PathLike

logger = logging.getLogger(__name__)


def plot_and_send_spectra(cube_path: PathLike, params: SpectraGenParams):
    """Calculate and plot frequency-integrated wavenumber spectra at one or more locations
    in a Cartesian cube. Optionally send plots to remote server (e.g. OSU web server).

    Parameters
    ----------
    cube_path
        Path to the Cartesian cube
    params
        SpectraGenParams with the parameters for how to create the spectra, plots, and
        transfer the data to a remote location.

    Raises
    ------
    RuntimeError
        Problem starting MATLAB
    SpectraGenError
        An error has occurred in running the spectra generation MATLAB script
    """
    cube_path = Path(cube_path)
    # Get the date for the date dir
    with h5py.File(cube_path) as ds:
        start_time: float = ds["results"]["start_time"]["epoch"][0, 0]  # type: ignore
    date_dirname = datetime.fromtimestamp(start_time, tz=timezone.utc).strftime(
        "%Y-%m-%d"
    )
    if params.date_dir:
        spectra_dir = params.spectra_dir / date_dirname
    else:
        spectra_dir = params.spectra_dir

    mat_eng = get_mat_eng()
    # MATLAB scripts are in a dir relative to this module
    mat_eng.cd(str(Path(__file__).parent / "mat_spectra"))
    # change to mat script terminology
    stdout_ = io.StringIO()
    stderr_ = io.StringIO()
    errmsg = ""
    logger.debug("spectra_wrapper_args:")
    logger.debug(
        "cart_path = '%s'\n spec_dir = '%s'\n json_conf = '%s'",
        cube_path,
        spectra_dir,
        params.json(),
    )
    try:
        (
            spectra_fnames,
            intParams_fname,
            spectra_json,
            subimages_json,
        ) = mat_eng.spectra_wrapper(
            str(cube_path),
            str(spectra_dir),
            params.json(),
            stdout=stdout_,
            stderr=stderr_,
            nargout=4,
        )  # type: ignore
        logger.debug("MATLAB output:\n%s", stdout_.getvalue())
        errmsg = stderr_.getvalue()
    except MatlabExecutionError as err:
        raise SpectraGenError("Error calling spectra_wrapper") from err
    finally:
        stdout_.close()
        stderr_.close()
    if spectra_fnames == "error":
        raise SpectraGenError(f"Error running spectra_wrapper:\n{errmsg}")

    specimg_paths = [Path(spectra_dir, f) for f in spectra_fnames if f != 0]
    intParams_path = Path(spectra_dir, intParams_fname)
    if params.rsync_params:
        logger.info("Sending imagery and json files to campus.")
        fpaths = specimg_paths + [intParams_path]
        try:
            params.rsync_params.send(fpaths, dest_dir=date_dirname, check=True)
        except CalledProcessError as err:
            # Report rsync errors but do not stop execution.
            log_called_process_error(
                logger,
                err,
                "There was an error sending the spectra to campus.",
            )


def plot_and_send_spectral_timeseries(timeseries_params: SpectralTimeseriesParams):
    """Create timeseries plots of spectral parameters. Optionally send plots to remote
    server (e.g. OSU web server).

    Parameters
    ----------
    timeseries_params
        SpectralTimeseriesParams with the parameters for how to create spectral time
        series plots and transfer the data to a remote location.

    Raises
    ------
    RuntimeError
        Problem starting MATLAB
    SpectraGenError
        An error has occurred in running the spectra generation MATLAB script
    """
    mat_eng = get_mat_eng()
    # MATLAB scripts are in a dir relative to this module
    mat_eng.cd(str(Path(__file__).parent / "mat_spectra"))
    # change to mat script terminology
    stdout_ = io.StringIO()
    stderr_ = io.StringIO()
    errmsg = ""
    logger.debug("spectral_timeseries_wrapper args:")
    logger.debug("json_conf = '%s'", timeseries_params.json())
    timeseries_params_json = timeseries_params.json()
    try:
        timeseries_fpaths = mat_eng.spectral_timeseries_wrapper(
            timeseries_params_json,
            stdout=stdout_,
            stderr=stderr_,
            nargout=1,
        )  # type: ignore
        logger.debug("MATLAB output:\n%s", stdout_.getvalue())
        errmsg = stderr_.getvalue()
    except MatlabExecutionError as err:
        raise SpectraGenError("Error calling spectral_timeseries_wrapper") from err
    finally:
        stdout_.close()
        stderr_.close()
    if timeseries_fpaths == "error":
        raise SpectraGenError(f"Error running spectra_wrapper:\n{errmsg}")

    if (
        timeseries_params
        and timeseries_params.rsync_params
        and is_str_list(timeseries_fpaths)
    ):
        logger.info("Sending spectral timeseries to campus.")
        fpaths = [Path(p) for p in timeseries_fpaths]
        try:
            timeseries_params.rsync_params.send(fpaths, check=True)
        except CalledProcessError as err:
            log_called_process_error(
                logger,
                err,
                "There was an error sending the spectral timeseries to campus.",
            )


def is_str_list(val: Any) -> TypeGuard[list[str]]:
    return isinstance(val, list) and all(isinstance(v, str) for v in val)
