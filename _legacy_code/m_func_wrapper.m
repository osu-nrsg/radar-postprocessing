function m_func_wrapper(m_file_path, inputs_json, outputs_json_path)
% Call an M-file function using inputs from a JSON file and save outputs to a JSON file.
%
% Usage: m_func_wrapper(m_file_path, inputs_json_path, outputs_json_path)
%
% Inputs
% ------
% - m_file_path
%     Path to an m-file with a function to run
% - inputs_json
%     Either a JSON string starting with "[" to indicate an array, or a path to a JSON
%     file containing the inputs to the spectra wrapper function. The JSON file should
%     contain a list arguments for the function in the m file.
% - outputs_json_path
%     Path to save output. If empty or not provided, JSON is printed to stdout.

% Check inputs
narginchk(2, 3);
if ~exist(m_file_path, 'file')
    error( ...
        'm_func_wrapper:check_inputs:invalid_m_file', ...
        'The specified m-file ''%s'' does not exist.', ...
        m_file_path ...
    );
end
if inputs_json(1) ~= '['
    if ~exist(inputs_json, 'file')
        error( ...
            'm_func_wrapper:check_inputs:invalid_inputs_json_path', ...
            'The specified inputs_json path ''%s'' does not exist.', ...
            inputs_json_path ...
        );
    end
    inputs_json = fileread(inputs_json);
end
if nargin < 3
    outputs_json_path = [];
else
    [outdir, outfile] = get_outdir_and_file(outputs_json_path);
    if ~isempty(outdir) && ~exist(outdir, 'dir')
        error( ...
            'm_func_wrapper:check_inputs:invalid_output_dir', ...
            'The parent directory ''%s'' for the specified outputs_json_path does not exist.', ...
            outdir ...
        );
    end
    outputs_json_path = fullfile(outdir, outfile);  % now absolute
end

% parse and check json
inputs = jsondecode(inputs_json);
if ~iscell(inputs) || ~isvector(inputs)
    error( ...
        'm_func_wrapper:bad_input_json', ...
        'The input json file must contain a list of arguments.' ...
    )
end

% Get the dir of the m_file and go there
[m_file_dir, m_file_name, ~] = fileparts(m_file_path);
if isempty(m_file_dir)
    m_file_dir = pwd;
end
pushd(m_file_dir);

% initialize outputs cell array
clear(m_file_name);
n_argout = nargout(m_file_name);
outputs = cell(1, n_argout);
% run the function
[outputs{:}] = feval(m_file_name, inputs{:});
% return to the original dir and print or save the output to json
popd();
json_outputs = jsonencode(outputs);
if isempty(outputs_json_path)
    fprintf(json_outputs);
else
    writelines(json_outputs, outputs_json_path);
end
end  % FUNCTION m_func_wrapper

%% GET_OUTDIR_AND_FILE
function [outdir, outfile] = get_outdir_and_file(outputs_json_path)
% Split the directory from outputs_json_path or get pwd if none is provided.
% Return the directory and the full filename with extension.
[outdir, outstem, outext] = fileparts(outputs_json_path);
if isempty(outdir)
    outdir = pwd;
end
outfile = [outstem, outext];
end  % FUNCTION get_outdir_and_file


%% PUSHD
function pushd(new_dir)
global pushd_dirstack  %#ok<GVMIS>
persistent first
if isempty(first)
    first = true;
    pushd_dirstack = {};
end
pushd_dirstack = [pushd_dirstack, {pwd}];
cd(new_dir);
end  % FUNCTION pushd

%% POPD
function popd(noerror)
if nargin < 1; noerror = false; end
global pushd_dirstack  %#ok<GVMIS>
if isempty(pushd_dirstack)
    if noerror
        return;
    end
    error('m_func_wrapper:popd:empty_dir_stack', 'Cannot popd. Directory stack empty.');
end
last_dir = pushd_dirstack{end};
cd(last_dir);
pushd_dirstack(end) = [];
end  % FUNCTION popd