function [RGB, alpha] = mat2rgba(A, clim, cmap)

clen = length(cmap);

% clip the intensity values
A(A < clim(1)) = clim(1);
A(A > clim(2)) = clim(2);
nans = isnan(A);
A(nans) = clim(1);

% normalize the intensity values
A = A - clim(1);

A = A*clen/(diff(clim));

RGB = ind2rgb(int64(A), cmap);
alpha = double(~nans);