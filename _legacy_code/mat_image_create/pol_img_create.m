function [image_paths, json_path, kmz_path] = pol_img_create(cube_path, img_dir, images, varargin)
% POL_IMG_CREATE - Create radar plots from pol cubes
%
% Usage:
%  [image_paths, json_path, kmz_path] = pol_img_create(cube_path, img_dir, images, [make_json, [make_kmz]])
%
% In addition to the standalone JSON file, each image file has its own metadata stored in JSON format in the
% PNG "Description" chunk.
%
% Inputs
% ------
% cube_path : str
%   full path to pol-interp cube
% img_dir : str
%   full path to the directory to store the images and json files
% images : struct or cell
%   images is a struct, struct array, or cell array of structs.
%   possile arguments and default values:
%     d_xy: 5
%     max_r: max(cube.Rg)
%     clim: [0, 255]
%     cmap: 'fire'  % colorcet or built-in colormap name
%     img_type: snapshot (else average, or brightest)
%     snap_rot: 0 (for type snap only)
%     start_rot: 0 (for non-snap types only)
%     end_rot: end (same)
%     west: western bound of data
%     east: eastern bound of data
%     south: southern bound of data
%     north: northern bound of data
%
% "'name', value" optional inputs
% --------------------
% 'repeat_images': struct or cell
%   repeat_images is the same as images except these are images that are repeated at some period. Instead of
%   snap_rot, start_rot, and end_rot, the following fields are in 'repeat_images':
%      period_s: Number of seconds between each plot
%      duration_s: Number of seconds to use for each plot if "average" or "brightest". Defaults to period_s.
% 'make_json' : boolean
%   Should the json file be created. Default is true.
% 'make_kmz': boolean
%   Should a kmz file be created. Default is false.
% 'date_dir': boolean
%   Should output files be put into subfolders by date. Default is true
% 'replot': boolean
%   Should the images be recreated if they already exist? Default is true.
%

% Don't suggest adding newlines in MATLAB editor
%#ok<*SEPEX>
p = inputParser;
p.KeepUnmatched = true;  % don't error on extraneous fields
p.addRequired('cube_path', @(val) (ischar(val) && exist(val, 'file')) || isstruct(val));
p.addRequired('img_dir', @(val) ischar(val));
p.addRequired('images', @(val) isstruct(val) || iscell(val));
p.addParameter('repeat_images', {}, @(val) isstruct(val) || iscell(val));
p.addParameter('make_json', true);
p.addParameter('make_kmz', false);
p.addParameter('date_dir', true);
p.addParameter('replot', true);
p.parse(cube_path, img_dir, images, varargin{:});
repeat_images = p.Results.repeat_images;
make_json = p.Results.make_json;
make_kmz = p.Results.make_kmz;
date_dir = p.Results.date_dir;
replot = p.Results.replot;

% cube = matfile(cube_path, 'Writable', false);
cube = load(cube_path);
data_shape = size(cube.data);
% cubedata = cube.data(:, :, :);

if exist(img_dir, 'dir') ~= 7, mkdir(img_dir); end

% Check for kml toolbox
if make_kmz
    [mfpath, ~] = fileparts(mfilename('fullpath'));
    kml_toolbox_path = fullfile(mfpath, 'kml-toolbox');
    if ~exist(kml_toolbox_path, 'dir')
        warning('No kml-toolbox dir in m-file dir. (symlink missing?) Skipping kml creation.');
        make_kmz = false;
    else
        addpath(fullfile(mfpath, 'kml-toolbox'));
    end
end

% Make sure images is a cell array
if ~iscell(images)
    images = num2cell(images);
end
if ~iscell(repeat_images)
    repeat_images = num2cell(repeat_images);
end

rec_time_utc = epoch2Matlab(cube.timeInt(1, 1));
if date_dir
    rec_date = datestr(rec_time_utc, 'yyyy-mm-dd');
    img_dir = fullfile(img_dir, rec_date);
    if exist(img_dir, 'dir') ~= 7, mkdir(img_dir); end
end

% Get origin
results = cube.results;
origin_E = results.XOrigin;
origin_N = results.YOrigin;
origin_Zone = results.UTMZone;
[or_lat, or_lon] = UTM2ll(origin_N, origin_E, origin_Zone);

% Other results fields
heading = results.heading;
start_azi = getfield2(results, 'startAzi', 0);
start_time = results.start_time;

img_params = get_image_params(images, repeat_images, max(cube.Rg), or_lat, or_lon, data_shape(3), cube.timeInt);

%% Create each image
% Start at the highest index so that the img_meta struct array gets allocated on the first
% iteration
img_meta = struct;
image_paths = {};
for i = 1:numel(img_params)
    %% Create the image
    iopts = img_params(i);
    rots = iopts.start_rot:iopts.end_rot;
    % Get image name to check for no_replot
    [img_name, img_time_iso_str, img_time_usr_str] = get_img_name_and_tstamps( ...
        cube, iopts, rots, 'US/Pacific');
    switch iopts.img_type
        case 'snapshot'
            suffix = '_snap';
        case 'average'
            suffix = '_avg';
        case 'brightest'
            suffix = '_bright';
        otherwise
            suffix = '';
    end
    img_name = [img_name, suffix, '.png']; %#ok <AGROW>
    img_path = fullfile(img_dir, img_name);
    image_paths = [image_paths, img_path]; %#ok <AGROW>
    if ~replot && exist(img_path, 'file')
        logmsg('%s exists.\n', img_name);
        continue
    end

    % Create the grid
    [azi0_grid, r_grid, west, east, south, north] = get_image_grid(...
        iopts, or_lat, or_lon, heading, start_azi);
    % Get image from data and image grid
    [out_frame, img_meta(i).scheme] = get_out_frame(cube, iopts, rots, start_azi, azi0_grid, r_grid);

    %% Collect image metadata and convert to JSON
    img_meta(i).type = iopts.img_type;
    img_meta(i).data_timestamp = img_time_iso_str;
    img_meta(i).data_timestr = img_time_usr_str;
    if strcmp(iopts.img_type, 'snapshot')
        img_meta(i).snap_rot = iopts.snap_rot;
    else
        img_meta(i).num_rot = numel(rots);
    end
    img_meta(i).West = west;
    img_meta(i).East = east;
    img_meta(i).South = south;
    img_meta(i).North = north;
    img_meta(i).center_lat = iopts.center_lat;
    img_meta(i).center_lon = iopts.center_lon;
    img_meta(i).site_name = cube.longName;
    img_meta(i).image_name = img_name;
    json_meta = jsonencode(img_meta(i));

    %% Save image
    [RGB, alpha] = mat2rgba(out_frame, iopts.clim, iopts.cmap);
    imwrite(flipud(RGB), img_path, 'Alpha', flipud(alpha), 'Description', json_meta);
    logmsg('Created image %s.\n', img_path);
end


%% Create a JSON file with the image details
json_path = [];
if make_json
    json_file = sprintf('%s_%s.json', cube.station, datestr(rec_time_utc, 'yyyymmdd_HHMM'));
    json_path = fullfile(img_dir, json_file);
    img_meta_json = jsonencode(img_meta);
    json_fid = fopen(json_path, 'w');
    fprintf(json_fid, img_meta_json);
    fclose(json_fid);
end

kmz_path = [];
if make_kmz
    setpref('kmltoolbox', 'ShowDisclaimer', false);
    kml_name = sprintf( ...
        '%s_%s', cube.station, ...
        datestr(start_time.dateNum, 'yyyymmdd_HHMMSS'));
    k = kml(kml_name);
    for i = 1:numel(img_meta)
        im = img_meta(i);
        k.overlay(...
            im.West, im.East, im.South, im.North, ...
            'file', fullfile(img_dir, im.image_name), ...
            'name', im.type, ...
            'timeStamp', im.data_timestamp);
    end
    kmz_path = fullfile(img_dir, [kml_name, '.kmz']);
    hrefs = k.xml.getElementsByTagName('href');
    nhrefs = hrefs.getLength();
    for i = 1:nhrefs
        parts = strsplit(char(hrefs.item(i-1).getTextContent()), '/');
        hrefs.item(i-1).setTextContent(parts{end});
    end
    k.save(kmz_path, true);
end

end  %%% END pol_img_create


function result = getfield2(S, field_name, default, keepempty)
% GETFIELD2 - Get a field from a struct or return default if missing or empty
narginchk(3, 4);
if nargin < 4, keepempty = false; end
if isfield(S, field_name) && (~isempty(S.(field_name)) || keepempty)
    result = S.(field_name);
else
    result = default;
end
end


function unwrapped = dunwrap(P_d, cutoff_d, dim)
% DUNWRAP - Same as UNWRAP but P and cutoff are in degrees
P = P_d*pi/180.0;  % to radians
if nargin < 2 || isempty(cutoff_d)
    cutoff_d = 180.0;
end
cutoff = cutoff_d*pi/180.0;
if nargin < 3
    q = unwrap(P, cutoff);
else
    q = unwrap(P, cutoff, dim);
end
unwrapped = q*180.0/pi;  % back to degrees
end


function img_params = get_image_params(images, repeat_images, max_r, or_lat, or_lon, n_rot, timeInt)
% GET_IMAGE_PARAMS
% Convert `images` and `repeat_images` cell arrays of structs to a single struct array and set defaults.
img_defaults = struct( ...
    'd_xy', 5, ...
    'max_r', max_r, ...
    'cmap', 'fire', ...
    'clim', [0, 255], ...
    'img_type', 'snapshot', ...
    'snap_rot', 0, ...
    'start_rot', 0, ...
    'end_rot', [], ...
    'west', [], ...
    'east', [], ...
    'south', [], ...
    'north', [], ...
    'center_lat', or_lat, ...
    'center_lon', or_lon, ...
    'period_s', 120, ...
    'duration_s', 120);

% Get size of images and repeat_images and preallocate struct arrays
n_image_params = numel(images);
n_rpt_image_params = numel(repeat_images);
img_params = repmat(img_defaults, n_image_params, 1);  % struct array version of images
repeat_image_params = repmat(img_defaults, n_rpt_image_params, 1);  % struct array version of repeat_images

% %% Convert images and repeat_images to struct arrays with all the same field names, modifying some fields
% %% along the way.
field_names = fieldnames(img_defaults);
for i = 1:(n_image_params + n_rpt_image_params)
    if i <= n_image_params
        img = images{i};
        if isempty(img_params), img_params = struct(); end
    else
        img = repeat_images{i - n_image_params};
        if isempty(repeat_image_params), repeat_image_params = struct(); end
        img.repeat = true;
    end
    for fni = 1:numel(field_names)
        field_name = field_names{fni};
        % Set to value in images or default
        value = getfield2(img, field_name, img_defaults.(field_name));
        % Process some fields
        switch field_name
        case 'cmap'
            try
                value = colorcet(value);
            catch
                try
                    value = colormap(value);
                catch err
                    error("Invalid colormap name supplied. Must be colorcet or built-in map name.");
                end
            end
        case {'start_rot', 'end_rot', 'snap_rot'}
            if strcmp(field_name, 'end_rot') && isempty(value), value = n_rot; end
            % convert to 1-based index
            value = value + 1;
        end
        if i <= n_image_params
            img_params(i).(field_name) = value;
        else
            repeat_image_params(i - n_image_params).(field_name) = value;
        end
    end
end

% %% Generate and append params fromrepeat_image_params to image_params
new_img_params = repmat(img_defaults, 0, 0);
new_img_params_i = 0;
% convert each set of repeat params to a set of image params
for i = 1:numel(repeat_image_params)
    params = repeat_image_params(i);
    cube_duration = timeInt(end, end) - timeInt(1, 1);
    n_new_images = floor(cube_duration/params.period_s);
    for ni = 1:n_new_images
        frame_times = timeInt(1, :);
        start_t = (ni - 1)*params.period_s + frame_times(1);
        end_t = start_t + params.duration_s;
        bounds = dsearchn(frame_times', [start_t; end_t]);  % indices closest to start_t and end_t
        if strcmp(params.img_type, 'snapshot')
            params.snap_rot = bounds(1);
        else
            params.start_rot = bounds(1);
            params.end_rot = bounds(2);
        end
        new_img_params_i = new_img_params_i + 1;
        new_img_params(new_img_params_i) = params;
    end
end
if new_img_params_i > 0
    if n_image_params > 0
        img_params = [img_params, new_img_params];
    else
        img_params = new_img_params;
    end
end

end  %%% END get_image_params


function [azi0_grid, r_grid, west, east, south, north] = get_image_grid(...
    iopts, or_lat, or_lon, heading, start_azi)
% Determine image bounds
if ~isempty(iopts.west)
    west = iopts.west;
    [west_m, ~] = geodetic2enu(or_lat, west, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
else
    [~, west] = enu2geodetic(-iopts.max_r, 0, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
    west_m = -iopts.max_r;
end
if ~isempty(iopts.east)
    east = iopts.east;
    [east_m, ~] = geodetic2enu(or_lat, east, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
else
    [~, east] = enu2geodetic(iopts.max_r, 0, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
    east_m = iopts.max_r;
end
if ~isempty(iopts.south)
    south = iopts.south;
    [~, south_m] = geodetic2enu(south, or_lon, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
else
    [south, ~] = enu2geodetic(0, -iopts.max_r, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
    south_m = - iopts.max_r;
end
if ~isempty(iopts.north)
    north = iopts.north;
    [~, north_m] = geodetic2enu(north, or_lon, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
else
    [north, ~] = enu2geodetic(0, iopts.max_r, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
    north_m = iopts.max_r;
end

%% Set up output grid
n_x = (east_m - west_m)/iopts.d_xy;
n_y = (north_m - south_m)/iopts.d_xy;
lon = linspace(west, east, n_x);
lat = linspace(south, north, n_y);
[lon_grid, lat_grid] = meshgrid(lon, lat);
[x_grid, y_grid] = geodetic2enu(lat_grid, lon_grid, 0, or_lat, or_lon, 0, wgs84Ellipsoid);
[phi_grid, r_grid] = cart2pol(x_grid, y_grid);
aziT_grid = 90 - phi_grid*180/pi;
azi0_grid = mod(aziT_grid - heading - start_azi, 360);
end  %%% END get_image_grid

function [out_frame, scheme] = get_out_frame(cube, iopts, rots, start_azi, azi0_grid, r_grid)
duration_s = cube.timeInt(end, iopts.end_rot) - cube.timeInt(1, iopts.start_rot);
r_mask = (cube.Rg > 0) & (cube.Rg <= iopts.max_r);
% matfile requires numeric indicies
r_keep = find(r_mask);
r_in = single(cube.Rg(1, r_keep));
azi0 = dunwrap(cube.Azi - start_azi);
if strcmp(iopts.img_type, 'snapshot')
    azi_mask = ~cube.Azi_oob(:, iopts.snap_rot);
    azi_keep = find(azi_mask);
    scheme = sprintf('Rotation %d', iopts.snap_rot);
    in_frame = single(cube.data(r_keep, azi_keep, iopts.snap_rot));
else
    azi_mask = ~min(cube.Azi_oob, [], 2);
    azi_keep = find(azi_mask);
    if strcmp(iopts.img_type, 'average')
        % Average
        scheme = sprintf('%.1f-min time exposure', duration_s/60);
        in_frame = mean(cube.data(r_keep, azi_keep, rots), 3);
    elseif strcmp(iopts.img_type, 'brightest')
        % Brightest-pixel
        scheme = sprintf('%.1f-min brightest-pixel image', duration_s/60);
        in_frame = single(max(cube.data(r_keep, azi_keep, rots), [], 3));
    end
end
azi0_in = single(azi0(1, azi_keep));
out_frame = interp2(azi0_in, r_in, in_frame, azi0_grid, r_grid);
end  %%% END get_out_frame


function [img_name, img_time_iso_str, img_time_usr_str] = get_img_name_and_tstamps(cube, iopts, rots, tz_name)
% GET_IMG_NAME_AND_TSTAMPS - Get the image name and time strings
if strcmp(iopts.img_type, 'snapshot')
    img_time_utc = epoch2Matlab(cube.timeInt(1, iopts.snap_rot));
else
    img_time_utc = epoch2Matlab(cube.timeInt(1, rots(end)));
end
img_time_iso_str = datestr(img_time_utc, 'yyyy-mm-ddTHH:MM:SSZ');
[img_time_pac, ~, ~] = timezone_convert(img_time_utc, 'UTC', tz_name);
img_time_usr_str = sprintf('%s Pacific', datestr(img_time_pac, 'yyyy-mm-dd HH:MM:SS'));
img_name = sprintf('%s_%s', cube.station, datestr(img_time_utc, 'yyyymmdd_HHMMSS'));
end  %%% END get_img_name_and_tstamps

function logmsg(varargin)
fprintf(2, varargin{:});
end
