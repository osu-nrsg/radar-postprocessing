function text_overlay(img_path, annotation, gravity, x, y, varargin)
% Select ImageMagick options. See ImageMagick doc for more info.
% For color ad undercolor, see https://www.imagemagick.org/script/color.php

% set up input params
p = inputParser;
p.addParameter('out_path', img_path);
p.addParameter('font', 'Arial');
p.addParameter('fontsize', 56);
p.addParameter('fontwidth', 5);
p.addParameter('color', 'red');
p.addParameter('bgcolor', '#000a');
% Get params from parser
p.parse(varargin{:});
out_path = p.Results.out_path;
font = p.Results.font;
fontsize = p.Results.fontsize;
fontwidth = p.Results.fontwidth;
color = p.Results.color;
bgcolor = p.Results.bgcolor;
% Make annotation
% set up ImageMagick options
options = {'-density 96', ...
           sprintf('-font ''%s''', font), ...
           sprintf('-pointsize %d', fontsize), ...
           sprintf('-stroke %s', color), ...
           sprintf('-strokewidth %d', fontwidth), ...
           sprintf('-fill %s', color), ...
           sprintf('-undercolor "%s"', bgcolor), ...
           sprintf('-gravity %s', gravity), ...
           sprintf('-annotate %+d%+d ''%s''', x, y, annotation)};
% run command and check result
cmd = strjoin(['convert', img_path, options, out_path, '2>&1']);
[result, stdout_] = system(cmd);
if result ~= 0
    error('polMat2TimexKmz:convert_failure', 'Error annotating image: %s', stdout_);
end
