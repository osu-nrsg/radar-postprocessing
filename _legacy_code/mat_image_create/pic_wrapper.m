function [image_paths, json_path] = pic_wrapper(cube_path, json_params)
% PIC_WRAPPER - Wrapper of pol_image_create for usage with the MATLAB engine for Python
%
% Usage:
%  [image_paths, json_path] = pic_wrapper(cube_path, img_dir, json_params)
%
% Inputs
% ------
% Required are the same as for pol_image_create, except for json_params, which is params encoded in
% JSON. No optional inputs are received; date_dir is set to true in pol_image_create
%
% Returns
% -------
% image_paths - list of paths to the created images, or 'error' if error
% json_path - Path to JSON file with metadata, or 'error' if error
%
% Raises
% ------
% If there is any error in pol_image_create, the error is printed to stderr, and json_path is
% returned as 'error'
try
    params = jsondecode(json_params);
    img_dir = params.img_dir;
    images = params.images;
    repeat_images = params.repeat_images;
    if isempty(images), images = {}; end
    if isempty(repeat_images), repeat_images = {}; end
    argin = struct2varargin(params, {'images', 'repeat_images', 'img_dir'});
    % do not capture kmz_path output as that feature is not currently validated
    [image_paths, json_path] = pol_img_create(cube_path, img_dir, images, 'repeat_images', repeat_images, argin{:});
catch err
    fprintf(2, getReport(err, 'extended', 'hyperlinks', 'off'));
    image_paths = 'error';
    json_path = 'error';
    % kmz_path = 'error';
end
if isempty(json_path)
    json_path = '';
end
% if isempty(kmz_path)
%     kmz_path = '';
% end

end

function C = struct2varargin(S, excluded_fields)
    if ~isstruct(S) || ~isscalar(S)
        error('struct2varargin:not_scalar_struct', 'S must be a scalar struct');
    end
    if nargin < 2
        excluded_fields = {};
    end
    fns = fieldnames(S);
    C = {};
    for i = 1:numel(fns)
        fn = fns{i};
        if ~ismember(fn, excluded_fields)
            val = S.(fn);
            if isempty(val)
                val = {[]};
            end
            C = [C, fn, val];  %#ok<AGROW>
        end
    end
end