import io
import logging
from pathlib import Path
from subprocess import CalledProcessError

from pydantic import FilePath, validate_call

from wimr_postproc._logging import log_called_process_error
from wimr_postproc.mat_eng import MatlabExecutionError, get_mat_eng
from wimr_postproc.params import KmzParams

logger = logging.getLogger(__name__)


@validate_call
def make_and_send_kmz(cube_path: FilePath, params: KmzParams) -> Path | None:
    """Create a KMZ file with an image overlay from a cube file

    Inputs
    ------
    cube_path
        Path to the cube from which to create the imagery
    params
        Parameters for polMat2TimexKmz

    Returns
    -------
    A path to the created KMZ file, or None if there is an error
    """
    mat_eng = get_mat_eng()
    kmz_dir = cube_path.parent if params.kmz_dir is None else params.kmz_dir
    kmz_path = Path(kmz_dir, f"{cube_path.stem}.kmz")
    mat_eng.cd(str(Path(__file__).parent / "mat_kmz"))
    stdout_ = io.StringIO()
    try:
        mat_eng.polMat2TimexKmz(
            str(cube_path.absolute()),
            str(kmz_path.absolute()),
            str(params.kmz_params_path.absolute()),
            stdout=stdout_,
            nargout=0,
        )
        stdout_.seek(0, 0)
        logger.debug("MATLAB output:\n%s", stdout_.read())
        stdout_.close()
    except MatlabExecutionError:
        logger.exception("Error calling polMat2TimexKmz.m")
        mat_eng.exit()
        return None
    if params.rsync_params:
        logger.info("Sending KMZ to campus.")
        try:
            params.rsync_params.send(kmz_path, check=True)
        except CalledProcessError as err:
            # Report rsync errors but do not stop execution.
            log_called_process_error(
                logger,
                err,
                "There was an error sending the KMZ to campus.",
            )
    return kmz_path
