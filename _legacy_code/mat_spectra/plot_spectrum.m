function handles = plot_spectrum(intSkkf, subimage, kplot, rec_time, longName, varargin)
% Required inputs:
%   - intSkkf:
%       Frequency-integrated wavenumber spectrum
%   - subimage:
%       Struct defining the boundaries of the subimage from which the spectrum
%       was derived. Also contains the calculated SNRs for the spectra
%   - kplot:
%       Wavenumbers to use on the radial axis of the wavenumber plot
%   - rec_time:
%       2-element vector of datenums specifying the start and end of the
%       timerange from which the spectrum was calculated.
%   - longName:
%       Site name to use in spectum plot title
%
% Optional inputs (provided as 'name', value):
%   - plot_timezone:
%       Timezone ID to use for the plot timestamps. Note that if the timezone is
%       of the form 'US/Pacific', then the 'US/' will be stripped off for the
%       plot title. For more info see the help for
%       timezone_convert. Defaults to 'UTC'.
%   - wdir_is_heading:
%       Defines whether the wave angle is the direction the waves are
%       heading (true), or the direction from which the waves are
%       coming (false, default).
%   - si_name_in_title:
%       If provided and true, then the name and location of the site are
%       included in the title of the plot. Defaults to false.
%   - h_ax:
%       Axes handle. If provided, the spectrum is plotted on the specifed axes.
%       Note that any other plots on the same figure that use a color axis will
%       take on the colormap used in this plot (since, unfortunately, there can
%       be only one colormap per figure). If not provided, gca is used to either
%       grab the current axes or create a new set of axes.
%   - card_dir:
%       If true, print peak direction as cardinal direction, e.g. 'NNE'
%       instead of '24'. Default is false;

% constants
SECONDARY_PEAK_LIMIT = 0.5;  % Only keep secondary peaks this fraction of the primary peak
L_MIN = 75;
CIRCLE_WAVELENGTHS = [L_MIN 100 150 200 300 500];
NRADIALS = 16;

%% Process inputs
narginchk(5, 14);

%%% defaults
plot_timezone = 'UTC';
wdir_is_heading = false;
si_name_in_title = false;
card_dir = false;

%%% process varargin
i = 1;
while i <= length(varargin)
    switch varargin{i}
        case 'plot_timezone'
            plot_timezone = varargin{i+1};
            i = i + 1;
        case 'wdir_is_heading'
            wdir_is_heading = varargin{i+1};
            i = i + 1;
        case 'si_name_in_title'
            si_name_in_title = varargin{i+1};
            i = i + 1;
        case 'h_ax'
            h_ax = varargin{i+1};
            i = i + 1;
        case 'card_dir'
            card_dir = varargin{i+1};
            i = i + 1;
        otherwise
            error('plot_spectrum:badArg', 'Bad input argument');
    end
    i = i + 1;
end

%%% input checking
if ~isstruct(subimage)
    error('plot_spectrum:subimageNoGood', 'The subimage argument must be a struct.');
end

if exist('h_ax', 'var')
    if ~ishandle(h_ax) || ~strcmp(get(h_ax, 'Type'), 'axes')
        error('plot_spectrum:bad_h_ax', 'h_ax must be a valid axes handle.');
    end
    h_fig = get(h_ax, 'Parent');
else
    if ~isempty(get(0, 'CurrentFigure'))
        h_fig = get(0, 'CurrentFigure');
        if ~isempty(get(h_fig, 'CurrentAxes'))
            h_ax = get(h_fig, 'CurrentAxes');
        else
            h_ax = axes;
        end
    else
        h_fig = figure;
        h_ax = axes;
    end

end

if ~(isnumeric(rec_time) && isvector(rec_time) && length(rec_time) == 2)
    error('plot_spectrum:bad_rec_time', ...
          ['rec_time must be a two-element datenum vector with the' ...
           ' start and end time of the data.']);
end

set(0, 'CurrentFigure', h_fig);
set(h_fig, 'CurrentAxes', h_ax);
cla(h_ax);

%% Prepare for plotting
if ~wdir_is_heading
    intSkkf = rot90(intSkkf, 2);
end
intSkkf_plot = intSkkf;
intSkkf_plot(intSkkf == 0) = NaN;
kmax = 2 * pi / L_MIN;% (Lmin + 10); % wavenumber limit (outer ring) for the spectral plot.

%%% get colormap - variable name is SpecIJRS
load('colormap_SpecIJRS', 'SpecIJRS');

%%% Prepare plot label
% time range for recording
rec_time = timezone_convert(rec_time, 'UTC', plot_timezone); % convert datenums to the desired time zone.
rec_time_str = datestr(rec_time, 'yyyy-mm-dd HH:MM PM');  %#ok<DATST>
if strcmp(plot_timezone(1:3), 'US/'), plot_timezone = plot_timezone(4:end); end % remove "US/" from "US/Pacific", etc.
timestr = [rec_time_str(1, 1:16) ' - ' strtrim(rec_time_str(2, 12:19)) ' ' plot_timezone];

plt_title{1} = 'Frequency-integrated Wavenumber Spectrum';
plt_title{2} = [longName ' -- ' timestr];
[center_lat, center_lon] = UTM2ll(subimage.center_y, subimage.center_x, subimage.UTMZone);
if si_name_in_title
    plt_title{3} = sprintf(...
        '%s region - %.5g°, %.5g°', subimage.name, center_lat, center_lon);
end

%% Create the plot
[~, h_contour] = contour(h_ax, kplot, kplot, intSkkf_plot, 30);
set(h_ax, 'DataAspectRatio', [1 1 1]);
axis(h_ax, 'tight');
axis(h_ax, 'off');
colormap(SpecIJRS); %colorbar;
axis(h_ax, [-kmax kmax -kmax kmax]);
h_title = title(h_ax, plt_title, 'FontWeight', 'bold');

hold on

% Plot polar axes on top of contours
circle_radii = 2*pi./CIRCLE_WAVELENGTHS;
handles = plot_polar_axes(h_ax, NRADIALS, kmax, circle_radii, wdir_is_heading);

%% Create Peak arrow[s] and textbox
if wdir_is_heading
    wavedir_str = 'toward';
else
    wavedir_str = 'from';
end

peak_text = {'Peak Wavelengths:'};
first_peak = 0;
arrow_handles = {};
for i = 1:numel(subimage.kpeaks)
    if i == 1
        first_peak = subimage.kpeaks(i).peak;
    elseif subimage.kpeaks(i).peak < (SECONDARY_PEAK_LIMIT * first_peak)
        % Peak is too small to include
        continue
    end
    switch i
        case 1
            color_ = [0.85, 0, 0];
        case 2
            color_ = [1, 0.6, 0.6];
    end
    if ismember(i, [1, 2])
        arrow_handles{i} = peak_arrow( ...
            subimage.kpeaks(i).ang, subimage.kpeaks(i).k, wdir_is_heading, ...
            'LineWidth', 1.5, ...
            'Color', color_, ...
            'EdgeColor', color_,  ...
            'FaceColor', color_, ...
            'headlength', 0.007, ...
            'headwidth', 0.004 ...
        ); %#ok<AGROW>
    end

    % Get proper angle direction
    if wdir_is_heading
        ang = subimage.kpeaks(i).ang;
    else
        ang = mod(subimage.kpeaks(i).ang - 180, 360);
    end
    if card_dir
        ang_str = deg2card(ang);
    else
        ang_str = sprintf('%.f°', ang);
    end
    peak_text = [ ...
        peak_text, ...
        sprintf('- %.f m %s %s', subimage.kpeaks(i).l, wavedir_str, ang_str) ...
    ]; %#ok<AGROW>
end
peak_text = [ ...
    peak_text, ...
    '', ...
    'Peak period:', ...
    sprintf('- %.1f s', subimage.int_Tpeak) ...
];
h_txt_stats = annotation(...
    'textbox', [0.02, 0.789, 0.23, 0.13], ...
    'String', peak_text, ...
    'LineStyle', 'none', ...
    'FitBoxToText', true, ...
    'FontWeight', 'bold', ...
    'HorizontalAlignment', 'left' ...
);
% Make first arrow be on top
uistack(arrow_handles{1}, 'top');

% %% consolidate handles
handles.fig = h_fig;
handles.ax = h_ax;
handles.contour = h_contour;
handles.title = h_title;
handles.txt.stats = h_txt_stats;

end % END function plot_spectrum

function handles = peak_arrow(angpeak, kpeak, wdir_is_heading, varargin)

if ~wdir_is_heading
    angpeak = mod(angpeak - 180, 360);
end

% %% Plot arrow from the peak wavenumber
[pkx, pky] = pol2cart(deg2rad(mod(90 - angpeak, 360)), kpeak);
% direction determines where the arrow head is
if wdir_is_heading
    %         q_h = quiver(0, 0, pkx, pky, 'k', 'LineWidth', 2.5);
    handles = plot_arrow(0, 0, pkx, pky, varargin{:});
else
    handles = plot_arrow(pkx, pky, 0, 0, varargin{:});
end

end  % END function peak_arrow

function handles = plot_polar_axes(h_ax, nradials, kmax, circle_radii, wdir_is_heading)
% Create the radials and circles to use as axes for the plot
% Plot W, E, S labels
handles.txt.W = text(-kmax*1.1, 0, 'W', 'FontSize', 12, 'VerticalAlign', 'middle');
handles.txt.E = text(kmax*1.1, 0, 'E', 'FontSize', 12, 'VerticalAlign', 'middle');
handles.txt.S = text(0, -kmax*1.1, 'S', 'FontSize', 12, 'HorizontalAlign', 'center');

%% Plot radials
dphi = 2*pi/nradials;
phi = 0:dphi:(2*pi - dphi);
handles.radials = zeros(size(phi));
for idx = 1:numel(phi)
    handles.radials(idx) = plot(h_ax, [0 kmax*cos(phi(idx))], [0 kmax*sin(phi(idx))], 'color', [0.5 0.5 0.5]);
end


%% plot wavenumber circles
handles.circles = zeros(size(circle_radii));
handles.txt.circles = zeros(size(circle_radii));

% circle labels positioned on "land" side
if wdir_is_heading
    circle_txt_phi = 5*pi/4;
else
    circle_txt_phi = 7*pi/4;
end

for ri = 1:length(circle_radii)
    r = circle_radii(ri);
    xc = r * sin(-pi:pi/500:pi);
    yc = r * cos(-pi:pi/500:pi);
    xl = r * cos(circle_txt_phi);
    yl = r * sin(circle_txt_phi);
    if r == kmax
        circle_txt = sprintf('wavelength L = %.f m', 2*pi./r);
        linewidth = 1.5;
        linecolor = [0, 0, 0];
    else
        circle_txt = sprintf('%.f', 2*pi./r);
        linewidth = 1;
        linecolor = [0.5, 0.5, 0.5];
    end
    handles.circles(ri) = plot(h_ax, xc, yc, 'Color', linecolor, 'LineWidth', linewidth);
    handles.txt.circles(ri) = text( ...
        xl, yl, circle_txt, ...
        'FontSize', 8, ...
        'FontWeight', 'bold', ...
        'HorizontalAlignment', 'center', ...
        'backgroundcolor', 'none');
end
end  % END FUNCTION plot_circles