function result = filled_ring(inner_radius, outer_radius, matrix, ring_val, masked_val)
% FILLED_RING Mask a matrix to preserve only a ring shape.
%   FILLED_RING(inner_radius, outer_radius, matrix) applies a mask to
%   matrix such that only the values between inner_radius and outer_radius
%   are preserved and all other values are set to 0.
%
%   Notes:
%   - inner_radius and outer_radius units are matrix "pixels".
%   - The ring is always oriented on the center of the matrix.
%   - A more advanced algorithm is needed to create a cicular ring if the
%     matrix's "x-units" are different from its "y-units".
%
%   FILLED_RING(inner_radius, outer_radius, matrix, ring_val) sets the
%   values inside the ring to ring_val instead of keeping their existing
%   values. To retain their values, set ring_val to [].
%
%   FILLED_RING(inner_radius, outer_radius, matrix, ring_val, masked_val)
%   sets the values outside the ring to masked_val instead of 0. To retain
%   their values, set masked_val to [].
%
%   So for example, to mask out the ring and keep only the values inside
%   the inner raidus and outside the outer radius, use:
%
%     FILLED_RING(inner_radius, outer_radius, matrix, 0, []);
%
if nargin < 4
    ring_val = [];
end
if nargin < 5
    masked_val = 0;
end
if ~isempty(ring_val) && (~isnumeric(ring_val) || ~isscalar(ring_val))
    error('filled_ring:invalid_ring_val', 'ring_val must be a numeric scalar or empty array');
end
if ~isempty(masked_val) && (~isnumeric(masked_val) || ~isscalar(masked_val))
    error('filled_ring:invalid_masked_val', 'masked_val must be a numeric scalar or empty array');
end

nrow = length(matrix(:, 1));
ncol = length(matrix(1, :));

newMatrix = zeros(nrow, ncol);

if mod(nrow, 2) == 0
    verticalCenter = (nrow / 2) + 1;
else
    verticalCenter = ceil(nrow / 2 + 0.5);
end

if mod(ncol, 2) == 0
    horizontalCenter = (ncol / 2) + 1;
else
    horizontalCenter = ceil(ncol / 2 + 0.5);
end

for i = 1:nrow
    for j = 1:ncol
        if sqrt((i - verticalCenter) * (i - verticalCenter) + ...
                (j - horizontalCenter) * (j - horizontalCenter)) < outer_radius
            if isempty(ring_val)
                newMatrix(i, j) = matrix(i, j);
            else
                newMatrix(i, j) = ring_val;
            end
        end
        if sqrt((i - verticalCenter) * (i - verticalCenter) + ...
                (j - horizontalCenter) * (j - horizontalCenter)) < inner_radius
            if isempty(masked_val)
                newMatrix(i, j) = matrix(i, j);
            else
                newMatrix(i, j) = masked_val;
            end
        end
    end
end

result = newMatrix;
