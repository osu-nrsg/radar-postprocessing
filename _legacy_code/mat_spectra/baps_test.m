spectra_conf = jsondecode('{"spectra_dir": "/data/spectra", "date_dir": true, "plot_timezone": "US/Pacific", "si_name_in_title": true, "wdir_is_heading": false, "rsync_params": {"dest_user": "pittmara", "dest_server": "access.engr.oregonstate.edu", "dest_base_dir": "/nfs/depot/cce_u1/haller/shared/public_html/Newport/spectra", "options": "-t --perms --chmod=ugo+rw", "ssh_key": "/home/daq-user/.ssh/id_rsa_OSU_transfer"}, "subimages": [{"center_x": 413632, "center_y": 4942403, "name": "Nye Beach", "size": 128, "h": 18.6}, {"center_x": 413713, "center_y": 4938053, "name": "South Beach", "size": 128, "h": 13.8}, {"center_x": 413251, "center_y": 4939929, "name": "Yaquina Inlet", "size": 128, "h": 18.0}]}');
cube_paths = dir('/data/cubes/**/*cart*.mat');
for cpi = numel(cube_paths):-1:1
    cube_path = fullfile(cube_paths(cpi).folder, cube_paths(cpi).name);
    cube = load(cube_path);
    build_and_plot_spectra('cube', cube, 'dir_res', sprintf('%s/spectra2', pwd), 'si_name_in_title', true, 'subimages', spectra_conf.subimages);
    break
end