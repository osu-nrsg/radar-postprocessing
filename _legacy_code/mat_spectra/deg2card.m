function bearing = deg2card(d, num_letters)
% Function DEG2CARD
%
% Usage:
%   bearing = deg2card(d)
%   bearing = deg2card(d, num_letters)
%
% Description:
%   Get a string representation of cardinal degrees, up to three letters.
%
% Inputs:
% - d
%     degree value
% - num_letters 
%     Number of letters to use, between 1 and 3. Default is 3.
%
% Outputs:
% - bearing
%     string bearing designator
%
% Examples:
% 
% >> deg2card(24)
% ans =
%     'NNE'
%
% >> deg2card(24, 2)
% ans =
%     'NE'
%

narginchk(1, 2);
if ~exist('num_letters', 'var')
    num_letters = 3;
end

%%% Get the dir strings for the number of letters selected
dirs = {'N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', ...
        'NW', 'NNW'};
switch num_letters
    case 1
        dirs = dirs(1:4:16);
    case 2
        dirs = dirs(1:2:16);
    case 3
        % do nothing
    otherwise
        error('deg2card:invalid_num_letters', 'num_letters must be an integer between 1 and 3');
end
        
ndirs = numel(dirs);
dir_width = 360/ndirs;
ix = round((d) / dir_width);
bearing = dirs{mod(ix, ndirs) + 1};
