function plot_paths = TimeSeriesPlots2( ...
    end_datenum, days_to_plot, path_to_search, img_dir, wdir_lims, plot_timezone, wdir_is_heading)
% FUNCTION TimeSeriesPlots2
% Purpose:
%   To plot a time series of the int_lpeaks, int_Tpeaks, and int_angpeaks
%   parameters from intParams files, as created by build_and_plot_spectra.m.
%
% Usage:
%   TimeSeriesPlots( ...
%       end_datenum, days_to_plot, path_to_search, img_dir, wdir_lims, plot_timezone, wdir_is_heading)
%
% Inputs:
%   - end_datenum:
%       MATLAB datenum specifying the ending time boundary for the plots
%       (in UTC). Typical usage is to use the |now| function here.
%   - days_to_plot:
%       Number of days (can be fractional) of history to plot. Note the
%       following behavior triggered by this value:
%           - 2 or less:
%               Plots have x-tick marks every 4 hours and no date is
%               specified on the tick marks.
%           - 7 or less (greater than 2):
%               Plots have tick marks every 6 hours which display the date
%               and time
%           - Greater than 7
%               Plots have tick marks every day, displaying the date only.
%   - path_to_search:
%       Path to search for dated subfolders containing the intParams files.
%       Subfolders should be named in the format |yyyy-mm-dd|.
%   - img_dir
%       Destination directory for timeseries plots.
%   - wdir_lims
%       Limits of reasonable wave direction angles for the site (degrees
%       from North). This should be relative to the wdir_is_heading
%       parameter, below.
%   - plot_timezone:
%       (Optional) Timezone ID to use for the plot timestamps. Note that if
%       the timezone is of the form 'US/Pacific', then the 'US/' will be
%       stripped off for the plot title. For more info see the help for
%       radar_toolbox.timezone_convert. Defaults to 'UTC'.
%   - wdir_is_heading:
%       (Optional) Defines whether the wave angle is the direction the
%       waves are heading (true), or the direction from which the
%       waves are coming (false, default).
%
% Outputs:
%   - plot_paths: Paths to the plots generated
%

% FUTURE: Consider using kpeaks to get secondary peaks

%% Constants
DPI = 150;
ANG_TICKSIZE = 30;  % should be a factor of 90.

%% Inputs and setup
% Check number of input arguments
narginchk(5, 7);

if ~(isnumeric(wdir_lims) && numel(wdir_lims) == 2)
    error('TimeSeriesPlots2:invalid_wdir_lims', 'wdir_lims must be a vector of two numbers');
end
wdir_lims = mod(wdir_lims, 360);
if diff(wdir_lims) < 0
    wdir_lims(2) = wdir_lims(2) + 360;
end

%%% Defaults for optional arguments
if nargin < 5 || isempty(plot_timezone)
    plot_timezone = 'UTC';
end
if nargin < 6 || isempty(wdir_is_heading)
    wdir_is_heading = false;
end

% get the name of the timezone for plotting (remove "US/" if of form
% "US/Pacific", etc.)
if strcmp(plot_timezone(1:3), 'US/')
    tz_name = plot_timezone(4:end);
else
    tz_name = plot_timezone;
end

% get starting time by subtracting the # of days to plot from the end_datenum
start_datenum = end_datenum - days_to_plot;
iP_paths = select_iP_paths(path_to_search, start_datenum, end_datenum);

% round the start and end times to the nearest hour (down at the start, up
% at the end)
plot_time = end_datenum;
dn_hour = datenum([0 0 0 1 0 0]);
plot_start_datenum = floor(start_datenum / dn_hour) * dn_hour;
plot_end_datenum = ceil(end_datenum / dn_hour) * dn_hour;
% Get stats from intParams files
stats = accumulate_spectral_stats(iP_paths);
load(iP_paths{1}, 'site');
station_nsname = site.station;
station_name = regexprep(station_nsname, '(\w)([A-Z]+)', '$1 $2');

%% Plotting set-up
% Convert datenums:
% We want to plot in the desired time zone, so we'll change the datenum
% values all to the desired time zone.
datetimes = timezone_convert(stats.time, 'UTC', plot_timezone);
plot_end_datenum = timezone_convert(plot_end_datenum, 'UTC', plot_timezone);
plot_start_datenum = timezone_convert(plot_start_datenum, 'UTC', plot_timezone);
plot_time = timezone_convert(plot_time, 'UTC', plot_timezone);

% Set up x-ticks
tick_limit = 10; % Limit the number of x-ticks
min_ticksize = (plot_end_datenum - plot_start_datenum) / tick_limit; % This is the smallest increment we will allow

% We're ok with tick sizes of the following sizes (or multiples of the largest)
allowed_ticksizes = [dn_hour * [1 2 3 4 6 12] 1];
allowed_ticksizes = sort(allowed_ticksizes); % make sure smallest to largest
ticksize = allowed_ticksizes(find(min_ticksize < allowed_ticksizes, 1, 'first'));
if isempty(ticksize)
    % Round min_ticksize up to a multiple of the largest allowed ticksize
    ticksize = ceil(min_ticksize / allowed_ticksizes(end)) * allowed_ticksizes(end);
end

% Now, we need to find the limits of the ticks. Round the limits out to a
% ticksize multiple.
start_xtick = floor(plot_start_datenum / ticksize) * ticksize;
end_xtick = ceil(plot_end_datenum / ticksize) * ticksize;
xticks = start_xtick:ticksize:end_xtick;

% The x limits should be outside the date range. We'll just round out to
% the nearest hour
x_lims = [start_xtick end_xtick];

% Make the tick labels
xtick_labels = cell(size(xticks));
if days_to_plot <= 10
    for i = 1:numel(xticks)
        if floor(xticks(i)) == xticks(i) % midnight tick
            xtick_labels{i} = datestr(xticks(i), 'mm/dd');
        else
            xtick_labels{i} = datestr(xticks(i), 'HHMM');
        end
    end
else
    for i = 1:numel(xticks)
        xtick_labels{i} = datestr(xticks(i), 'dd');
    end
end

% Set up timestr for titles
if days_to_plot <= 2
    timestr = [num2str(ceil(days_to_plot * 24)) '-Hour'];
    timeabbr = [num2str(ceil(days_to_plot * 24)) 'H'];
elseif days_to_plot <= 61
    timestr = [num2str(ceil(days_to_plot)) '-Day'];
    timeabbr = [num2str(ceil(days_to_plot)) 'D'];
else
    timestr = [num2str(ceil(days_to_plot / 30.4)) '-Month'];
    timeabbr = [num2str(ceil(days_to_plot / 30.4)) 'Mo'];
end

%% Create the figures
h_fig = zeros(3, 1);
plot_paths = cell(3, 1);
for i = 1:3
    h_fig(i) = figure;
    % Make the paper size half as tall as the default, and make the figure
    % the same size.
    set(h_fig(i), 'Units', 'points');
    set(h_fig(i), 'PaperUnits', 'points');
    fig_Pos = get(h_fig(i), 'Position');
    fig_PaperPos = get(h_fig(i), 'PaperPosition');
    set(h_fig(i), 'PaperPosition', [fig_PaperPos(1:3) fig_PaperPos(4) * 0.5]);
    fig_PaperPos = get(h_fig(i), 'PaperPosition');
    set(h_fig(i), 'Position', [fig_Pos(1:2) fig_PaperPos(3:4)]);

    %% Plot peaks
    % Plot
    switch (i)
        case 1
            angpeaks = stats.angpeak;
            if ~wdir_is_heading
                angpeaks = mod(angpeaks + 180, 360);
            else
                angpeaks = mod(angpeaks, 360);
            end
            if wdir_lims(2) > 360
                angpeaks = rad2deg(unwrap(deg2rad(angpeaks)));
            end
            yt0 = ceil(wdir_lims(1) / ANG_TICKSIZE) * ANG_TICKSIZE;
            yt1 = floor(wdir_lims(2) / ANG_TICKSIZE) * ANG_TICKSIZE;
            y_ticks = yt0:ANG_TICKSIZE:yt1;
            plot( ...
                datetimes, angpeaks, 'LineStyle', 'none', 'Marker', 'o', ...
                'MarkerEdgeColor', [0 0.5 0], 'MarkerFaceColor', [0 0.5 0], 'MarkerSize', 4 ...
            );
            set(gca, 'YGrid', 'on');
            ylim(wdir_lims);
            yticks(y_ticks);
            if any(yticks > 360)
                yticklabels(arrayfun(@num2str, mod(y_ticks, 360), 'UniformOutput', false));
            end
            if wdir_is_heading
                ylabel('Wave Peak Dir. deg. (heading)');
            else
                ylabel('Wave Peak Dir. deg. (from)');
            end
            title_text = 'Peak Wave Direction';
            plot_name = sprintf('%s_%s_wave_dir_timeseries.png', station_nsname, timeabbr);
        case 2
            plot( ...
                datetimes, stats.Tpeak, 'LineStyle', 'none', 'Marker', 'o', ...
                'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b', 'MarkerSize', 4 ...
            );
            ylim([5 20]);
            ylabel({'Period'; '(s)'});
            set(gca, 'YGrid', 'on');
            set(gca, 'YMinorGrid', 'on');
            title_text = 'Peak Period';
            plot_name = sprintf('%s_%s_wave_T_timeseries.png', station_nsname, timeabbr);
        case 3
            plot( ...
                datetimes, stats.lpeak, 'LineStyle', 'none', 'Marker', 'o', ...
                'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r', 'MarkerSize', 4 ...
            );
            ylim([50 300]);
            ylabel({'Wavelength'; '(m)'});
            set(gca, 'YGrid', 'on');
            set(gca, 'YMinorGrid', 'on');
            title_text = 'Peak Wavelength';
            plot_name = sprintf('%s_%s_wave_L_timeseries.png', station_nsname, timeabbr);
        otherwise
            % should never be here!
            assert(false, 'bad iterator');
    end

    % x axis limits
    xlim(x_lims);

    % x-axis ticks
    set(gca, 'XTick', xticks);
    set(gca, 'XTickLabel', xtick_labels);

    title({ ...
        sprintf('%s Time Series of %s', timestr, title_text); ...
        sprintf('%s %s, %s station', datestr(plot_time, 'yyyy-mm-dd HH:MM PM'), tz_name, station_name) ...
        });

    plot_paths{i} = fullfile(img_dir, plot_name);
    dpistr = sprintf('-r%.f', DPI);
    print(gcf, '-dpng', plot_paths{i}, dpistr);

end
close(h_fig);

end  % END FUNCTION TimeSeriesPlots2
