function timeseries_fpaths = spectral_timeseries_wrapper(timeseries_options)
% Wrapper around TimeSeriesPlots2 that can be used by the MATLAB API for Python.
%
% Usage: timeseries_fpaths = spectral_timeseries_wrapper(timeseries_options)
%
% Inputs
% ------
% - timeseries_options
%     JSON-encoded parameters for generating timeseries plots with TimeSeriesPlots2
%
% Returns
% -------
% timeseries_fpaths
%    Paths to timeseries plots
%

try
    options = jsondecode(timeseries_options);
    timeseries_fpaths = TimeSeriesPlots2( ...
        timezone_convert(now, [], 'UTC'), options.days_to_plot, options.spectra_dir, options.spectra_dir, ...
        options.wdir_lims, options.plot_timezone, options.wdir_is_heading ...
    );
catch err
    fprintf(2, getReport(err, 'extended', 'hyperlinks', 'off'));
    timeseries_fpaths = 'error';
end
