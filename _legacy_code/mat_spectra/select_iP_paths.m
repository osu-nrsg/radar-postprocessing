function iP_paths = select_iP_paths(path_to_search, start_datenum, end_datenum)
% FUNCTION select_iP_paths
% Purpose:
%   Find intParams files from a specific time period
%
% Usage:
%   iP_paths = select_iP_paths(path_to_search, end_datenum, days_to_include)
%
% Inputs:
%   - start_datenum:
%       MATLAB datenum specifying the starting time boundary for the plots
%       (in UTC).
%   - end_datenum:
%       MATLAB datenum specifying the ending time boundary for the plots
%       (in UTC).
%   - path_to_search:
%       Path to search for dated subfolders containing the intParams files.
%       Subfolders should be named in the format |yyyy-mm-dd|.
%
% Outputs:
%   - iP_paths: Paths to the located intParams files

ndays = end_datenum - start_datenum;
%% Find matching intParams files
% Search through each subfolder in the range of days (e.g. 2012-11-24
% through 2012-11-26)
iP_paths = cell(1, ceil(ndays * 24));  % preallocate cells
keepcount = 0;
for day = floor(start_datenum):ceil(end_datenum)
    [y, m, d] = datevec(day);
    dt_folder = fullfile(path_to_search, sprintf('%04d-%02d-%02d', y, m, d));
    iP_thisdir = dir(fullfile(dt_folder, '*_intParams.mat')); % directory listing of the folder to search
    iP_thisdir = arrayfun(@(p)fullfile(p.folder, p.name), iP_thisdir, 'UniformOutput', false);
    keep = false(size(iP_thisdir));
    if (day > start_datenum + 1) && (day < end_datenum - 1)
        % Trust date folder names and assume we can keep all files that are definitely
        % within the bounds
        keep(:) = true;
    else
        for j = 1:numel(iP_thisdir)
            iP_path = iP_thisdir{j};
            iP = load(iP_path, 'int_time');
            % only recall the intParams files that match the time range
            keep(j) = iP.int_time >= start_datenum && iP.int_time <= end_datenum;
        end
    end
    % filter iP_thisdir with keep
    iP_thisdir = iP_thisdir(keep);
    % insert iP_thisdir into iP_paths
    iP_paths(keepcount + 1:keepcount + numel(iP_thisdir)) = iP_thisdir;
    % Update keepcount to the new total
    keepcount = keepcount + numel(iP_thisdir);
end
iP_paths(keepcount + 1:end) = [];  % remove extra cells
