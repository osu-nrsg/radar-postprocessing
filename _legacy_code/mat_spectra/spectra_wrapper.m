function [spectra_fnames, intParams_fname, spectra, subimages_json] = ...
    spectra_wrapper(cube_path, result_dir, baps_json_options)
% Wrapper around build_and_plot_spectra that can be used by the MATLAB API for Python.
%
% Usage: [spectra_fnames, intParams_fname, spectra, subimages_json] = ...
%             spectra_wrapper(cube_path, result_dir, baps_json_options)
%
% Inputs
% ------
% - cube_path and result_dir should be self-explanatory
% - baps_json_options:
%     Where build_and_plot_spectra uses varargin to supply optional options, this function
%     takes in a json string that contains an object with the options enclosed. This allows us to
%     get around a number of the incompatibilities in the API (no struct arrays, no cell
%     arrays). The following options can be defined in the json object:
%       - plot_timezone
%       - subimages - can either be a dict of one subimage or an array of dicts
%       - wdir_is_heading
%       - save_best
%       - show_si_info
%       - no_replot
%
% Returns
% -------
% spectra_fnames
%    Paths to spectra image plots
% intParams_fname
%    Path to intParams mat file
% spectra
%    Cell array of f-integrated spectra for each subimage
% subimages_json
%    JSON-encoded data about subimages
%

narginchk(3, 3);
try
    cube = load(cube_path);
    options = jsondecode(baps_json_options);
    optargs = {};
    fns = fieldnames(options);
    for fni = 1:numel(fns)
        optargs = [optargs, fns(fni), {options.(fns{fni})}];  %#ok<AGROW>
    end

    [spectra_fnames, intParams_fname, spectra, subimages] = build_and_plot_spectra( ...
        'cube', cube, 'dir_res', result_dir, optargs{:});
    subimages_json = jsonencode(subimages);
    spectra = jsonencode(spectra);
catch err
    fprintf(2, getReport(err, 'extended', 'hyperlinks', 'off'));
    [spectra_fnames, intParams_fname, spectra, subimages_json] = deal('error');
end
