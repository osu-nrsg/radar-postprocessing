function [glattet] = smooth_2D(matrise)
% Written by Kjell Inge Gausland
% Last edited: 090399 Kjell Inge Gausland
%
% matisre - matrix to me smoothed
% glattet - smoothed matrix
%
% The function the input matrix by applying a 3 by 3 window on each
% element. Each element is replaced by the mean of the 3 by 3 window.

[m, n] = size(matrise);

glattet = [zeros(1, n+2); zeros(m, 1) matrise zeros(m, 1); zeros(1, n+2)];

% top left
shifted = [matrise zeros(m, 2); zeros(2, n+2)];
glattet = glattet+shifted;

% top centre
shifted = [zeros(m, 1) matrise zeros(m, 1); zeros(2, n+2)];
glattet = glattet+shifted;

% top right
shifted = [zeros(m, 2) matrise; zeros(2, n+2)];
glattet = glattet+shifted;

% centre left
shifted = [zeros(1, n+2); matrise zeros(m, 2); zeros(1, n+2)];
glattet = glattet+shifted;

% centre centre
% already included

% centre right
shifted = [zeros(1, n+2); zeros(m, 2) matrise; zeros(1, n+2)];
glattet = glattet+shifted;

% bottom left
shifted = [zeros(2, n+2); matrise zeros(m, 2)];
glattet = glattet+shifted;

% bottom center
shifted = [zeros(2, n+2); zeros(m, 1) matrise zeros(m, 1)];
glattet = glattet+shifted;

% bottom right
shifted = [zeros(2, n+2); zeros(m, 2) matrise];
glattet = (glattet+shifted)./9;

glattet = glattet(2:m+1, 2:n+1);
