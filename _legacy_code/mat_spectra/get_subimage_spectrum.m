function [Skkf, intSkkf, int_Sf, k_waves, f_waves, new_params] = get_subimage_spectrum(cube, subimage, varargin)
% FUNCTION GET_SUBIMAGE_SPECTRUM
%
% Usage:
%   [Skkf, intSkkf, int_Sf, k_waves, f_waves, new_params] = get_subimage_spectrum(cube, subimage, ...)
%
% Description:
%   This function calculates a wavenumber/frequency spectrum from a radar recording.
%
% Inputs:
%   - cube:
%       Cartesian-interpolated WIMR cube. Does not need to have the whole radar
%       footprint--just the portion specified by the bounds of subimage
%   - subimage:
%       struct defining the area from which the spectrum is to be calculated.
%       Fields:
%         - center_x, center_y:
%             x and y positions of the center of the subimage, in the same units
%             as grid_x and grid_y from the cube (e.g. UTC)
%         - size:
%             Number of bins for each side of the subimage. That is, if size is
%             128, the subimage will be 128x128 bins, centered on center_x and
%             center_y.
%         - h:
%             Approximate water depth in the cube region. Used to calculate wave
%             period
%         - name:
%             String for qualitative identification of the subimage location.
% Optional Inputs (given in the form of 'name', value):
%   - selected_frames:
%       Monotonically increasing vector of indices. If provided, only the
%       selected frames (rotations) from the cube are used to calculate the
%       spectrum. Otherwise, all rotations are used.
%
% Outputs:
%   - Skkf:
%       3D Wavenumber-frequency spectrum. Indices are kx, ky, f
%   - intSkkf:
%       Frequency-integrated wavenumber spectrum
%   - int_Sf:
%       Wavenumber-integrated frequency spectrum
%   - k_waves:
%       Wavenumbers corresponding to kx and ky indices in the 3D and integrated
%       spectra.
%   - f_waves:
%       Frequencies corresponding to the f index in the 3D spectrum.
%   - new_params:
%       Struct of additional output parameters calculated in this function.
%       Typically these are appended to subimage outside of this function.
%       E.g.
%         | [Skkf, intSkkf, int_Sf, k_waves, f_waves, new_params] = get_subimage_spectrum(cube, subimage)
%         | field_names = fieldnames(new_params);
%         | % add new params to subimage struct
%         | for i = 1:numel(field_names)
%         |     subimage.(field_names{i}) = new_params.(field_names{i});
%         | end
%       Fields:
%         - side:
%             Length of subimage side in phyical units (meters)
%         - x1, x2, y1, y2:
%             Coordinates of the subimage corners
%         - snr_AH82:
%             Signal-to-noise ratio of the subimage computed with the Alpers &
%             Hasselmann (1982) algorithm.
%         - snr_BSA94:
%             Signal-to-noise ratio of the subimage computed with the Bruning,
%             Schmidt and Alpers (1994) algorithm.
%         - int_angpeak:
%             Peak wave angle of the frequency-integrated spectrum (direction
%             waves are travelling **towards**, contrary to convention).
%             NOTE: This is the azimuth angle in degrees CW from North.
%         - int_kpeak:
%             Peak wavenumber of the frequency-integrated spectrum.
%         - int_lpeak:
%             Peak wavelength of the frequency-integrated spectrum.
%         - int_Tpeak:
%             Estimated peak wave period of the frequency-integrated spectrum.
%

[mfiledir, ~, ~] = fileparts(mfilename('fullpath'));
addpath(fullfile(mfiledir, 'fastpeakfind'));

%% Constants
%%% Spectral limits:
L_MIN = 30;  % [m] minimum shallow-water wavelength to consider
L_MAX = 400;  % [m] maximum shallow-water wavelength to consider
T_MIN = 5;  % [s] minimum wave period to analyze.
T_MAX = 20;  % [s] max wave period to analyze.

%% Process inputs
% defaults
selected_frames = 1:size(cube.data, 3);

%%% process varargin
i = 1;
while i <= length(varargin)
    switch varargin{i}
        case 'selected_frames'
            selected_frames = varargin{i + 1};
            i = i + 1;
        otherwise
            error('plotSpectrum:badArg', 'Bad input argument');
    end
    i = i + 1;
end

% Need to use an even number of frames for this function.
if numel(selected_frames) >= 3
    if mod(numel(selected_frames), 2) ~= 0
        selected_frames(end) = [];
    end
else
    warning('get_subimage_spectrum:too_few_frames', 'Not enough frames to get a spectrum.');
    [Skkf, intSkkf, k_waves, f_waves, new_params] = deal([]);
    return
end

num_rotations = numel(selected_frames);

%% Get calculated members of subimages struct
new_params.side = subimage.size * cube.d_xy;  % get side length values
new_params.x1 = subimage.center_x - new_params.side / 2;  % get x1 values
new_params.x2 = new_params.x1 + new_params.side;  % get x2 values
new_params.y1 = subimage.center_y - new_params.side / 2;  % get y1 values
new_params.y2 = new_params.y1 + new_params.side;  % get y2 values

%%  Select the subimage
%  ~~~~~~~~~~~~~~~~~~
%%% Convert UTM coordinates to Cube indices:
x_ind = find(cube.grid_x >= new_params.x1);  % cube indices for Subimage Eastings
y_ind = find(cube.grid_y >= new_params.y1);  % cube indices for Subimage Northings

%%  Compute the 3D fft
%  ~~~~~~~~~~~~~~~~
cubito = cube.data( ...
y_ind(1):y_ind(1) + subimage.size - 1, ...
    x_ind(1):x_ind(1) + subimage.size - 1, ...
    selected_frames);  % in UINT8 format
cubito = double(cubito);  % cube.data(1:end-1, 1:end-1, :));
Sp3D = fftn(cubito);
Sp3D = Sp3D .* conj(Sp3D);  % power spectrum [equivalent to (abs(fftn(cubito)).^2)].
Sp3D = 2 * Sp3D(:, :, 1:size(Sp3D, 3) / 2);  % one-sided [in frequency] spectrum

for i0 = 1:size(Sp3D, 3)
    Sp3D(:, :, i0) = fftshift(Sp3D(:, :, i0));
    Sp3D(:, :, i0) = rot90(Sp3D(:, :, i0), 2);
end

%%  Set up the f and k domains
%  ~~~~~~~~~~~~~~~~~~~~~~~

%%%  Frequency domain:
if ~isfield(cube.results, 'fullAfile') || cube.results.fullAfile  % assume full A file if this field is missing.
    d_t = cube.results.RunLength / num_rotations;  % [s] sampling interval.
else
    start_times = cube.time(1, :);
    seconds_per_rots = diff(start_times);
    seconds_per_rots(seconds_per_rots > 2.4) = [];
    d_t = mean(seconds_per_rots);
end
f_Ny = 1 / (2 * d_t);  % Nyquist frequency
f = linspace(0, f_Ny, 1 + (num_rotations / 2));
f = f(2:end);  % added 20130409.

%%%  Wavenumber domain:

%%% FYI: Nyquist-based domain (not used)
% k_Ny = 2 * pi / (2 * cube.d_xy);  % Nyquist wavenumber
% k = linspace(0, k_Ny, 1 + (subimage.size / 2));  % absolute (positive?) wavenumber domain
% k_full = [-k(end) + k(1:end - 1) k(1:end - 1)];  % from -k to k

%%  Filter the spectrum to typical ocean-wave dimensions
%  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

f_min = 1 / T_MAX;
f_max = 1 / T_MIN;
f_waves_i = f >= f_min & f <= f_max;  % indices of typical frequency-range of wind waves
f_waves = f(f_waves_i);  % wind-waves frequency sub-domain.

%%%  Smooth the spectra in the wavenumber domain (subrouting courtesy of S.Lehner from DLR):
n_smooth = 2;
Sp3D_sm = nan(size(Sp3D));  % pre-allocate memory space.

for i1 = 1:size(Sp3D_sm, 3)  % all frequencies
    for s = 1:n_smooth
        Sp3D_sm(:, :, i1) = smooth_2D(Sp3D(:, :, i1));
    end
end

%%%  Crop the (smoothed) wavenumber spectra to typical ocean-wave domain.
%%%  Since the spectra is has (kx,ky,f) dims, we need to use some sort of
%%%  Cartesian distance algorithm to mask out spectral data outside of the
%%%  desired wavenumber bounds. The filled_ring function does this, but
%%%  requires a bit of setup.
nx = size(Sp3D_sm, 1);  % length of the subImage [pixels];
tileSize = size(Sp3D_sm, [1,2]);
if mod((tileSize / 2), 2) == 0  % to determine the center pixel
    halfTile = ceil(tileSize / 2) + 1;
else
    halfTile = ceil (tileSize / 2);
end

dk = 2 * pi / (nx * cube.d_xy);  % delta k
kMin = floor(2 * pi / L_MAX / dk);  % number of k-bins from 0 to 2*pi/Lmax
kMax = floor(2 * pi / L_MIN / dk);  % number of k-bins from 0 to 2*pi/Lmin

cutSize = kMax * 2;  % last k-bin
wl = linspace(1, cutSize, cutSize + 1);
k_waves = ((wl - fix(cutSize / 2)) .* dk) - (dk / 2);  % vector of wavenumbers [symetric] centered in 0 (for the plot)

if kMax < halfTile
    %%%  Loop to crop all wave-number spectra (i.e., "rotations"):
    cutSpek3D = nan([2 * kMax + 1, 2 * kMax + 1, size(Sp3D_sm, 3)]); % pre-allocate memory
    for i2 = 1:size(Sp3D_sm, 3)
        Sp3D_filtered = filled_ring(kMin, kMax, squeeze(Sp3D_sm(:, :, i2)));
        cutSpek3D(:, :, i2) = Sp3D_filtered( ...
            (halfTile - kMax):(halfTile + kMax), ...
            (halfTile - kMax):(halfTile + kMax) ...
        );
    end
else
    cutSpek3D = Sp3D_sm;
end

%%%  Crop the spectrum in the frequency domain:
Skkf = cutSpek3D(:, :, f_waves_i);

%%  Estimate spectral SNR's:
signal_sp = mean(Skkf(:));

%%% version 1, after Alpers & Hasselmann (1982):
i_clutter = Sp3D(:) <= 0.02 * max(Sp3D(:));
clutter_AH82 = mean(Sp3D(i_clutter));
snr_AH82 = signal_sp / clutter_AH82;

%%%  version 2, after Bruning, Schmidt and Alpers (1994): media + 2 std of a domain away from waves
clutter_BSA94 = mean(mean(Sp3D(1:33, 1:33))) + (2 * std(std(Sp3D(1:33, 1:33))));
snr_BSA94 = signal_sp / clutter_BSA94;

%% Get integrated spectra
%%% Get frequency-integrated spectrum
intSkkf = trapz(f_waves, Skkf, 3); % integration of the 3D spectrum over frequency

%%% Also get peak f from spectrum
int_Sf = squeeze(trapz(k_waves, trapz(k_waves, Skkf, 1), 2));
[~, f_peak_i] = max(int_Sf);
int_fpeak = f_waves(f_peak_i);


%% Get 2d peaks
kpeaks = get_kpeaks(intSkkf, k_waves);
% TODO: Get fpeaks

%% Save kpeaks in new_params, use highest peak for int_* values
new_params.kpeaks = kpeaks;
new_params.int_peak = kpeaks(1).peak;
new_params.int_angpeak = kpeaks(1).ang;
new_params.int_kpeak = kpeaks(1).k;
new_params.int_lpeak = kpeaks(1).l;
new_params.int_Tpeak = 1 / int_fpeak;

% %% Get polar spectrum
% [Skphif, k_pol, phi] = get_polar_spectrum(Skkf, k_waves);
% %%% get integrated direction,frequency spectrum
% intSphif = squeeze(trapz(k_pol, Skphif, 1));
% maxima = find_polar_maxima2(intSphif);

%% Save clutter and SNR values
new_params.clutter_AH82 = clutter_AH82;
new_params.snr_AH82 = snr_AH82;
new_params.clutter_BSA94 = clutter_BSA94;
new_params.snr_BSA94 = snr_BSA94;

end % % END FUNCTION get_subimages_spectra


function kpeaks = get_kpeaks(intSkkf, k_waves)
% Get the wavenumber "coordinates" and peak values of all the peaks in the
% spectrum

kpeaks = struct('xi', {}, 'yi', {}, 'kx', {}, 'ky', {}, 'peak', {});
% Get something of a noise floor for normalization
min_intSkkf = min(intSkkf(intSkkf > 0), [], 'all');
% Normalize and convert to u16 for FastPeakFind
intSkkf_norm = (intSkkf - min_intSkkf) ./ (max(intSkkf, [], 'all') - min_intSkkf);
intSkkf_u16norm = uint16(65535 * intSkkf_norm);
% Find the 2D peaks
% Already smoothed so we don't want to filter (3rd arg) as this gives
% incorrect results.
% FUTURE: Maybe use imregionalmax if ok with Image Processing toolbox dep.
cent = FastPeakFind(intSkkf_u16norm, 'default', 'none');
% convert to cell arrays of (xi, yi) coordinates
cent_pairs = num2cell(reshape(cent, 2, numel(cent) / 2)', 2);
% If there's no peak from FastPeakFind, just get the 2D max so there's at
% least one peak in kpeaks.
if isempty(cent_pairs)
    [~, xxi] = max(intSkkf);
    [~, xi] = max(max(intSkkf(:, :)));
    yi = xxi(xi);
    cent_pairs{1} = [xi, yi];
end
% Get the peak values for each coordinate pair
for i = 1:numel(cent_pairs)
    xi = cent_pairs{i}(1);
    yi = cent_pairs{i}(2);
    kpeaks(i).xi = xi;
    kpeaks(i).yi = yi;
    kpeaks(i).kx = k_waves(xi);
    kpeaks(i).ky = k_waves(yi);
    kpeaks(i).peak = intSkkf(yi, xi);
    kpeaks(i).k = hypot(kpeaks(i).kx, kpeaks(i).ky);
    kpeaks(i).l = 2 * pi / kpeaks(i).k;
    % Use atan2(x, y) instead of atan2(y, x) to get azimuth angle instead of polar angle
    kpeaks(i).ang = mod(atan2(kpeaks(i).kx, kpeaks(i).ky) * 180 / pi, 360);
end
[~, order] = sort([kpeaks(:).peak], 'descend');
kpeaks = kpeaks(order);

end  % END FUNCTION find_kpeaks

%
% function kpeaks = get_fpeaks(Skkf, kpeaks, f_waves)
% % Find the max frequency for each kpeak and save in the kpeaks struct
%
% for kpi = 1:numel(kpeaks)
%     f_kpeak = squeeze(Skkf(kpeaks(kpi).yi, kpeaks(kpi).xi, :));
%     [~, fpeak_i] = max(f_kpeak);
%     fpeak = f_waves(fpeak_i);
%     kpeaks(kpi).fpeak_i = fpeak_i;
%     kpeaks(kpi).fpeak = fpeak;
% end
%
% end  % END FUNCTION find_fpeaks
