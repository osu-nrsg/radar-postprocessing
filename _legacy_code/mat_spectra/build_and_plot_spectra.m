function [spectra_fnames, intParams_fname, intSkkfs, subimages] = ...
    build_and_plot_spectra(varargin)
% FUNCTION build_and_plot_spectra Purpose:
%   This function creates spectrum plots from the specified cube. Constant
%   parameters within this function set the subimages to use to get the spectrum
%   plots. The spectra from all subplots are saved as png files, and also if one
%   of them is good enough (high enough SNR in the data) then it is saved as the
%   "best" spectrum.
%
% Usage:
%   build_and_plot_spectra('argname',arg, ...)
%
% Inputs:
%   - dir_cubes:
%       String containing the path to the location of the cartesian-interpolated
%       cube MAT file. Defaults to C:\1MarineWinXP\to_cube\cubes.
%   - cubeName:
%       Name of the cube MAT file to read in. If not provided, a prompt will ask
%       for the cube name (unless cube, below, is provided).
%   - cube:
%       Alteratively, the cube struct may be provided directly instead of having
%       the cube file be read in. This is a little faster if the cube is already
%       in memory.
%   - dir_res:
%       Directory in which to save spectrum plots and intParams. Defaults to
%       dir_cubes (C:\1MarineWinXP\to_cube\cubes if dir_cubes is not provided).
%   - plot_timezone:
%       Timezone ID to use for the plot timestamps. Note that if the timezone is
%       of the form 'US/Pacific', then the 'US/' will be stripped off for the
%       plot title. For more info see the help for
%       timezone_convert. Defaults to 'UTC'.
%   - subimages:
%       A struct or array of structs with the bounding information for the
%       region(s) to examine. Required fields include:
%         - center_x, center_y:
%             Center x and y coordinates, UTM meters (Eastings, Northings)
%         - name: Name of subimage
%         - size: Side length of the subimage (subimage will be square)
%         - h: Approximate water depth for the region (from MSL)
%       Defaults to three regions west of Nye Beach, South Beach, and the
%       Yaquina inlet near Newport.
%   - wdir_is_heading:
%       Defines whether the wave angle is the direction the waves are
%       heading (true), or the direction from which the waves are
%       coming (false, default).
%   - save_best:
%       Determines whether the function will attempt to choose a "best"
%       spectrum and save it with the name 'best'. Default is false.
%   - si_name_in_title:
%       Determines whether each plot title will include the name of the
%       subimage and its center coordinates. Default is false.
%   - no_replot
%       If this is true, a plot will not be recreated if it already exists.
%       Defaults to false.
%
% Outputs:
%   - spectra_fnames:
%       Filenames of the spectra plots
%   - intParams_fname:
%       Filename of the intParams file
%   - Skkfs:
%       (Cell array) - 3D spectra of the subimages
%
% Customization:
%   This function currently has the positions of the subplots hard-coded, under
%   the "User-adjustable parameters" section. (Eventually want to pull this out
%   to input arguments).
%
% Notes:
%   1. Even if plot_timezone is specified, the time stamp saved in the
%      intParams file remains the same as the cube file (should be UTC, assuming
%      the clock on the DAQ PC was correctly synced to UTC, even if the PC's
%      timezone was set to something else). Only the timestamps on the plots are
%      changed.

% History:
%   Guillermo Diaz-Mendez 11/2012 OSU (Updated: Nov/05/2012) Randall Pittman
%   2012-11-13:
%       Converted to script to play nicely with other Newport scripts.

% Constants
PNG_W = 950;
PNG_H = 900;
DPI = 120;
DEFAULT_MAX_L = 300;

% FUTURE: Add SNR check to determination of "validity".
% FUTURE: Add peak check of kpeaks to determination of validity

%% Timing
tic;

%% Inputs
p = inputParser;
p.KeepUnmatched = true;  % don't error on extraneous fields
p.addParameter('dir_cubes', '.', @(d) exist(d, 'dir'));
p.addParameter('cubeName', '');
p.addParameter('cube', [], @isstruct);
p.addParameter('dir_res', '');
p.addParameter('plot_timezone', 'UTC');
p.addParameter( ...
    'subimages', ...
    struct('center_x', {}, 'center_y', {}, 'name', {}, 'size', {}, 'h', {}), ...
    @isstruct ...
);
p.addParameter('save_best', false);
p.addParameter('si_name_in_title', false);
p.addParameter('wdir_is_heading', false);
p.addParameter('no_replot', false);
p.parse(varargin{:});
dir_cubes = p.Results.dir_cubes;
cubeName = p.Results.cubeName;
cube = p.Results.cube;
dir_res = p.Results.dir_res;
plot_timezone = p.Results.plot_timezone;
subimages = p.Results.subimages;
save_best = p.Results.save_best;
si_name_in_title = p.Results.si_name_in_title;
wdir_is_heading = p.Results.wdir_is_heading;
no_replot = p.Results.no_replot;

if isempty(cubeName) && isempty(cube)
    error('build_and_plot_spectra:cube_missing', 'No cube or cubeName was provided');
end

%  Load cube if not already provided:
if isempty(cube)
    cube_path = fullfile(dir_cubes, cubeName);
    try
        cube = load(cube_path);
    catch err
        if strcmp(err.identifier, 'MATLAB:load:couldNotReadFile')
            error( ...
                'build_and_plot_spectra:invalid_cube_path', ...
                'There is no cube file at %s', cube_path);
        else
            rethrow(err);
        end
    end
end

% set dir_res if it is unset, then make sure the directory exists.
if isempty(dir_res)
    dir_res = dir_cubes;
end
if ~exist(dir_res, 'dir')
    mkdir(dir_res);
end

% maxL - needed to determine if a calculated spectrum is any good
if isfield(cube, 'maxL')
    maxL = cube.maxL;
else
    maxL = DEFAULT_MAX_L;
end

% longName
if isfield(cube, 'longName')
    longName = cube.longName;
else
    longName = cube.station;
end

if isempty(subimages)
    subimages(1).center_x = 413632;
    subimages(1).center_y = 4942403;
    subimages(1).name = 'Nye Beach';
    subimages(1).size = 128;
    subimages(1).h = 18.6;

    subimages(2).center_x = 413713;
    subimages(2).center_y = 4938053;
    subimages(2).name = 'South Beach';
    subimages(2).size = 128;
    subimages(2).h = 13.8;

    subimages(3).center_x = 413251;
    subimages(3).center_y = 4939929;
    subimages(3).name = 'Yaquina Inlet';
    subimages(3).size = 128;
    subimages(3).h = 18.0;

end

% Get file number
% filenumber = cube.header.file(2:8);  % old way - use the M file number
filenumber = datestr(cube.results.start_time.dateNum, 'yyyymmdd_HHMMSS');  %#ok<*DATST>

%% Get time range of recording
rec_time = [epoch2Matlab(cube.time(1, 1)) ...
            epoch2Matlab(cube.time(1, end))];

%%  LOOP:
intSkkfs = cell(1, numel(subimages));
int_Sfs = cell(1, numel(subimages));
spectra_fnames = repmat({0}, 1, numel(subimages));
for ss = 1:length(subimages)
    % Save the UTMZone to the subimage
    subimages(ss).UTMZone = cube.results.UTMZone;
    % Save no-spaces name
    subimages(ss).nsname = subimages(ss).name(subimages(ss).name ~= ' ');
    %% Get the spectrum for the subimage
    [~, intSkkfs{ss}, int_Sfs{ss}, kplot, ~, new_params] = get_subimage_spectrum(cube, subimages(ss));
    % add new params to subimages struct
    field_names = fieldnames(new_params);
    for i = 1:numel(field_names)
        subimages(ss).(field_names{i}) = new_params.(field_names{i});
    end

    % Decide if the spectra is worth plotting
    if (isempty(intSkkfs{ss}) || new_params.int_lpeak > maxL || ...
            ~valid_peak_angle(cube, new_params.int_angpeak))
        subimages(ss).valid = false;
        subimages(ss).pngName = '';
        continue
    else
        subimages(ss).valid = true;
    end

    %% Create plot (if we want to)
    % Create the name of the PNG
    subimages(ss).pngName = sprintf('%s_%s_Skxky_%s.png', cube.station, filenumber, subimages(ss).nsname);
    if no_replot && exist(fullfile(dir_res, subimages(ss).pngName), 'file')
        % The plot exists and we're not replotting. Move on to the next.
        logmsg('%s has already been plotted. Next.\n', subimages(ss).pngName);
        continue
    end

    %%% Create plot
    set(gcf, 'Units', 'pixels', 'Position', [0, 0, PNG_W, PNG_H]);
    handles = plot_spectrum( ...
        intSkkfs{ss}, subimages(ss), kplot, rec_time, longName, ...
        'plot_timezone', plot_timezone ...
        , 'wdir_is_heading', wdir_is_heading ...
        , 'si_name_in_title', si_name_in_title ...
        , 'card_dir', true ...
    );

    %%% Prepare text for printing to PNG
    text_handles = struct2cell(handles.txt);
    text_handles = [text_handles{:} handles.title];
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperPosition', [0, 0, PNG_W / DPI, PNG_H / DPI]);  % 10" x 7.5"
    set(text_handles, 'FontUnits', 'inches', 'FontSize', 20 / DPI);  % 12 pt

    %%% save spectra png
    spectra_png_path = fullfile(dir_res, subimages(ss).pngName);
    print(gcf, '-dpng', spectra_png_path, sprintf('-r%d', DPI));
    % exportgraphics(gcf, spectra_png_path, 'Resolution', DPI);
    logmsg('%s plotted.\n', subimages(ss).pngName);
    % Delete annotation, since it's not removed when the axes are cleared
    delete(handles.txt.stats);
    spectra_fnames{ss} = subimages(ss).pngName;
end
close(gcf); % close figures.

%%  Save variables:
%  The "best" S(kx, ky) spectrum
%  ~~~~~~~~~~~~~~~~~~~~~~~~~
best_spec_fname = sprintf('%s_%s_Skxky_best.png', cube.station, filenumber);
best_subimage = [];
if save_best
    [~, best_snr_i] = max([subimages.snr_BSA94]);
    if subimages(best_snr_i).valid
        best_subimage = best_snr_i;
        copyfile(fullfile(dir_res, subimages(best_snr_i).pngName), fullfile(dir_res, best_spec_fname));
    end
    spectra_fnames = [spectra_fnames, best_spec_fname];
end

% Save a few general parameters about the site
if isfield(cube, 'location')
    site.location = cube.location;
end
site.longName = cube.longName;
site.station = cube.station;
site.x1 = cube.results.XOrigin - cube.Rg(end);
site.x2 = cube.results.XOrigin + cube.Rg(end);
site.y1 = cube.results.YOrigin - cube.Rg(end);
site.y2 = cube.results.YOrigin + cube.Rg(end);
if isfield(cube.results, 'UTMZone')
    site.UTMZone = cube.results.UTMZone;
end
if isfield(cube.results, 'RunLength')
    runLength = cube.results.RunLength;
else
    runLength = [];
end

%  The corresponding radar-derived integral parameters
%  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int_time = epoch2Matlab(cube.time(1, 1));
if ~isfield(cube, 'grid_name') || isempty(cube.grid_name)
    cube.grid_name = '';
end
intParams_fname = sprintf('%s_%s_%sintParams.mat', cube.station, filenumber, [cube.grid_name '_']);
intParams_fullpath = fullfile(dir_res, intParams_fname);
save(intParams_fullpath, 'subimages', 'int_time', 'best_subimage', 'maxL', 'site', 'runLength', '-v7.3');

%% Add metadata as JSON to each png
for si = 1:numel(subimages)
    if subimages(si).valid
        subimage = subimages(si);
        png_path = fullfile(dir_res, subimages(ss).pngName);
        metadata = struct('site', site, 'subimage', subimage);
        set_png_metadata(png_path, metadata);
    end
end

if ~isempty(best_subimage)
    png_path = fullfile(dir_res, best_spec_fname);
    metadata = struct('site', site, 'subimage', subimages(best_subimage));
    set_png_metadata(png_path, metadata);
end

%%  Elapsed time:
t_Cubes = toc;
logmsg('The creation and plotting of spectra took %.f seconds to complete.\n\n', t_Cubes);
end  % END FUNCTION build_and_plot_spectra


function in_range = valid_peak_angle(cube, wave_angle)
% Determine if a peak angle is valid based on the site_wave_dir_range
% parameter
wave_src_dir = mod(wave_angle - 180, 360);
if ~isfield(cube, 'site_wave_dir_range')
    in_range = true;
    return
end
angle_bounds = mod(cube.site_wave_dir_range, 360);
if angle_bounds(2) < angle_bounds(1)
    angle_bounds(2) = angle_bounds(2) + 360;
end
in_range = (wave_src_dir >= angle_bounds(1) && wave_src_dir <= angle_bounds(2));
end  % END FUNCTION check_wave_dir


function set_png_metadata(png_path, metadata)
% Add metadata to a PNG file
if isfile(png_path)
    img = imread(png_path);
    imwrite(img, png_path, 'Description', jsonencode(metadata));
end
end  % END FUNCTION set_png_metadata

function logmsg(varargin)
fprintf(2, varargin{:});
end
