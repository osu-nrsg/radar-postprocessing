function [Skphif, k_pol, phi] = get_polar_spectrum(Skkf, k_waves)

% Get minimum dphi by finding the distance between the furthest points in
% the domain
x0 = k_waves(end-1);
y0 = k_waves(end);
[phi0, ~] = cart2pol(x0, y0);
x1 = k_waves(end);
y1 = k_waves(end-1);
[phi1, ~] = cart2pol(x1, y1);
dphi = phi0 - phi1;

% Create grid and transform to Cartesian
k_pol = k_waves(k_waves > 0);
phi = 0:dphi:2*pi;
% don't replicate 2*pi if dphi happens to be an even factor of 2*pi (unlikely)
phi(phi == 2*pi) = [];
[phi_gd, k_gd] = meshgrid(phi, k_pol);
[x_gd, y_gd] = pol2cart(phi_gd, k_gd);

nk = numel(k_pol);
nf = size(Skkf, 3);
nphi = numel(phi);
Skphif = zeros(nk, nphi, nf);

for fi = 1:nf
    Skphif(:, :, fi) = interp2(k_waves, k_waves, Skkf(:, :, fi), x_gd, y_gd);
end
