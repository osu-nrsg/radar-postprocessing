function [stats, stats_tbl] = accumulate_spectral_stats(iP_files)
% ACCUMULATE_SPECTRAL_STATS
%
% Purpose:
%   Get spectral peak values from a list of intParams files. Use the best
%   subimage in each file, if there is one, depending on SNR.
%
% Inputs:
%   iP_files
%     cell array of intParams file paths or struct from call to `dir()`.
%
% Outputs:
%   stats
%     struct with the best stats for each intParams file
%     Fields:
%     - best_roi - The name of the roi (e.g. Nye Beach)
%     - time - vector of int_time
%     - Tpeak - vector of int_Tpeak for the best subimage in each file
%     - lpeak - vector of int_lpeak for the best subimage in each file
%     - angpeak - vector of int_angpeak for the best subimage in each file
%     - snr_AH82 - vector of snr_AH82 for the best subimage in each file
%     - snr_BSA94 - vector of snr_BSA94 for the best subimage in each file

nans = nan(1, numel(iP_files));
bests = repmat({''}, size(iP_files));
best_rois = bests;
times = nans;
Tpeak = nans;
lpeak = nans;
angpeak = nans;
snr_AH82 = nans;
snr_BSA94 = nans;

% fprintf('%20s\t%13s\t%5s\t%6s\t%6s\n', 'DateTime', 'Site', 'valid', 'AH82', 'BSA94');
for file_i = 1:numel(iP_files)
    %%% Get fpath
    if isstruct(iP_files)
        fpath = fullfile(iP_files(file_i).folder, iP_files(file_i).name);
    elseif iscell(iP_files)
        fpath = iP_files{file_i};
    else
        error( ...
            'get_intParams:invalid_iP_files', ...
            'iP_files must be a struct from the dir command or a cell array of paths');
    end

    %%%
    intParams = load(fpath);
    valid_roi_mask = [intParams.subimages.valid];
    if ~any(valid_roi_mask)
        continue
    end
    times(file_i) = intParams.int_time;
    valid_rois = intParams.subimages(valid_roi_mask);
    [~, best_roi_i] = max([valid_rois.snr_BSA94]);
    best_roi = valid_rois(best_roi_i);
    best_rois{file_i} = best_roi.name;
    Tpeak(file_i) = best_roi.int_Tpeak;
    lpeak(file_i) = best_roi.int_lpeak;
    angpeak(file_i) = best_roi.int_angpeak;
    snr_AH82(file_i) = best_roi.snr_AH82;
    snr_BSA94(file_i) = best_roi.snr_BSA94;
end

skipped = isnan(times);
% filter data
times(skipped) = [];
best_rois(skipped) = [];
Tpeak(skipped) = [];
lpeak(skipped) = [];
angpeak(skipped) = [];
snr_AH82(skipped) = [];
snr_BSA94(skipped) = [];
dts = datetime(times, 'ConvertFrom', 'datenum', 'TimeZone', 'UTC');
stats_tbl = timetable( ...
    dts(:), best_rois(:), Tpeak(:), lpeak(:), angpeak(:), snr_AH82(:), snr_BSA94(:), ...
    'VariableNames', {'best_roi', 'Tpeak', 'lpeak', 'angpeak', 'snr_AH82', 'snr_BSA94'} ...
);
stats.time = times;
stats.best_roi = best_rois;
stats.Tpeak = Tpeak;
stats.lpeak = lpeak;
stats.angpeak = angpeak;
stats.snr_AH82 = snr_AH82;
stats.snr_BSA94 = snr_BSA94;
end  % END FUNCTION accumulate_spectral_stats
