function maxima = find_polar_maxima2(data)
% FIND_POLAR_MAXIMA2 - Find local maxima in a 2d array that wraps along its
% first dimension

[nrow, ncol] = size(data);
maxima = false(size(data));

for ci = 1:ncol
    for ri = 2:(nrow - 1)  % don't include top and bottom edges
        % get coordinate positions
        current = data(ri, ci);
        left = ci - 1;
        right = ci + 1;
        up = ri + 1;
        down = ri - 1;
        % This is how we can wrap on the first axis
        if left < 1
            left = ncol;
        end
        if right > ncol
            right = 1;
        end
        maxima(ri, ci) = ( ...
            current > data(up, left) && ...
            current > data(up, ci) && ...
            current > data(up, right) && ...
            current > data(ci, left) && ...
            current > data(ci, right) && ...
            current > data(down, left) && ...
            current > data(down, ci) && ...
            current > data(down, right) ...
        );
    end
end
   
