klim = [-2*pi/75. 2*pi/75];
c_lim = [min(Skkf, [], 'all'), max(Skkf, [], 'all')];
%% Plot all freqencies
parfor fi = 1:numel(f_waves)
    fig = figure;
    ax = axes(fig);
    h_kxky = surface(ax, k_waves, k_waves, rot90(Skkf(:, :, fi),2));
    shading flat
    caxis(ax, c_lim);
    title(sprintf('f = %.3f fi = %.f T = %.2f s', f_waves(fi), fi, 1/f_waves(fi)));
    xlim(ax, klim);
    ylim(ax ,klim);
    xlabel(ax, 'kx');
    ylabel(ax, 'ky');
    colormap(ax, turbo);
    colorbar(ax);
    print(fig, '-dpng', sprintf('fplots/%.f.png', fi));
    logmsg('%.f of %.f\n', fi, numel(f_waves));
    close(fig);
end

%% Plot all kx
clf;
xlim([-kmax, kmax]);
ylim([min(f_waves), max(f_waves)]);
xlabel('ky');
ylabel('f');
for ki = 1:numel(k_waves)
    h_f = surface(k_waves, f_waves, squeeze(Skkf(:, ki, :))'); shading flat
    zlim(c_lim);
    caxis(c_lim);
    title(sprintf('kx = %.3f rad/s', k_waves(ki)));
    print(gcf, '-dpng', sprintf('kxplots/%.f.png', ki));
    logmsg('%.f of %.f\n', ki, numel(k_waves));
    delete(h_f);
end

%% Plot all ky
clf;
xlim([-kmax, kmax]);
ylim([min(f_waves), max(f_waves)]);
xlabel('kx');
ylabel('f');
for i = 1:numel(k_waves)
    h_f = surface(k_waves, f_waves, squeeze(Skkf(i, :, :))'); shading flat
    zlim(c_lim);
    caxis(c_lim);
    title(sprintf('ky = %.3f rad/s', k_waves(i)));
    print(gcf, '-dpng', sprintf('kyplots/%.f.png', i));
    logmsg('%.f of %.f\n', i, numel(k_waves));
    delete(h_f);
end


%% Search through all frequencies and find peaks
fpeaks = struct('fi', num2cell(1:numel(f_waves)), 'f', num2cell(f_waves), 'kxi', [], 'kyi', [], 'kx', [], 'ky', [], 'peak', []);
for fi = 1:numel(f_waves)
    Skk_fi = Skkf(:, :, fi);
    [~, xxi] = max(Skk_fi);
    [~, xi] = max(max(Skk_fi(:, :)));
    yi = xxi(xi);
    fpeaks(fi).kxi = xi;
    fpeaks(fi).kyi = yi;
    fpeaks(fi).kx = k_waves(xi);
    fpeaks(fi).ky = k_waves(yi);
    fpeaks(fi).peak = Skk_fi(yi, xi);
end
% sort by peak value, descending
[~, order] = sort([fpeaks.peak], 'descend');
fpeaks = fpeaks(order);

%% Plot Skphif for all wavenumbers. Also can plot intSkphif
nk = numel(k_pol);
nf = numel(f_waves);
[phi_plt, f_plt] = meshgrid(phi, f_waves);
[x_plt, y_plt] = pol2cart(phi_plt, f_plt);
c_lim = [min(Skkf, [], 'all'), max(Skkf, [], 'all')];
flim = [-max(f_waves), max(f_waves)];
parfor ki = 1:nk
    fig = figure;
    ax = axes(fig);
    Sphif = squeeze(Skphif(ki, :, :));
    h_surf = surface(ax, -x_plt, -y_plt, Sphif');
    shading(ax, 'flat');
    axis(ax, 'image');
    caxis(c_lim);
    zlim(c_lim);
    xlim(ax, flim);
    ylim(ax ,flim);
    xlabel(ax, 'fx');
    ylabel(ax, 'fy');
    colormap(ax, turbo);
    colorbar(ax);
    title(sprintf('ki = %.f k = %.3f L = %6.1f', ki, k_pol(ki), 2*pi/k_pol(ki)));
    print(fig, '-dpng', sprintf('kplots_pol/%.f.png', ki));
    logmsg('%.f of %.f\n', ki, nk);
    close(fig);
end

function logmsg(varargin)
fprintf(2, varargin{:});
end
