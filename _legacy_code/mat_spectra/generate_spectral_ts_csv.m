function generate_spectral_ts_csv(end_datenum, days_to_include, spectra_dir, csv_path, wdir_is_heading)

    % get starting time by subtracting the # of days to plot from the end_datenum
    start_datenum = end_datenum - days_to_include;

    iP_paths = select_iP_paths(spectra_dir, start_datenum, end_datenum);
    stats = accumulate_spectral_stats(iP_paths);

    if wdir_is_heading
        angpeaks = stats.angpeak;
        angpeak_lbl = 'Peak Direction (deg to)';
    else
        angpeaks = mod(stats.angpeak + 180, 360);
        angpeak_lbl = 'Peak Direction (deg from)';
    end

    fid = fopen(csv_path, 'w');
    fprintf(fid, 'Datetime (UTC), Peak Period (s), Peak Wavelength (m), %s\n', angpeak_lbl);
    for i = 1:numel(stats.time)
        fprintf( ...
            fid, ...
            '%s, %5.2f, %5.1f, %5.1f\n', ...
            datestr(stats.time(i), 'yyyy-mm-ddTHH:MM:SSZ'), stats.Tpeak(i), stats.lpeak(i), angpeaks(i) ...
        );
    end
    fclose(fid);
