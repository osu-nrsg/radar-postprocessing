import io
import logging
from pathlib import Path

from pydantic import FilePath, validate_call

from wimr_postproc.mat_eng import MatlabExecutionError, get_mat_eng
from wimr_postproc.params import CoGeoRectParams, CoRectParams

logger = logging.getLogger(__name__)


@validate_call
def corectify_cube(
    cube_path: FilePath,
    corect_params: CoRectParams | None = None,
    cogeorect_params: CoGeoRectParams | None = None,
) -> None:
    cr_params_json = corect_params.json() if corect_params else "null"
    cgr_params_json = cogeorect_params.json() if cogeorect_params else "null"
    mat_eng = get_mat_eng()
    mat_eng.cd(str(Path(__file__).parent / "mat_rectify"))
    stdout_ = io.StringIO()
    stderr_ = io.StringIO()
    try:
        mat_eng.corectify_wrapper(
            str(cube_path.absolute()),
            cr_params_json,
            cgr_params_json,
            stdout=stdout_,
            stderr=stderr_,
            nargout=0,
        )
    except MatlabExecutionError:
        mat_txt = ""
        if stdout_.tell() > 0:
            stdout_.seek(0, 0)
            mat_txt += f"MATLAB stdout:\n{stdout_.read()}\n"
        if stderr_.tell() > 0:
            stderr_.seek(0, 0)
            mat_txt += f"MATLAB stderr:\n{stderr_.read()}"
        logger.exception("Error calling polMat2TimexKmz.m:\n%s", mat_txt)
    finally:
        stdout_.close()
        stderr_.close()
        mat_eng.exit()
