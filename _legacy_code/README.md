# `_legacy_code` contents

This directory contains legacy code that I want to keep handy for future use. It is not
included in the built version of this library.
