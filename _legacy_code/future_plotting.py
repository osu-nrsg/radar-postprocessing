from __future__ import annotations

import logging
from dataclasses import dataclass, field
from functools import lru_cache
from typing import Literal, Type

import h5py
import hdf5storage.utilities as h5s_util
import numpy as np
import pymap3d
import utm
from nc4_tools.ncdataset import NCDataset
from nc4_tools.ncvarslice import NCVarSlicer
from scipy.interpolate import RegularGridInterpolator, interp1d, interp2d

from wimr_postproc.util import img_norm
from wimr_postproc.util.conversions import cart2pol
from wimr_postproc.util.other import uint_divisor

logger = logging.getLogger(__name__)


@dataclass
class PlotBounds:
    """Plot boundaries container/generator"""

    lat0: float
    lon0: float
    r_max: float
    north: float | None = None
    south: float | None = None
    east: float | None = None
    west: float | None = None
    d_xy: float = 10.0
    lat_bounds: tuple[float, float] = field(init=False)
    lon_bounds: tuple[float, float] = field(init=False)
    x_bounds: tuple[float, float] = field(init=False)
    y_bounds: tuple[float, float] = field(init=False)
    nx: int = field(init=False)
    ny: int = field(init=False)
    x_dom: np.ndarray = field(init=False)
    y_dom: np.ndarray = field(init=False)
    lat_dom: np.ndarray = field(init=False)
    lon_dom: np.ndarray = field(init=False)

    def __hash__(self) -> int:
        return hash(
            (
                self.lat0,
                self.lon0,
                self.r_max,
                self.north,
                self.south,
                self.east,
                self.west,
                self.d_xy,
            )
        )

    def __post_init__(self) -> None:
        if self.north is None:
            self.north = pymap3d.enu2geodetic(
                0, self.r_max, 0, self.lat0, self.lon0, 0, deg=True
            )[0]
        if self.south is None:
            self.south = pymap3d.enu2geodetic(
                0, -self.r_max, 0, self.lat0, self.lon0, 0, deg=True
            )[0]
        if self.east is None:
            self.east = pymap3d.enu2geodetic(
                self.r_max, 0, 0, self.lat0, self.lon0, 0, deg=True
            )[1]
        if self.west is None:
            self.west = pymap3d.enu2geodetic(
                -self.r_max, 0, 0, self.lat0, self.lon0, 0, deg=True
            )[1]
        self.lat_bounds = (self.south, self.north)
        self.lon_bounds = (self.west, self.east)
        self.y_bounds = tuple(
            pymap3d.geodetic2enu(
                self.lat_bounds, self.lon0, 0, self.lat0, self.lon0, 0, deg=True
            )[1]
        )
        self.x_bounds = tuple(
            pymap3d.geodetic2enu(
                self.lat0, self.lon_bounds, 0, self.lat0, self.lon0, 0, deg=True
            )[0]
        )
        self.nx = int((self.x_bounds[1] - self.x_bounds[0]) / self.d_xy)
        self.ny = int((self.y_bounds[1] - self.y_bounds[0]) / self.d_xy)
        self.x_dom = np.linspace(*self.x_bounds, self.nx)
        self.y_dom = np.linspace(*self.y_bounds, self.ny)
        self.lon_dom = np.linspace(*self.lon_bounds, self.nx)
        self.lat_dom = np.linspace(*self.lat_bounds, self.ny)

    @classmethod
    def from_enu(
        cls: Type[PlotBounds],
        lat0: float,
        lon0: float,
        r_max: float,
        north_m: float | None = None,
        south_m: float | None = None,
        east_m: float | None = None,
        west_m: float | None = None,
        d_xy: float = 10.0,
    ) -> PlotBounds:
        if north_m is not None:
            north, _, _ = pymap3d.enu2geodetic(0, north_m, 0, lat0, lon0, 0, deg=True)
        else:
            north = None
        if south_m is not None:
            south, _, _ = pymap3d.enu2geodetic(0, south_m, 0, lat0, lon0, 0, deg=True)
        else:
            south = None
        if east_m is not None:
            _, east, _ = pymap3d.enu2geodetic(east_m, 0, 0, lat0, lon0, 0, deg=True)
        else:
            east = None
        if west_m is not None:
            _, west, _ = pymap3d.enu2geodetic(west_m, 0, 0, lat0, lon0, 0, deg=True)
        else:
            west = None
        return cls(lat0, lon0, r_max, north, south, east, west, d_xy)


@lru_cache
def ar_grid_from_plot_bounds(plot_bounds: PlotBounds, h0: float, azi0_bound: float):
    lon_grid, lat_grid = np.meshgrid(plot_bounds.lon_dom, plot_bounds.lat_dom)
    ll_grid_x, ll_grid_y = pymap3d.geodetic2enu(
        lat_grid, lon_grid, 0, plot_bounds.lat0, plot_bounds.lon0, 0, deg=True
    )[:2]

    ll_grid_x, ll_grid_y = pymap3d.geodetic2enu(
        lat_grid, lon_grid, 0, plot_bounds.lat0, plot_bounds.lon0, 0, deg=True
    )[:2]
    x_q, y_q = ll_grid_x, ll_grid_y
    phi_q, r_q = cart2pol(x_q, y_q)
    azi_q = 90 - phi_q * 180 / np.pi  # azi_trueN
    azi0_q = np.mod(azi_q - azi0_bound, 360.0)
    r_slant_q = np.sqrt(r_q**2 + h0**2)
    return azi0_q, r_slant_q


def interp_wimr_to_latlon(
    wimr_ds: NCDataset,
    sweep_i: int,
    norm_limits: tuple[float, float] = (0, 255),
    heading_correction: float = 0,
    plot_bounds: PlotBounds | None = None,
):
    """Interpolate a dataset frame to a lat/lon grid

    TODO: document parameters
    """
    with wimr_ds:
        if not plot_bounds:
            plot_bounds = PlotBounds(
                wimr_ds["latitude"][0], wimr_ds["longitude"][0], wimr_ds["range"][-1]
            )
        slicer = NCVarSlicer(
            wimr_ds["sweep_start_ray_index"], wimr_ds["sweep_end_ray_index"]
        )
        intensity = wimr_ds["return_intensity"][slicer[sweep_i], :]
        azi = wimr_ds["azimuth"][slicer[sweep_i]] + heading_correction
        r_slant = wimr_ds["range"][:]
        h0 = wimr_ds["altitude"][0]
        #: azi0_offset is what we subtract from True North azi to get azi0.
        azi0_offset = (
            wimr_ds.site_heading
            + wimr_ds.get_scalar_var("start_azi", default=0)
            + heading_correction
        )
        azi0 = np.mod(azi - azi0_offset, 360.0)
        azi0_q, r_slant_q = ar_grid_from_plot_bounds(plot_bounds, h0, azi0_offset)
        intensity_out, *_, intensity_oob = interp_sweep(
            intensity, None, azi0, r_slant, azi0_q, r_slant_q
        )
        intensity_out /= uint_divisor(
            wimr_ds["summing_factor"][sweep_i], wimr_ds["daq_bit_depth"][0]
        )
    img_norm.img_norm(intensity_out, *norm_limits, 255)

    return intensity_out, intensity_oob


def interp_pol_cube_to_latlon(
    ds: h5py.File,
    img_type: Literal["snap", "avg"],
    norm_limits: tuple[float, float] = (0, 255),
    heading_correction: float = 0,
    plot_bounds: PlotBounds | None = None,
    snap_idx: int = 0,
    avg_idx_slc: slice = slice(None),
):
    lat0, lon0, h0, r_max = get_pol_cube_bounds(ds)
    if not plot_bounds:
        plot_bounds = PlotBounds(lat0, lon0, r_max)
    if img_type == "snap":
        intensity = ds["data"][snap_idx, ...]
    elif img_type == "avg":
        if avg_idx_slc.stop is None:
            # Get the whole recording timex
            intensity = ds["timex"][:]
        else:
            intensity = ds["data"][avg_idx_slc, ...].mean(axis=0, dtype="float32")
    else:
        raise ValueError("img_type must be 'avg' or 'snap'")
    azi0 = ds["Azi"][:].ravel()
    start_azi = ds["results"].get("startAzi", [[0]])[0][0]
    azi0_offset = start_azi + ds["results"]["heading"][0, 0] + heading_correction
    azi0_q, r_slant_q = ar_grid_from_plot_bounds(plot_bounds, h0, azi0_offset)
    intensity_out, *_, intensity_oob = interp_sweep(
        intensity, None, azi0, ds["range_pol"][:].ravel(), azi0_q, r_slant_q
    )
    img_norm.img_norm(intensity_out, *norm_limits, 255)
    return intensity_out, intensity_oob


def get_pol_cube_bounds(ds: h5py.File):
    """Get the origin latitude, longitude, and altitude, as well as the maximum slant
    range, from a polar cube"""
    east0 = ds["results"]["XOrigin"][0, 0]
    north0 = ds["results"]["YOrigin"][0, 0]
    h0 = ds["results"]["ZOrigin"][0, 0]
    zone = h5s_util.convert_to_str(ds["results"]["UTMZone"][:, 0])
    lat0, lon0 = utm.to_latlon(east0, north0, int(zone[:-1]), zone[-1])
    r_max = ds["range_pol"][()].ravel()[-1]
    return lat0, lon0, h0, r_max


def interp_sweep(
    intensity_in: np.ndarray,
    time_in: np.ndarray | None,
    azi_in: np.ndarray,
    rg_in: np.ndarray,
    azi_q: np.ndarray,
    rg_q: np.ndarray,
) -> tuple[np.ndarray, np.ndarray | None, np.ndarray | None, np.ndarray | None]:
    """Perform gridded or scattered interpolation of sweep intensity and time data.

    Parameters
    ----------
    intensity_in
        Sweep intensity data, with dimensions (n_rays, n_samples)
    time_in
        Sweep ray time vector, with dimensions (n_rays,)
    azi_in
        Sweep ray azimuth vector, with dimensions (n_rays,)
    rg_in
        Ray range vector, with dimensions (nsamples,)
    azi_q
        Azimuth query points, either vector for gridded interpolation or matrix for
        scattered
    rg_q
        Range query points, either vector for gridded interpolation or matrix for
        scattered
    Returns
    -------
    intensity_out
        Interpolated intensity as a uint8 matrix
    time_out
        If not scattered, the interpolated time vector
    azi_oob
        If not scattered, the query azimuths outside the bounds of the sweep's azimuths
    aziq_rq_oob
        logical array (mask) same shape as intensity with True for out-of bounds azi_q or
        r_q

    Raises
    ------
    ValueError
        If azi0_in or rg_in are not monotonically increasing
    """
    # create the azimuth mask to exclude any bad (non-increasing) azimuths
    if np.any(np.diff(azi_in) <= 0):  # non-increasing azimuths?
        azi_mask = np.ones_like(azi_in, dtype=bool)
        azi_mask[1:] = np.diff(azi_in)
        azi_in = azi_in[azi_mask]
        intensity_in = intensity_in[azi_mask, :]
        if time_in is not None:
            time_in = time_in[azi_mask]
        logger.debug("Removed %d non-increasing azimuths", np.sum(~azi_mask))

    # Make sure azi_in and rg_in are monotonically increasing
    if not (np.all(np.diff(azi_in) > 0) and np.all(np.diff(rg_in) > 0)):
        raise ValueError("azi_in and rg_in must be monotonically increasing.")

    # Interpolate the intensity
    scattered = azi_q.shape == rg_q.shape and len(azi_q.shape) > 1
    if scattered:
        f = RegularGridInterpolator(
            (azi_in, rg_in), intensity_in, bounds_error=False, fill_value=np.nan
        )
        intensity_out = f((azi_q, rg_q))
    else:
        f = interp2d(
            rg_in, azi_in, intensity_in, copy=False, bounds_error=False, fill_value=np.nan
        )
        intensity_out = f(rg_q, azi_q, assume_sorted=True)

    # Find query azi and r that are outside the data bounds
    aziq_oob = (azi_q > np.max(azi_in)) | (azi_q < np.min(azi_in))
    rq_oob = (rg_q > np.max(rg_in)) | (rg_q < np.min(rg_in))
    # Set out-of bounds query locations to zero
    aziq_rq_oob = np.zeros_like(intensity_out, dtype=bool)
    if not scattered:
        aziq_rq_oob[aziq_oob, :] = True
        aziq_rq_oob[:, rq_oob] = True
    else:
        aziq_rq_oob = aziq_oob | rq_oob

    # Interpolate time, if requested
    time_out = None
    if not scattered:
        f_t = interp1d(
            azi_in, time_in, copy=False, fill_value="extrapolate", assume_sorted=True
        )
        time_out = f_t(azi_q)

    # set return vars
    azi_oob = aziq_oob if not scattered else None
    return intensity_out, time_out, azi_oob, aziq_rq_oob
