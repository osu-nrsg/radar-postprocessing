function Cube = coRectify(Cube, params)
%
% This function reads in a Cube (e.g. Cube = load('*_pol.mat');) and
% computes azimuthal cross-correlations at specified ranges to co-register
% all frames. The frame with least likelihood of having a trigger-skip is
% used as the control.
%
% Output is the same input Cube, but with the Cube.data field overwritten.
% If selected (doOverwrite = false), a new field will be added instead:
% Cube.dataCoRect
%
% A new field with heading offset values is added:
% Cube.headingOffset        = Offset vector utilized in azimuthal shifts
%                             (newHeading = Cube.results.heading + Cube.headingOffset)
%
% 2017-05-31 David Honegger
% 2019-02-14 Update: Define reference feature and search boxes; also use
% xcorr2() instead of an xcorr() per range
%

narginchk(2, 2);
original_path = path;


if isstruct(params)
    % assign params to variables in this workspace
    % param_map maps `params` field names to varnames for this script
    param_map = { ...
        'do_overwrite', 'doOverwrite'; ...
        'ref_coords', 'refCoords'; ...
        'search_width', 'searchWidth'; ...
        'interp_method', 'interpMethod'; ...
        'thresh_lag', 'threshLag'; ...
    };
    for i = 1:size(param_map, 1)
        param_name = param_map{i, 1};
        varname = param_map{i, 2};
        try
            assignin('caller', var_name, params.(param_name));
        catch err
            if strcmp(err.identifier, 'MATLAB:nonExistentField')
                error('coRectify:missing_param', '''%s'' is missing from params struct.', param_name);
            else
                rethrow(err);
            end
        end
    end
elseif ischar(params) && exist(params, 'file')
    %%%%%%%% DEFAULT USER INPUT PARAMETERS %%%%%%%%%%%%%
    %%% Reference feature coordinates [azi0 azi1 dAzi range0 range1 drange]. Zero and Inf
    %%% allowed. Note: dAzi should be smaller than the native resolution
    doOverwrite     = true;
    refCoords       = [120 155 1/20     1850 inf 100]; % In degrees and meters
    searchWidth     = 15;             % Search region width beyond edge of reference feature (in degrees)
    interpMethod    = 'spline';           % For griddedInterpolant.m ... 'linear','spline','nearest', etc.
    threshLag       = searchWidth;                     % Frames with lags beyond this value don't get adjusted (in degrees)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % strip off and add the path of the params file, if necessary.
    [params_dir, paramsFilename, ~] = fileparts(params);
    if ~isempty(params_dir)
        addpath(params_dir);
    end

    % Import the provided parameters file
    eval(paramsFilename)
else
    error('coRectify:bad_params', '''params'' is not a struct or path to an M-file.')
end

%% Some parameter calculations
%%% Calculate native grid information
minRange            = find(diff(Cube.Rg),1,'first'); % First range that steps forward from antenna

%%% Calculate reference and search vectors
refCoords(1)        = max(refCoords(1),min(Cube.Azi));
refCoords(2)        = min(refCoords(2),max(Cube.Azi));
refCoords(4)        = max(refCoords(4),minRange);
refCoords(5)        = min(refCoords(5),max(Cube.Rg));
%
if length(searchWidth)==1
    searchWidth     = repmat(searchWidth,1,2);
end
searchCoords        = refCoords; % Just populate for now
searchCoords(1)     = max(refCoords(1)-searchWidth(1),min(Cube.Azi));
% // (Ensure that grids are aligned)
gridAlign           = mod(refCoords(1)-searchCoords(1),refCoords(3));
if gridAlign~=0
    searchCoords(1) = searchCoords(1) + gridAlign;
end
% //
searchCoords(2)     = min(refCoords(2)+searchWidth(2),max(Cube.Azi));
%
referenceAzi        = refCoords(1):refCoords(3):refCoords(2);
referenceRg         = refCoords(4):refCoords(6):refCoords(5);
searchAzi           = searchCoords(1):searchCoords(3):searchCoords(2);
searchRg            = referenceRg;

%%% Calculate reference and search indices in native grid
[RG,AZI]            = ndgrid(Cube.Rg,Cube.Azi);
referenceAziIdx     = unique(find_approx(referenceAzi,Cube.Azi));
referenceRgIdx      = unique(find_approx(referenceRg,Cube.Rg));
%
searchAziIdx        = unique(find_approx(searchAzi,Cube.Azi));
searchRgIdx         = referenceRgIdx;

%%% Calculate interpolant bounding box
bboxAzi             = searchAziIdx(1) :  searchAziIdx(end);
bboxRg              = searchRgIdx(1)  :  searchRgIdx(end);


%%% Calculate reference and search indices in search grid
[hires.RG,hires.AZI]    = ndgrid(searchRg,searchAzi);
hires.referenceAziIdx   = (referenceAzi(1)-searchAzi(1))/refCoords(3) + (0:length(referenceAzi)-1);


% azimuthResolutionFactor = 1/10;    % Resolution factor of search grid (1 if same as input resolution, 1/2 if two gridpoints per input resolution cell, etc.)
% rangesToAnalyze = [1800 2015];       % Ranges to consider (in PIXELS)
% azisToAnalyze = [163 178];        % Azimuths to consider (in DEGREES)
% dRange = 6;                         % Range decimation of analysis (in pixels)
% maxLag = 15;                        % Maximum lag to consider (in degrees)
%
%
%
% % HiRes grid
% dAzi = aziResolution*azimuthResolutionFactor;
% nativeAziRange = [min(Cube.Azi) max(Cube.Azi)];
% aziVec = nativeAziRange(1):dAzi:nativeAziRange(2)-dAzi;
% [hires.RG,hires.AZI] = ndgrid(Cube.Rg,aziVec);
%
% Output grid: same as input grid:
dataOut = Cube.data;
%
% % Analysis indices
% rangeIdxAll         = max(minRange,rangesToAnalyze(1)):min(rangesToAnalyze(2),length(Cube.Rg)); % Continuous set of range indices
% rangeIdxToAnalyze   = rangeIdxAll(1:dRange:end); % Decimate range indices
%
% aziIdxRangeToAnalyze = find_approx([max(0,azisToAnalyze(1)),min(azisToAnalyze(2),max(Cube.Azi))],Cube.Azi); % Find azimuth indices that correspond to chosen angles in hires grid
% aziIdxToAnalyze     = aziIdxRangeToAnalyze(1):aziIdxRangeToAnalyze(2); % Continuous set of azimuth indices in hires grid

%% Choose control frame: smallest squared difference of reference feature from median frame
idAziNotBlank         = mean(diff(Cube.data,[],1)==0)<0.99; % is the mean dI/dr equal to zero?
isAziNotBlankMask     = single(repmat(idAziNotBlank,size(Cube.data,1),1,1));
isAziNotBlankMask(isAziNotBlankMask==0) = NaN;
datac = single(Cube.data).*isAziNotBlankMask;
mean_dIdAz = squeeze(mean((diff(datac,[],2).^2),'omitnan'));
%%% Prefer frames with most variance in azimuthal direction
trigSkipDiagnostic = sum(mean_dIdAz,'omitnan');
A = median(trigSkipDiagnostic);
S = std(trigSkipDiagnostic);
idRemove = trigSkipDiagnostic < (A - S);
% idRemove = trigSkipDiagnostic < prctile(trigSkipDiagnostic,32);  % requires a toolbox...

%%%% Outdated diagnostic (fails when signal is low or saturated)
% trigSkipDiagnostic = sum(mean_dIdAz<1);
% trigSkipDiagnostic = sum(squeeze(mean(diff(single(Cube.data(:,:,:)).*isAziNotBlankMask,[],2),'omitnan')) < 1,'omitnan');
% idRemove = trigSkipDiagnostic > 1;

medFrame            = uint8(median(Cube.data(referenceRgIdx,referenceAziIdx,:),3));
diffMedianArray     = Cube.data(referenceRgIdx,referenceAziIdx,:)-repmat(medFrame,1,1,size(Cube.data,3));
medianDiagnostic    = squeeze(mean(mean(diffMedianArray.^2)));
medianDiagnostic(idRemove) = NaN;
[~,bestFrameIdx]    = min(medianDiagnostic);
rotationsToAnalyze  = setdiff(1:size(Cube.data,3),bestFrameIdx);

%% Now apply high-pass filter to remove large moving features (e.g. rain cells)
dataFiltered = Cube.data - imboxfilt(Cube.data,[round(diff(refCoords(4:5))/8)*2+1 round(diff(refCoords(1:2))/8)*2+1]);

%% Generate interpolant for generating reference and search grid values
interpFuncXcorr = griddedInterpolant(...
    RG(bboxRg,bboxAzi),...
    AZI(bboxRg,bboxAzi),...
    single(dataFiltered(bboxRg,bboxAzi,bestFrameIdx)),...
    interpMethod,'nearest');
controlFrame = interpFuncXcorr(hires.RG(:,hires.referenceAziIdx),hires.AZI(:,hires.referenceAziIdx));

%% Create offset interpolant
offsetInterpolantInitialized = false; % Only initialize if an offset is called for

headingOffset = zeros(size(Cube.data,3),1);



% aziIdxRangeToAnalyze = find_approx([max(0,azisToAnalyze(1)),min(azisToAnalyze(2),max(Cube.Azi))],aziVec); % Find azimuth indices that correspond to chosen angles in hires grid
% aziIdxToAnalyze     = aziIdxRangeToAnalyze(1):aziIdxRangeToAnalyze(2); % Continuous set of azimuth indices in hires grid
%% Loop thru rotations (first is redundant)
for iRot = rotationsToAnalyze

    % Interp this rotation to hires azi grid
    interpFuncXcorr.Values  = single(dataFiltered(bboxRg,bboxAzi,iRot));
    testFrame               = interpFuncXcorr(hires.RG,hires.AZI);

    lags = findArrayDelay(controlFrame,testFrame)-hires.referenceAziIdx(1);
    headingOffset(iRot) = -(lags)*refCoords(3);



    logmsg('Rotation %.f: ',iRot)
    % If lag~=0, interpolate test frame to control grid
    if headingOffset(iRot)
        if abs(headingOffset(iRot)) < threshLag

            if ~offsetInterpolantInitialized
                interpFuncOffset = griddedInterpolant(...
                    RG(minRange:end,:),...
                    AZI(minRange:end,:),...
                    single(Cube.data(minRange:end,:,bestFrameIdx)),...
                    interpMethod,'nearest');
                offsetInterpolantInitialized = true;
            end


            interpFuncOffset.GridVectors        = {Cube.Rg(minRange:end),Cube.Azi + headingOffset(iRot)};
            interpFuncOffset.Values             = single(Cube.data(minRange:end,:,iRot));
            dataOut(minRange:end,:,iRot)        = uint8(interpFuncOffset(RG(minRange:end,:),AZI(minRange:end,:)));
            logmsg('Shifted by %.2f degrees.\n',headingOffset(iRot))
        else % If lag > threshold, probably a skipped trigger and this process won't help
            logmsg('Max lag threshold exceeded. Lag is %.2f.\n',iRot,headingOffset(iRot))
        end
    else
        logmsg('No shift.\n')
    end

end

Cube.headingOffset = headingOffset;
if doOverwrite
    Cube.data = dataOut;
	Cube.timex = mean(dataOut,3);
else
    Cube.dataCoRect = dataOut;
	Cube.timexCoRect = mean(dataOut,3);
end
Cube.coRectParams = struct(...
    'refCoords', refCoords, 'searchWidth', searchWidth, ...
    'interpMethod', interpMethod, 'threshLag', threshLag);

% Restore path
path(original_path);

end

%% Add find_approx.m from toolbox
function g = find_approx(value,refArray,n)
%     g = find_approx(m,v,[n])
% Find index (g) of matrix (m) that is most nearly equal to a value (v). This
% is similar to 'g = find(m==v)', except that the nearest approximate equality
% is found if no exact equality exists.
% The third argument (default n=1) tells how many values to find
% (e.g., n=3 means the nearest 3 indices in order of descending nearness).

%%% Edited David Honegger 07-2015 to allow m to be a vector
if length(value)<2
    g=find(value==refArray);
    if isempty(g)
        [nul,g]=min(abs(value-refArray));
        if isnan(nul)
            g = nan;
        elseif isinf(nul)
            if value > 0
                [~,g] = max(refArray);
            else
                [~,g] = min(refArray);
            end
        end
    end

    if nargin>2
        g=zeros(n,1)*nan;
        for nn=1:n
            [~,g(nn)]=min(abs(value-refArray));
            if any(isdatetime(value))
                refArray(g(nn)) = NaT;
            else
                refArray(g(nn))=nan;
            end
        end
    end
else
    value = value(:);
    for im = 1:length(value)

        gtmp=find(value(im)==refArray);
        if isempty(gtmp)
            [nul,gtmp]=min(abs(value(im)-refArray));
            if isnan(nul)
                gtmp = nan;
            elseif isinf(nul)
                if value(im) > 0
                    [~,gtmp] = max(refArray);
                else
                    [~,gtmp] = min(refArray);
                end
            end
        end


        if nargin>2
            gtmp=zeros(n,1)*nan;
            for nn=1:n
                [~,gtmp(1,nn)]=min(abs(value(im)-refArray));
                if any(isdatetime(value))
                    refArray(gtmp(1,nn)) = NaT;
                else
                    refArray(gtmp(1,nn)) = NaN;
                end
            end
        end

        g(im,:) = gtmp;
    end
end
end

function logmsg(varargin)
fprintf(2, varargin{:});
end
