function Cube = coGeoRectify(Cube, params)
%
% This function reads in a coRectified Cube
% e.g. ......
%   params = '/shared/local_config/rectParams.m';
%   Cube = load('*_pol.mat');
%   Cube = coRectify(Cube, params);
%   Cube = coGeoRectify(Cube, params);
% ...... and then computes azimuthal cross-correlations at specified ranges
% to co-rectify the azimuthal grid to a control grid that is (hopefully)
% properly rectified already.
%
% Output is the same input Cube, but with the Cube.data field overwritten.
% The Cube.results.heading value is updated to match the control Cube
% heading, and the old heading value is renamed to:
% Cube.results.headingOld
%
% 2017-05-31 David Honegger
% 2019-02-13 DAH Update: Set range of ranges and azimuths (instead of
% assuming they border on first or last index; also allow consideration of
% footprints smaller than 360 deg
%

narginchk(2, 2);
original_path = path;

if isstruct(params)
    % assign params to variables in this workspace
    % param_map maps `params` field names to varnames for this script
    param_map = { ...
        'do_overwrite', 'doOverwrite'; ...
        'ref_coords', 'refCoords'; ...
        'search_width', 'searchWidth'; ...
        'interp_method', 'interpMethod'; ...
        'thresh_lag', 'threshLag'; ...
        'control_cube', 'controlCube'; ...
    };
    for i = 1:size(param_map, 1)
        param_name = param_map{i, 1};
        varname = param_map{i, 2};
        try
            assignin('caller', var_name, params.(param_name));
        catch err
            if strcmp(err.identifier, 'MATLAB:nonExistentField')
                error('coGeoRectify:missing_param', '''%s'' is missing from params struct.', param_name);
            else
                rethrow(err);
            end
        end
    end
elseif ischar(params) && exist(params, 'file')
    %%%%%%%% DEFAULT USER INPUT PARAMETERS %%%%%%%%%%%%%
    %%% Reference feature coordinates [azi0 azi1 dAzi range0 range1 drange]. Zero and Inf
    %%% allowed. Note: dAzi should be smaller than the native resolution
    doOverwrite     = true;
    refCoords       = [120 155 1/20     1850 inf 100]; % In degrees and meters
    searchWidth     = 15;             % Search region width beyond edge of reference feature (in degrees)
    interpMethod    = 'spline';           % For griddedInterpolant.m ... 'linear','spline','nearest', etc.
    threshLag       = searchWidth;                     % Frames with lags beyond this value don't get adjusted (in degrees)
    controlCube     = 'geoRefTimex.mat'; % Name of reference cube (needs to be in matlab path or absolute path)
    %%%%%%%% //USER INPUT PARAMETERS (SHOULD BE FUNCTION ARGUMENTS) %%%%%%%%%%%%%

    % strip off and add the path of the params file, if necessary.
    [params_dir, paramsFilename, ~] = fileparts(params);
    if ~isempty(params_dir)
        addpath(params_dir);
    end

    % Import the provided parameters file
    eval(paramsFilename)
else
    error('coGeoRectify:bad_params', '''params'' is not a struct or path to an M-file.')
end

%% Some parameter calculations
%%% Calculate native grid information
minRange            = find(diff(Cube.Rg),1,'first'); % First range that steps forward from antenna

%%% Calculate reference and search vectors
refCoords(1)        = max(refCoords(1),min(Cube.Azi));
refCoords(2)        = min(refCoords(2),max(Cube.Azi));
refCoords(4)        = max(refCoords(4),minRange);
refCoords(5)        = min(refCoords(5),max(Cube.Rg));
%
if length(searchWidth)==1
    searchWidth     = repmat(searchWidth,1,2);
end
searchCoords        = refCoords; % Just populate for now
searchCoords(1)     = max(refCoords(1)-searchWidth(1),min(Cube.Azi));
% // (Ensure that grids are aligned)
gridAlign           = mod(refCoords(1)-searchCoords(1),refCoords(3));
if gridAlign~=0
    searchCoords(1) = searchCoords(1) + gridAlign;
end
% //
searchCoords(2)     = min(refCoords(2)+searchWidth(2),max(Cube.Azi));
%
referenceAzi        = refCoords(1):refCoords(3):refCoords(2);
referenceRg         = refCoords(4):refCoords(6):refCoords(5);
searchAzi           = searchCoords(1):searchCoords(3):searchCoords(2);
searchRg            = referenceRg;

%%% Calculate reference and search indices in native grid
[RG,AZI]            = ndgrid(Cube.Rg,Cube.Azi);
referenceAziIdx     = unique(find_approx(referenceAzi,Cube.Azi));
referenceRgIdx      = unique(find_approx(referenceRg,Cube.Rg));
%
searchAziIdx        = unique(find_approx(searchAzi,Cube.Azi));
searchRgIdx         = referenceRgIdx;

%%% Calculate interpolant bounding box
bboxAzi             = searchAziIdx(1) :  searchAziIdx(end);
bboxRg              = searchRgIdx(1)  :  searchRgIdx(end);


%%% Calculate reference and search indices in search grid
[hires.RG,hires.AZI]    = ndgrid(searchRg,searchAzi);
hires.referenceAziIdx   = (referenceAzi(1)-searchAzi(1))/refCoords(3) + (0:length(referenceAzi)-1);

% Output grid: same as input grid:
if isfield(Cube,'dataCoRec')
    % If there is a dataCoRec field (from coRectify.m), assume that it is
    % the one we care about
    dataOut = Cube.dataCoRec;
else
    dataOut = Cube.data;
end

%% Generate interpolant for generating reference and search grid values
refCube = load(controlCube);
if isfield(refCube,'timex')
    refFrame = double(refCube.timex);
else
    refFrame = double(mean(refCube.data,3));
end

interpFuncXcorr = griddedInterpolant(...
    RG(bboxRg,bboxAzi),...
    AZI(bboxRg,bboxAzi),...
    single(refFrame(bboxRg,bboxAzi)),...
    interpMethod,'nearest');
controlFrame = interpFuncXcorr(hires.RG(:,hires.referenceAziIdx),hires.AZI(:,hires.referenceAziIdx));

%% Generate search frame
interpFuncXcorr.Values  = single(mean(Cube.data(bboxRg,bboxAzi,:),3));
testFrame               = interpFuncXcorr(hires.RG,hires.AZI);

%% Calculate lag between reference (control) frame and search (test) frame
lags = findArrayDelay(controlFrame,testFrame)-hires.referenceAziIdx(1);
headingOffset = -(lags)*refCoords(3);

% If lag~=0, interpolate test frame to control grid
if headingOffset
    if abs(headingOffset) < threshLag
        logmsg('Shifting Cube data by %.2f degrees:\n',headingOffset)
        % Now create interpolant for offsetting all frames
        interpFuncOffset = griddedInterpolant(...
                    RG(minRange:end,:),...
                    AZI(minRange:end,:),...
                    single(Cube.data(minRange:end,:,1)),...
                    interpMethod,'nearest');

        for iRot = 1:size(Cube.data,3)
            logmsg('.');if mod(iRot,64)==0;logmsg('\n');end
            interpFuncOffset.GridVectors = {Cube.Rg(minRange:end),Cube.Azi + headingOffset};
            interpFuncOffset.Values = single(Cube.data(minRange:end,:,iRot));
            dataOut(minRange:end,:,iRot) = uint8(interpFuncOffset(RG(minRange:end,:),AZI(minRange:end,:)));
        end
            logmsg('Done.\n')
    else
        logmsg('Max lag threshold exceeded.\n')
    end
else
    logmsg('No shift.\n')
end

%%% Update rectification parameters in "results" field with reference
%%% parameters
Cube.results.XOrigin    = refCube.results.XOrigin;
Cube.results.YOrigin    = refCube.results.YOrigin;
Cube.results.ZOrigin    = refCube.results.ZOrigin;
Cube.results.UTMZone    = refCube.results.UTMZone;
Cube.results.doughnut   = refCube.results.doughnut;

Cube.results.headingOld = Cube.results.heading;
Cube.results.heading = refCube.results.heading;
Cube.headingOffset = headingOffset;
if doOverwrite
    Cube.data = dataOut;
	Cube.timex = mean(dataOut,3);
else
    Cube.dataCoGeoRect = dataOut;
	Cube.timexCoGeoRect = mean(dataOut,3);
end
Cube.coGeoRectParams = struct(...
    'refCoords', refCoords, 'searchWidth', searchWidth, ...
    'interpMethod', interpMethod, 'threshLag', threshLag, ...
    'controlCube', controlCube);

% Restore path
path(original_path);

end


%% Add find_approx.m from toolbox
function g = find_approx(value,refArray,n)
%     g = find_approx(m,v,[n])
% Find index (g) of matrix (m) that is most nearly equal to a value (v). This
% is similar to 'g = find(m==v)', except that the nearest approximate equality
% is found if no exact equality exists.
% The third argument (default n=1) tells how many values to find
% (e.g., n=3 means the nearest 3 indices in order of descending nearness).

%%% Edited David Honegger 07-2015 to allow m to be a vector
if length(value)<2
    g=find(value==refArray);
    if isempty(g)
        [nul,g]=min(abs(value-refArray));
        if isnan(nul)
            g = nan;
        elseif isinf(nul)
            if value > 0
                [~,g] = max(refArray);
            else
                [~,g] = min(refArray);
            end
        end
    end

    if nargin>2
        g=zeros(n,1)*nan;
        for nn=1:n
            [~,g(nn)]=min(abs(value-refArray));
            if any(isdatetime(value))
                refArray(g(nn)) = NaT;
            else
                refArray(g(nn))=nan;
            end
        end
    end
else
    value = value(:);
    for im = 1:length(value)

        gtmp=find(value(im)==refArray);
        if isempty(gtmp)
            [nul,gtmp]=min(abs(value(im)-refArray));
            if isnan(nul)
                gtmp = nan;
            elseif isinf(nul)
                if value(im) > 0
                    [~,gtmp] = max(refArray);
                else
                    [~,gtmp] = min(refArray);
                end
            end
        end


        if nargin>2
            gtmp=zeros(n,1)*nan;
            for nn=1:n
                [~,gtmp(1,nn)]=min(abs(value(im)-refArray));
                if any(isdatetime(value))
                    refArray(gtmp(1,nn)) = NaT;
                else
                    refArray(gtmp(1,nn)) = NaN;
                end
            end
        end

        g(im,:) = gtmp;
    end
end
end

function logmsg(varargin)
fprintf(2, varargin{:});
end
