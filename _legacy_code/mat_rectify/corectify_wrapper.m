function corectify_wrapper(mat_path, corectify_params_json, cogeorectify_params_json)
% CORECTIFY_WRAPPER - Wrapper script to process a cube with coRectify and optionally coGeoRectify
%
% Inputs:
%  mat_path:
%    Path to the cube
%  corectify_params_json:
%    json-encoded coRectify params struct. To skip corectify, set to 'null'.
%  cogeorectify_params_json
%    json-encoded coGeoRectify params struct. To skip coGeorectify, don't provide or set to 'null'.

narginchk(2, 3)
if nargin < 3
    cogeorectify_params_json = 'null';
end


cube = load(mat_path);
% decode json to structs
cr_params = jsondecode(corectify_params_json)
cgr_params = jsondecode(cogeorectify_params_json)
modified = false;
% Run coRectify if params provided
if ~isempty(cr_params)
    cube = coRectify(cube, cr_params);
    modified = true;
end
% Run coGeoRectify if params provided
if ~isempty(cgr_params)
    cube = coGeoRectify(cube, cr_params);
    modified = true;
end

if modified
    save(mat_path, '-struct', 'cube', '-v7.3');
end
