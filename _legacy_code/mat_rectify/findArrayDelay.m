function [d,cn] = findArrayDelay(R,S)
%
% find horizontal lag between reference array R and search array S
% this is akin to matching the best location of feature R within a larger
% (or equal) window S
%
% Note that size(R,1)==size(S,1) because this is a horizontal search only
[ry,rx] = size(R);
[sy,sx] = size(S);

cr0 = sum(abs(R(:)).^2);
cs0 = sum(abs(S(:)).^2);
if ( (cr0==0) || (cs0==0) )
    % If either sequence x or y is all zeros, set c to all zeros.

    cn = NaN;
    d = 0;
    return
else

    cn = abs(xcorr2(S,R))/sqrt(cr0*cs0);
    cn = cn(sy,:);
    [~,idmax] = max(cn);
    d = idmax-rx;
        
    
end