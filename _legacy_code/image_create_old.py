import io
import logging
from datetime import datetime, timezone
from pathlib import Path
from subprocess import CalledProcessError

# prevent error loading matlab engine
# isort: off
from wimr_postproc.mat_eng import MatlabExecutionError, get_mat_eng
import h5py

# isort: on

from wimr_postproc._logging import log_called_process_error
from wimr_postproc.errors import ImageCreationError
from wimr_postproc.params import ImageryGenParams
from wimr_postproc.util.types import PathLike

logger = logging.getLogger(__name__)


def plot_and_send_imagery(
    cube_path: PathLike, params: ImageryGenParams
) -> tuple[list[Path], Path]:
    """Plot radar imagery and optionally send plots to a remote server.

    Parameters
    ----------
    cube_path
        Path to the cube
    params
        ImageryGenParams with the parameters for how to create the plots and
        transfer the data to a remote location.

    Returns
    -------
    image_paths
        List of image paths
    json_path
        Path to JSON metadata file

    Raises
    ------
    RuntimeError
        Problem starting MATLAB
    ImageCreationError
        An error has occurred in running the imagery generation MATLAB script
    """
    cube_path = Path(cube_path)
    with h5py.File(cube_path) as ds:
        start_time = ds["results"]["start_time"]["epoch"][0, 0]  # type: ignore
        if not isinstance(start_time, (int, float)):
            raise ValueError(
                f"results.start_time.epoch in {cube_path} is not a float or int."
            )
    date_dirname = datetime.fromtimestamp(start_time, tz=timezone.utc).strftime(
        "%Y-%m-%d"
    )
    matlab = get_mat_eng()
    # MATLAB scripts are in a dir relative to this module
    matlab.cd(str(Path(__file__).parent / "mat_image_create"))
    sout = io.StringIO()
    serr = io.StringIO()
    errmsg = ""
    logger.debug("pic_args:")
    logger.debug("cube_path = '%s'\njson_params = '%s'", cube_path, params.json())
    try:
        image_paths, json_path = matlab.pic_wrapper(
            str(cube_path), params.json(), stdout=sout, stderr=serr, nargout=2
        )  # type: ignore
        logger.debug("MATLAB output:\n%s", sout.getvalue())
        errmsg = serr.getvalue()
    except MatlabExecutionError as err:
        raise ImageCreationError("Error calling pic_wrapper") from err
    finally:
        sout.close()
        serr.close()
    if image_paths == "error":
        raise ImageCreationError(f"Error running pic_wrapper:\n{errmsg}")
    if params.rsync_params:
        logger.info("Sending imagery and json files to campus.")
        fpaths = image_paths + [json_path]
        try:
            params.rsync_params.send(fpaths, dest_dir=date_dirname, check=True)
        except CalledProcessError as err:
            # Report rsync errors but do not stop execution.
            log_called_process_error(
                logger, err, "There was an error sending the imagery to campus."
            )
    image_paths = [Path(p) for p in image_paths]
    return image_paths, Path(json_path)
