function [utentrend]=removeTrend(medtrend)
% Written by Kjell Inge Gausland
% Last edited: 200499 Kjell Inge Gausland
%
% medtrend - matrix with trend
% utentrend - matrix with this trend removed
%
% The function fits a regression plane to the data in medtrend and
% removes this trend.

% Figuring out the dimension of the matrix and initialising some
% variables 
[ni, nj]=size(medtrend);

isum1 = ni;
isumi = ni*(ni+1)/2;
isumi2 = ni*(ni+1)*(2*ni+1)/6;

jsum1 = nj;
jsumj = nj*(nj+1)/2;
jsumj2 = nj*(nj+1)*(2*nj+1)/6;

% sumjx
sumjx = 0;
for j=1:nj,
  sumjx = sumjx + j*sum(medtrend(:,j));
end

% sumix
sumix = 0;
for i=1:ni,
  sumix = sumix + i*sum(medtrend(i,:));
end

sumx = sum(sum(medtrend));

% Producing the the equations to be solved: Ax=b
A = [isum1*jsumj2 isumi*jsumj isum1*jsumj;
     isumi*jsumj jsum1*isumi2 isumi*jsum1;
     isum1*jsumj jsum1*isumi isum1*jsum1];
b = [sumjx sumix sumx];

% solves
x = b/A;

% make the plane
a = x(1);
b = x(2);
c = x(3);

[J I] = meshgrid(1:1:nj, 1:1:ni);
plan = J.*a + I.*b + c;

% Remove this plane
utentrend = medtrend - plan;
