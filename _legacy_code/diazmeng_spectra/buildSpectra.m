function buildSpectra(varargin)
% FUNCTION buildSpectra
% Purpose:
%   This function creates spectrum plots from the specified cube. Constant
%   parameters within this function set the subimages to use to get the
%   spectrum plots. The spectra from all subplots are saved as png files,
%   and also if one of them is good enough (high enough SNR in the data)
%   then it is saved as the "best" spectrum.
%
% Usage:
%   buildSpectra('argname',arg,...)
%
% Inputs:
%   - dir_cubes:
%       String containing the path to the location of the
%       cartesian-interpolated cube MAT file. Defaults to
%       C:\1MarineWinXP\to_cube\cubes.
%   - cubeName:
%       Name of the cube MAT file to read in. If not provided, a prompt
%       will ask for the cube name (unless cube, below, is provided).
%   - cube:
%       Alteratively, the cube struct may be provided directly instead of
%       having the cube file be read in. This is a little faster if the
%       cube is already in memory.
%   - plot_timezone: 
%       Timezone ID to use for the plot timestamps. Note that if the
%       timezone is of the form 'US/Pacific', then the 'US/' will be
%       stripped off for the plot title. For more info see the help for
%       radar_toolbox.timezone_convert. Defaults to 'UTC'.
%   - subimages:
%       A struct or array of structs with the bounding information for the
%       region(s) to examine. Required fields include:
%         - center.x, center.y:
%             Center x and y coordinates, UTM meters (Eastings, Northings)
%         - name: Name of subimage
%         - size: Side length of the subimage (subimage will be square)
%         - h: Approximate water depth for the region
%       Defaults to two regions west of Nye Beach and South Beach, near
%       Newport.
%   - wave_heading:
%           Defines whether the wave angle is the direction the waves are
%           heading (true), or the direction from which the waves
%           are coming (false, default).
%
% Outputs:
%   No variables are returned, but the spectral plots are saved to 
%   dir_cubes
%
% Customization:
%   This function currently has the positions of the subplots hard-coded, 
%   under the "User-adjustable parameters" section. (Eventually want to 
%   pull this out to input arguments).
%
% Notes:
%   1. Even if plot_timezone is specified, the time stamp saved in the
%      intParams file remains the same as the cube file (should be UTC,
%      assuming the clock on the DAQ PC was correctly synced to UTC, even
%      if the PC's timezone was set to something else).
%      Only the timestamps on the plots are changed.

% History:
%   Guillermo Diaz-Mendez 11/2012 OSU (Updated: Nov/05/2012)
%   Randall Pittman 2012-11-13:
%       Converted to script to play nicely with other Newport scripts.

%% Timing
tic;

%% Inputs
% defaults
dir_cubes = 'C:\1MarineWinXP\to_cube\cubes';
plot_timezone = 'UTC';

i = 1;
while i <= length(varargin)
    switch varargin{i}
        case 'dir_cubes'
            dir_cubes = varargin{i+1};
            i = i + 1;
        case 'cubeName'
            cubeName = varargin{i+1};
            i = i + 1;
        case 'cube'
            cube = varargin{i+1};
            assert(isstruct(cube),'radar_scripts:buildSpectra:cubeNoGood','The provided cube argument is not a struct.');
            i = i + 1;
        case 'subimages'
            subimages = varargin{i+1};
            assert(isstruct(cube),'radar_scripts:buildSpectra:subimagesNoGood','The provided subimages argument is not a struct.');
            i = i + 1;
        case 'plot_timezone'
            plot_timezone = varargin{i+1};
            i = i + 1;
        case 'wave_heading'
            wave_heading = varargin{i+1};
            i = i + 1;
        otherwise
            error('radar_scripts:buildSpectra:badArg','Bad input argument');
    end
    i = i + 1;
end

if ~exist('cubeName', 'var') && ~exist('cube', 'var')
    cubeName =  input('Cube file to load:  ', 's');
end

%  Load cube if not already provided:
if ~exist('cube','var')
    cube = load(fullfile(dir_cubes, cubeName));
end

% maxL - needed to determine if a calculated spectrum is any good
if isfield(cube,'maxL')
    maxL = cube.maxL;
else
    maxL = 300;
end

% longName
if isfield(cube,'longName')
    longName = cube.longName;
else
    longName = cube.station;
end

if ~exist('subimages','var')
    subimages(1).center_x = 413632;
    subimages(1).center_y = 4942403;
    subimages(1).name = 'Nye Beach';
    subimages(1).size = 128;
    subimages(1).h = 17.5;
    
    subimages(2).center_x = 413713;
    subimages(2).center_y = 4938053;
    subimages(2).name = 'South Beach';
    subimages(2).size = 128;
    subimages(2).h = 17.5;
end

if exist('wave_heading','var')
    if ~islogical(wave_heading) || ~isscalar(wave_heading)
        error('radar_scripts:buildSpectra:arg_wave_heading_bad','wave_heading must be a logical scalar.');
    end
else
    wave_heading = false;
end

% Get file number
filenumber = cube.header.file(2:8);

%%  Directories:
dir_res = dir_cubes; % directory where the processed cubes will be saved to
% dir_res = 'c:\Documents and Settings\daq-user\Desktop';

%% Get calculated members of subimages struct
tmp = num2cell([subimages.size].*cube.d_xy); % get side length values
[subimages.side] = deal(tmp{:});% put side length values into struct
tmp = num2cell([subimages.center_x] - [subimages.side]./2); % get x1 values
[subimages.x1] = deal(tmp{:}); % put x1 values into struct
tmp = num2cell([subimages.x1] + [subimages.side]); % get x2 values
[subimages.x2] = deal(tmp{:}); % put x2 values into struct
tmp = num2cell([subimages.center_y] - [subimages.side]./2); % get y1 values
[subimages.y1] = deal(tmp{:}); % put y1 values into struct
tmp = num2cell([subimages.y1] + [subimages.side]); % get y2 values
[subimages.y2] = deal(tmp{:}); % put y2 values into struct
tmp = cellfun(@(name) name(name ~= ' '), {subimages.name},'UniformOutput',false); % get the name without spaces
[subimages.nsname] = deal(tmp{:}); % put the name without spaces in the struct

%% Get time range of recording
rec_time = [radar_toolbox.epoch2Matlab(cube.time(1)) radar_toolbox.epoch2Matlab(cube.time(end))];
rec_time = radar_toolbox.timezone_convert(rec_time,'UTC',plot_timezone); % convert datenums to the desired time zone.
rec_time_str = datestr(rec_time,'yyyy-mm-dd HH:MM PM');
if strcmp(plot_timezone(1:3),'US/'),plot_timezone = plot_timezone(4:end); end % remove "US/" from "US/Pacific", etc.
timestr = [rec_time_str(1,1:16) ' - ' strtrim(rec_time_str(2,12:19)) ' ' plot_timezone];

%%  LOOP:

[subimages.int_angpeak] = deal(zeros(1,2));
[subimages.int_kpeak] = deal(subimages.int_angpeak);
[subimages.int_lpeak] = deal(subimages.int_angpeak);

for ss = 1:length(subimages)
    
    %  Select the subimages
    %  ~~~~~~~~~~~~~~~~~~
    %%% Convert UTM coordinates to Cube indices:
    x_ind = find(cube.grid_x >= subimages(ss).x1); % cube indices for Subimage Eastings
    y_ind = find(cube.grid_y >= subimages(ss).y1); % cube indices for Subimage Northings
    
    %  Compute the 3D fft
    %  ~~~~~~~~~~~~~~~~
    cubito = cube.data(y_ind(1):y_ind(1) + subimages(ss).size - 1 , x_ind(1):x_ind(1) + subimages(ss).size - 1 , :); % in UINT8 format
    cubito = double(cubito); % cube.data(1:end-1, 1:end-1, :));
    
    %%%  Prepare the prism for the 3D fft: convert values to sigma_0 and detrend
    
    cubote = zeros(size(cubito)); % to pre-allocate memory space
    
    for ii1 = 1:cube.header.rotations
        
        rebanada = cubito(:,:,ii1); % select individual rotation
        
        %%%  1) Transform each snapshot to radar cross-section:
        
        rebanada_0 = find(rebanada(:) == 0);
        rebanada(rebanada_0) = 0.0001;
        %         rebanada_s0 = 10*log10(rebanada/mean(rebanada(:)));
        
        %%%  2) Detrend each snapshot in space [x,y] domain:
        
        %  lis       rebanada_dt = removeTrend(rebanada_s0); % detrend prism slice.
        cubote(:,:,ii1) = rebanada;%_dt;
        
    end
    
    %%%  3) Detrend in the time domain:
    
    %     for ii2 = 1:size(cubote, 1)
    %         for jj2 = 1:size(cubote, 1)
    %
    %             cubote(ii2, jj2, :) = detrend(cubote(ii2, jj2, :));
    %
    %         end
    %     end
    
    
    Sp3D = fftn(cubote);
    
    for i0 = 1:size(Sp3D,3);
        Sp3D(:,:,i0) = fftshift(Sp3D(:,:,i0));
        % Sp3D(:,:,i0) = flipud(fliplr(Sp3D(:,:,i0)));
        Sp3D(:,:,i0) = rot90(Sp3D(:,:,i0),2);
    end
    
    Sp3D = Sp3D .* conj(Sp3D);
    
    
    %  Set up the f and k domains
    %  ~~~~~~~~~~~~~~~~~~~~~~~
    
    %%%  Frequency domain:
    
    d_t = cube.results.RunLength / cube.header.rotations;
    f_Ny = 1/(2*d_t); % Nyquist frequency
    f = linspace(0, f_Ny, 1+(cube.header.rotations/2));
    
    waves = find(f >= 1/20 & f<=1/5); % typical frequency range for wind waves
    f_waves = f(waves); % wind-waves frequency sub-domain.
    
    %%%  Wavenumber domain:
    
    k_Ny = 2*pi/(2*cube.d_xy); % Nyquist wavenumber
    k = linspace(0, k_Ny, 1+(subimages(ss).size/2)); % absolute (positive?) wavenumber domain
    k_full = [-k(end)+k(1:end-1) k(1:end-1)]; % from -k to k
    
    
    %  Filter the spectrum to typical ocean-wave dimensions
    %  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Lmax = 400; % maximum wavelength to compute [m] ~ T = 16 s.
    Lmin = 50; % minimum wavelength to compute [m] ~ T = 5 s.
    
    n_smooth = 2; % smoothing as a function of subImage size:
    
    kmax = 2 * pi / Lmin; % wavenumber limit (outer ring) for the spectral plot.
    
    xLength = size(cubote, 1); % length of the subImage [pixels];
    
    tileSize = size(cubote(:,:,1));
    
    if mod((tileSize/2),2) == 0; % to determine the center pixel
        halfTile= ceil (tileSize/2)+1;
    else
        halfTile =ceil(tileSize/2);
    end
    
    dk = 2*pi/(xLength*cube.d_xy); % delta k
    
    kMin = floor( 2*pi/Lmax/dk ); % number of k-bins from 0 to 2*pi/Lmax
    kMax = floor( 2*pi/Lmin/dk ); % number of k-bins from 0 to 2*pi/Lmin
    
    cutSize = kMax * 2;
    wl = linspace(1, cutSize, cutSize+1); % vector of wavlengths
    kplot = ((wl-fix(cutSize/2)).*dk) - (dk/2); % vector of wavenumbers centered in 0 (for the plot)
    
    %  Perform [k_x, k_y] spectral makeup (aesthetic post-processing) on a per-snapshot loop
    %  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Spkxkyf = zeros([size(Sp3D, 1), size(Sp3D, 1), numel(f)]); % pre-allocate memory space.
    
    for jj = 1:numel(f) % all frequencies OR just the frequencies corresponding to WAVES (f_waves)
        
        %%%  Smooth the spectrum (subrouting courtesy of S.Lehner from DLR):
        
        for s = 1:n_smooth
            Spkxkyf(:,:,jj) = smooth(Sp3D(:,:,jj));
        end
        
    end
    
    %%% Cut the spectrum:
    
    if kMax < halfTile
        cutSpek3D = zeros([2*kMax+1, 2*kMax+1, numel(f)]);
        
        for kk = 1:numel(f);
            cutSpek3D(:,:,kk) = Spkxkyf((halfTile-kMax):(halfTile+kMax),(halfTile-kMax):(halfTile+kMax), kk);
        end
        
    else
        cutSpek3D = Spkxkyf;
    end
    
    %%%  Smooth the spectrum along the frequency domain:
    
    avg_fact = 2;
    
    f2_waves = binavg(f_waves, avg_fact);
    
    Skkf = zeros(size(cutSpek3D, 1),size(cutSpek3D, 1),floor(numel(waves)/avg_fact));
    for ll = 1:size(cutSpek3D, 1)
        for jj2 = 1:size(cutSpek3D, 1)
            
            Skkf(ll, jj2, :) = binavg(cutSpek3D(ll, jj2, waves(1):waves(end)), avg_fact);
            
        end
    end
    
    
    %  Plot spectra: frequency-integrated S(kx, ky)
    %  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    intSkkf = trapz(f2_waves, Skkf, 3); % integration of the 3D spectrum over frequency
    
    kmax = 2 * pi / (Lmin + 10); % wavenumber limit (outer ring) for the spectral plot.
    load colormap_SpecIJRS
    
    clf; % reuse the same figure box instead of making a new one.
    
    if ~wave_heading
        intSkkf = rot90(intSkkf,2);
    end
    contour(kplot, kplot, intSkkf, 30);
    
    axis equal; axis tight; axis off
    colormap(SpecIJRS); %colorbar;
    axis([-kmax kmax -kmax kmax]);
    plt_label{1} = 'Frequency-integrated Wavenumber Spectrum \itS(\itk\rm_x, \itk\rm_y)';
    plt_label{2} = [longName ' - ' timestr];
    title(plt_label);
%     text(0, kmax*1.1, 'N', 'FontSize', 12, 'HorizontalAlign', 'center');
    text(-kmax*1.1, 0, 'W', 'FontSize', 12, 'VerticalAlign', 'middle');
    text(kmax*1.1, 0, 'E', 'FontSize', 12, 'VerticalAlign', 'middle');
    text(0, -kmax*1.1, 'S', 'FontSize', 12, 'HorizontalAlign', 'center');
%     text(0, -kmax*1.15, plt_label, 'FontSize', 10, 'HorizontalAlign', 'center');
    
    hold on
    
    for ii = 0:2*pi/12:(23/12)*pi
        plot([0 kmax*cos(ii)], [0 kmax*sin(ii)], 'color', [0.5 0.5 0.5]);
    end
    
    kas = 2*pi./[75 100 150 200 300 500];
    
    for cc = 1:length(kas)
        xc = kas(cc) * sin(-pi:pi/500:pi);  yc = kas(cc) * cos(-pi:pi/500:pi);
        plot(xc, yc, 'Color', [0.5 0.5 0.5])
        text(kas(cc)*sin(pi*5/4), kas(cc)*cos(pi*5/4), num2str(2*pi./kas(cc), '%5.0f'), 'FontSize', 7,...
            'HorizontalAlignment', 'center', 'backgroundcolor', 'white');
    end
    xc = kmax * sin(-pi:pi/500:pi);  yc = kmax * cos(-pi:pi/500:pi);
    plot(xc, yc, 'k-', 'LineWidth', 1.5);
    text(kmax*sin(pi*5/4), kmax*cos(pi*5/4), ['wavelength, L = ', num2str(2*pi./kmax, '%5.0f'), ' m'], 'FontSize', 7, 'HorizontalAlignment',...
        'center', 'backgroundcolor', 'white');
    
    %%%  Estimation of k_p, L_p, \theta_p and Tp:
    
    [mxren, xxi]= max(intSkkf);
    [maximo, xx]= max(max(intSkkf(:,:)));
    emax = maximo * dk^2;
    en = intSkkf ./ maximo;
    yy = xxi(xx);
    pkx = kplot(xx);
    pky = kplot(yy);
    esum = sum(sum(intSkkf));
    et = esum * dk^2;
    
    subimages(ss).int_angpeak = atan2(pkx, pky)*180/pi;	% direction of the peak wave
    if (subimages(ss).int_angpeak < 0) subimages(ss).int_angpeak = subimages(ss).int_angpeak + 360; end % make all angles positive.
    subimages(ss).int_kpeak = sqrt(pkx^2 + pky^2);	% wave length of the peak wave
    subimages(ss).int_lpeak = 2*pi/subimages(ss).int_kpeak;
    
    subimages(ss).int_Tpeak(ss) = 2*pi/sqrt(9.81 * subimages(ss).int_kpeak * tanh(subimages(ss).h*subimages(ss).int_kpeak)); % considering g = 9.81 ms^-2 & h = 17.5 m.
    
    if wave_heading
%         q_h = quiver(0, 0, pkx, pky, 'k', 'LineWidth', 2.5);
        plot_arrow(0, 0, pkx, pky,'LineWidth',2);
    else
        plot_arrow(pkx, pky, 0, 0,'LineWidth',2);
    end
    
    text(kmax*1.05, kmax*0.9, ['\theta_p = ', num2str(subimages(ss).int_angpeak, '%5.0f'), '^o'], 'FontSize', 12,...
        'HorizontalAlign', 'left');
    text(kmax*1.05, kmax*0.7, ['L_p = ', num2str(subimages(ss).int_lpeak, '%5.1f'), ' m'], 'FontSize', 12,...
        'HorizontalAlign', 'left');
    text(kmax*1.05, kmax*0.5, ['T_p = ', num2str(subimages(ss).int_Tpeak(ss), '%5.1f'), ' s'], 'FontSize', 12,...
        'HorizontalAlign', 'left');
    text(kmax*0.75, -kmax, {'Note:';'Wave angle is reported as';'the direction from which';'the waves are coming.'},'HorizontalAlign','left');
    
    % save spectra png
    subimages(ss).pngName = sprintf('%s_%s_Skxky_%s.png',cube.station,filenumber,subimages(ss).nsname);
    print(gcf,'-dpng',fullfile(dir_res,subimages(ss).pngName));
    
    
end
close(gcf); % close figures.

%%  Save variables:

%  The "right" S(kx,ky) spectrum
%  ~~~~~~~~~~~~~~~~~~~~~~~~~
best_spec_fname = sprintf('%s_%s_Skxky_best.png',cube.station,filenumber);
best_subimage = [];
for i=1:length(subimages)
    if subimages(i).int_lpeak <= maxL
        copyfile(fullfile(dir_res,subimages(i).pngName),fullfile(dir_res,best_spec_fname));
        best_subimage = i;
        break;
    end
end

%  The corresponding radar-derived integral parameters
%  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int_time = radar_toolbox.epoch2Matlab(cube.time(1,1));
intParamsFname = sprintf('%s_%s_intParams.mat',cube.station,filenumber);
save(fullfile(dir_res,intParamsFname), 'subimages', 'int_time', 'best_subimage', 'maxL', 'wave_heading');



%%  Elapsed time:

t_Cubes = toc;

disp(' ');
disp(['These subroutine took ', num2str(t_Cubes/60, '%3.1f'), ' minutes to complete.']);
